## WCORS SPA ver
  
#### <仕様>
  
Root.vue  
L common/header.vue  
L common/sidebar.vue  
L pages/xxx.vue  
    L pages/partial/xxx/SearchForm.vue  
  	
  
- **Mixin**
  - canvas.js:  Dashboardの棒グラフ描画用
  - common_api_request_method.js:   各一覧上で共通のAPIリクエストメソッド。
  - common_dynamic_property.js:   各一覧上で共通の動的プロパティ。主にVuexのプロパティ。
  - common_event_handler_method.js:    各一覧畳で共通のイベントハンドラ。
  - common_static_property.js:    各一覧で共通の静的プロパティ。	csrf、formatなど。
  - detail_common_method.js:    詳細コンポーネント,新規ユーザ作成コンポーネントで共通のユーティリティメソッド。
  - download_csv.js:    一覧上でCSVダウンロードする際のメソッド
  - view_helper.js:   ビューヘルパー
  
  
#### 今後の対応すべき内容
  
**汎化用修正**  
  
各一覧上で表示される項目の「順序」が可変でない。  
現状、種別→エントリー日時→氏名→フリガナ→メールアドレス...  
と固定の順番になってしまっている。  
  
Options通りに可変にするにはOptionsの内容に基づいて配列のインデックスに応じて
以下のth,tdタグの並びを変える必要がある。（全一覧コンポーネントで修正が必要）
  
```
table
  thead.p-list__table-head
    tr(v-if="user_records.length === 0")
      th.p-list__table-col 資料送付状況一覧
    tr(v-else).p-list__table-row
      th.p-list__table-col(data-colname="bulk-checkbox")
      th.p-list__table-col(data-colname="entry-type" v-if="showable_columns.includes('entry_type')") 種別
      th.p-list__table-col(data-colname="entry-date" v-if="showable_columns.includes('entry_date')") エントリー日時
      th.p-list__table-col(data-colname="user-name" v-if="showable_columns.includes('name')") 氏名
      th.p-list__table-col(data-colname="user-kana" v-if="showable_columns.includes('kana')") フリガナ
      th.p-list__table-col(data-colname="user-mail" v-if="showable_columns.includes('email')") メールアドレス
      th.p-list__table-col(data-colname="user-occupation" v-if="showable_columns.includes('occupation')") 職業
      th.p-list__table-col(data-colname="user-tel" v-if="showable_columns.includes('tel')") 電話番号
      th.p-list__table-col(data-colname="user-zipcode" v-if="showable_columns.includes('zipcode')") 郵便番号
      th.p-list__table-col(data-colname="user-address" v-if="showable_columns.includes('address')") 住所
      th.p-list__table-col(data-colname="licence-type" v-if="showable_columns.includes('licence_type')") 免許種類
      th.p-list__table-col(data-colname="general" v-if="showable_columns.includes('general1')") {{ general_map['general1']}}
      th.p-list__table-col(data-colname="react-date" v-if="showable_columns.includes('react_date')") 対応日
      th.p-list__table-col(data-colname="react-interval") 対応日数
      th.p-list__table-col(data-colname="react-status" v-if="showable_columns.includes('react_status')") 状況
      th.p-list__table-col(data-colname="detail") 詳細
  tbody.p-list__table-body
    tr(v-if="user_records.length === 0")
      td.p-list__table-col 対象のエントリーはありません。
    template(v-else)
      tr(v-for="user in user_records"  :class="['is-'+ user.react_status]"  :data-userid="user.id").p-list__table-row
        td.p-list__table-col(data-colname="bulk-checkbox")
          label.u-db.u-posrel(v-if="user.react_status === 'ongoing'")
            input.c-ui__checkbox(type="checkbox"  :data-userid="user.id")
            span.c-ui__checkboxIcon
        td.p-list__table-col(data-colname="entry-type" v-if="showable_columns.includes('entry_type')") {{ entry_types_map[user.entry_type] }}
        td.p-list__table-col(data-colname="entry-date" v-if="showable_columns.includes('entry_date')") {{ formatEntryDate(user.entry_date) }}
        td.p-list__table-col(data-colname="user-name" v-if="showable_columns.includes('name')")
          a(@click.prevent="toggleDetailBox(user)") {{ user.name }}
        td.p-list__table-col(data-colname="user-kana" v-if="showable_columns.includes('kana')") {{ user.kana }}
        td.p-list__table-col(data-colname="user-mail" v-if="showable_columns.includes('email')") {{ user.email }}
        td.p-list__table-col(data-colname="user-occupation" v-if="showable_columns.includes('occupation')") {{ user.occupation }}
        td.p-list__table-col(data-colname="user-tel" v-if="showable_columns.includes('tel')") {{ user.tel }}
        td.p-list__table-col(data-colname="user-zipcode" v-if="showable_columns.includes('zipcode')") {{ user.zipcode }}
        td.p-list__table-col(data-colname="user-address" v-if="showable_columns.includes('address')") {{ setAddress(user) }}
        td.p-list__table-col(data-colname="licence-type" v-if="showable_columns.includes('licence_type')") {{ licence_types_map[user.licence_type] }}
        td.p-list__table-col(data-colname="general" v-if="showable_columns.includes('general1')") {{ user.general1 }}
        template(v-if="showable_columns.includes('react_date')")
          td.p-list__table-col(data-colname="react-date" v-if="user.react_status === 'ongoing'")
            button.c-ui__basicbutton(:data-userid="user.id" @click.prevent="openCalendar($event)") 送付完了
              .c-ui__calendar.is-popup(:data-userid="user.id")
                DatePicker(:inline="true"  language="ja"  :format="format")
                .c-ui__calendar-footer
                  button.c-ui__calendar-btn(	
                    data-btn-type="ensure"
                    :data-userid="user.id"
                    @click.prevent="updateReactedDate($event)") 確定
                  button.c-ui__calendar-btn(
                    data-btn-type="cancel"
                    @click.prevent="closeCalendar") キャンセル
                  label.c-ui__calendar-label
                    input.c-ui__checkbox(type="checkbox" v-model="disable_auto_send")
                    span.u-posrel.c-ui__checkboxIcon 自動送信メールを送らない
        td.p-list__table-col(data-colname="react-date" v-else)
									span {{ user.react_date }} 
        td.p-list__table-col(data-colname="react-interval" v-html="getIntervalSinceEntry(user)")
        td.p-list__table-col(data-colname="react-status" v-if="showable_columns.includes('react_status')") {{ status_types_map[user.react_status] }}
        td.p-list__table-col(data-colname="detail") 
          button.c-ui__basicbutton(@click.prevent="toggleDetailBox(user)") 詳しく見る
```
  
**モデルスキーマ変更**
  
以前であれば、  
  
Customer : Entry   =>  1:N  
Entry : EntryType  =>  1:N  
  
という関係性であったが、非正規化してEntryモデル一本になっている。  
ただし、CSVダウンロード/アップロードなどにおいてデータ数が1万から2万件辺りまでくると現状のスキーマだと  
処理が逆に遅くなる可能性がある。  