/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueRouter from 'vue-router'
import Vuex from 'vuex'

window.Vue = require('vue')
window.Vue.use(VueRouter)
window.Vue.use(Vuex)


require('./bootstrap');
require('es6-promise').polyfill();
require('babel-polyfill');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


/**
 * import respective page's component
 */
import Root            from './components/vue/Root'
import Login           from './components/vue/pages/Login'
import Dashboard       from './components/vue/pages/Dashboard'
import EntryList       from './components/vue/pages/EntryList'
import InquiryList     from './components/vue/pages/InquiryList'
import BrochureList    from './components/vue/pages/BrochureList'
import CampaignList    from './components/vue/pages/CampaignList'
import TmpEnteringList from './components/vue/pages/TmpEnteringList'
import EnteringList    from './components/vue/pages/EnteringList'
import NoActionList    from './components/vue/pages/NoActionList'
import MailTemplate    from './components/vue/pages/MailTemplate'
import FileUpload      from './components/vue/pages/FileUpload'


/**
 * ルーティング管理
 */
const router = new VueRouter({
	mode : 'history',    
	routes : [
        { path : '/'                     , redirect  : '/login'          ,meta : {requiresAuth : false }},
        { path : '/login'                , component : Login             ,meta : {requiresAuth : false }},        
        { path : '/crm'                  , component : Root                 ,redirect  : '/crm/entry-list' ,
            children : [
                { path : 'dashboard'        , component : Dashboard         ,meta : {requiresAuth : true }},
                { path : 'entry-list'       , component : EntryList         ,meta : {requiresAuth : true }},
                { path : 'inquiry-list'     , component : InquiryList       ,meta : {requiresAuth : true }},
                { path : 'brochure-list'    , component : BrochureList      ,meta : {requiresAuth : true }},
                { path : 'campaign-list'    , component : CampaignList      ,meta : {requiresAuth : true }},
                { path : 'tmp-entering-list', component : TmpEnteringList   ,meta : {requiresAuth : true }},
                { path : 'entering-list'    , component : EnteringList      ,meta : {requiresAuth : true }},
                { path : 'no-action-list'   , component : NoActionList      ,meta : {requiresAuth : true }},
                { path : 'mail-template'    , component : MailTemplate      ,meta : {requiresAuth : true }},
                { path : 'file-upload'      , component : FileUpload        ,meta : {requiresAuth : true }},
            ]
        },
    ],    
});


/**
 * 認証・認可のルーティング管理
 * -- セッション・Remember Me保持はLocalStorageで。
 * -- 発行はLogin.Vue内で行い、bootstrap.jsでexpires_timeの管理を行う
 * -- ルーティングの度に認可の確認が必要なので、app.jsで全体を管理する
 */
router.beforeEach((to,from,next) => {

    const authenticate = window.localStorage.getItem('_auth');
    const authrization = JSON.parse(window.localStorage.getItem('crm_auth_role')||null);

    if(to.matched.some(record => record.meta.requiresAuth)){

        // ログイン前は、requiresAuthがfalseのページにしか行けない
        if(!authenticate){
            next({ path : '/login'});
        }else{
            next();
        }


        // ログイン済みであっても認可以外のページにはアクセスできない。
        // 認可情報は、ログイン時にUserModelからLocalStorageへ返却される
        if(authrization){
            switch(to.path){
                case '/crm/dashboard':
                    if(authrization.role_dashboard == 0){
                        next({ path : '/crm/entry-list'})
                    }else{
                        next();
                    }
                    break;
                case '/crm/campaign-list':
                    if(authrization.role_campaign == 0){
                        next({ path : '/crm/entry-list'})
                    }else{
                        next();
                    }
                    break;
                case '/crm/mail-template':
                    if(authrization.role_templates == 0){
                        next({ path : '/crm/entry-list'})
                    }else{
                        next();
                    }
                    break;
                default:
                    break;
            }    
        }    

    }else{

        // ログイン後は、requiresAuthがtrueのページにしか行けない
        if(window.localStorage.getItem('_auth')){
            next({ path : '/crm/entry-list'})
        }else{
            next();    
        }        
    }
})


/**
 * Vuex 状態管理
 */
const store = new Vuex.Store({
	state : {
        /** 
         * authorization
         *
         * LocalStorageの認可情報は一定期間保持されるが、
         * 同様の内容を`Session中のみ保持するため`のVuexオブジェクト
         * ログイン時に設定され、ログアウト時に破棄される
         */
        role_admin : false,
        role_campaign : false,
        role_csv : false,
        role_dashboard : false,
        role_entries : false,
        role_general_user : false,
        role_templates : false,



        /**
         * Configuration
         *
         */
        config__entry_types_map      : "",
        config__licence_types_map    : "",
        config__status_types_map     : "",
        config__history_types_map    : "",
        config__via_routes_map       : "",
        config__dashboard_colors     : "",
        config__dashboard_colors_map : "",
        config__general_maps         : "",
        config__showable_items       : "",
        
        // 現在のページ情報
		current_page : "",

        // 学校ごとの設定
        school__key  : "",
        school__name : "",
        school__entering_type : "",

        // Detail Vueへのデータ引き渡し用
        current_user : "",
        reference_user : "",
        current_user_history : "",
        current_user_histories : "",

        // 表示状態管理
        detail_is_opening : false,
        create_user_is_opening : false,        

        // SidebarToolTip用
        ongoing_inquiry : "",
        ongoing_brochure : "",
        ongoing_tmp_entering : "",

        // Campaign-listでの一括送信判定用
        white_list : "",

        /**
         * Event Hub
         *
         * 兄弟コンポーネント向けにevent pub/sub発行をするためのVuexオブジェクト
         */
        from : "",
        to : "",
        what : "",

        // print
        open_print : false,        
	},

	mutations : {
        // authorization map
        makeAuthrizationMap(state,roles){
            for(let key in roles){
                roles[key] == 1 ? state[key] = true : state[key] = false;
            }
        },
        resetAuthrizationMap(state){
            state.role_admin = false;
            state.role_campaign = false;
            state.role_csv = false;
            state.role_dashboard = false;
            state.role_entries = false;
            state.role_general_user = false;
            state.role_templates = false;
        },

        // control path
		getCurrentPage(state){
			let current_page = "";
			const BASE_DIR = "/crm/";

			current_page = router.currentRoute.path.substr(BASE_DIR.length) // truncate slush
			state.current_page = current_page ? current_page : 'login';
		},

        // user data rendering in Detail
        setCurrentUser(state,user){
            if(user){
                state.current_user = user;
                state.reference_user = _.cloneDeep(user);
                return true;
            }
            return false;           
        },
        resetUser(state){
            state.current_user = "";
            state.reference_user = "";
            return true;
        },
        setCurrentUserHistory(state,history){            
            state.current_user_history = history;
        },
        resetCurrentUserHistory(state){            
            state.current_user_history = "";
        },
        setCurrentUserHistories(state,histories){
            state.current_user_histories = histories;
        },
        resetCurrentUserHistories(state){
            state.current_user_histories = "";
        },
        updateCurrentUserHistories(state,histories){
            state.current_user_histories = histories;
        },

        toggleCreateUserComponent(state){
            state.create_user_is_opening = !state.create_user_is_opening;
        },
        toggleDetailComponent(state){
            state.detail_is_opening = !state.detail_is_opening;

            if(state.detail_is_opening === false){
                state.current_user = "";
                state.reference_user = "";
            }
        },        

        // sidebar tooltip
        fetchTooltipCounter(state){
            axios.get('/api/fetch-no-action-tooltip').then( res => {
                state.ongoing_inquiry      = res.data.ongoing_inquiry;
                state.ongoing_brochure     = res.data.ongoing_brochure;
                state.ongoing_tmp_entering = res.data.ongoing_tmp_entering;                
            });
        },

        // campaign
        setWhiteListForSendingMail(state,users){
            state.white_list = users;
            return true;
        },
        resetWhiteList(state){
            state.white_list = "";
            return true;
        },

        // print
        setOpenPrintPreview(state,value){
            state.open_print = value;
        },

        // event hub
        notifyEventToSiblingElement(state,obj){
            state.from = obj.from;
            state.to   = obj.to;
            state.what = obj.what;   
        },
        resetEventhub(state){
            state.from = "";
            state.to   = "";
            state.what = "";
        },        
        fetchConfig(state){
            axios.get('/api/fetch-config').then(res => {
                state.config__entry_types_map       = res.data.entry_types_map;
                state.config__licence_types_map     = res.data.licence_types_map;
                state.config__status_types_map      = res.data.status_types_map;
                state.config__history_types_map     = res.data.history_types_map;
                state.config__via_routes_map        = res.data.via_routes_map;                
                state.config__dashboard_colors      = res.data.dashboard_colors;
                state.config__general_maps          = res.data.general_maps;
                state.config__showable_items        = res.data.items;
                state.config__dashboard_colors      = res.data.dashboard_colors;
                state.config__dashboard_colors_map  = res.data.dashboard_colors_map;

                state.school__entering_type         = res.data.school_entering_type;
                state.school__key                   = res.data.school_key;
                state.school__name                  = res.data.school_name;
            });
        }
	}
})

/**
 * インスタンス生成
 */
const app = new Vue({
    el: '#app',
    router,    
    store,
    components : {
        Root
    },
    computed : {
    	current_page(){
    		return this.$store.state.current_page;
    	}
    },
    watch : {
    	$route : {
    		handler : function(newValue){
    			this.$store.commit('getCurrentPage');
    		}
    	}
    },
    created(){
        const roles = JSON.parse(window.localStorage.getItem('crm_auth_role')||null);

        if(roles) this.$store.commit('makeAuthrizationMap',roles);        
    },
    beforeUpdate(){
        const roles = JSON.parse(window.localStorage.getItem('crm_auth_role')||null);

        if(roles) this.$store.commit('makeAuthrizationMap',roles);
    },
    mounted(){
    	this.$store.commit('getCurrentPage');
        this.$store.commit('fetchTooltipCounter');
        this.$store.commit('fetchConfig');        
    }
});