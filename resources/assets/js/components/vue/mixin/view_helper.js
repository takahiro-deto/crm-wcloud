/**
 *
 * Mixin: view_helper.js
 *
 * setAddress            : 住所の連結
 * formatEntryDate       : entry_dateのdatetimeをdateに
 * getIntervalSinceEntry : react_statusに応じて、経過日数or対応日数を返す
 * 
 *
 */

// 3rd Party Lib
const moment = require('moment')

export default{
	methods : {
		setAddress(user){
			return (user.address_pref || "") + (user.address_city || "") + (user.address_town || "");
		},
		formatEntryDate(entry_date){
			let format_date = "";
			const match = entry_date.match(/(\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}):\d{1,2}/);

			match ? format_date = match[1] : format_date = entry_date;				

			return format_date;
		},
		getIntervalSinceEntry(user){
			let interval, today, entry_date, react_date;


			today = moment();
			entry_date = moment(user.entry_date);
			if(user.react_date) react_date = moment(user.react_date);


			if(user.react_status == "ongoing"){				
				interval = "経過日数<br/>" + today.diff(entry_date,'days') + "日";
			}else if(user.react_status == "entered"){
				interval = "対応日数";
			}else if(user.react_date){
				interval = "対応日数<br/>" + react_date.diff(entry_date,'days') + "日";
			}
			
			return interval;
		}
	}		
}