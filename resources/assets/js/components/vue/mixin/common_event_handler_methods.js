/**
 *
 * Mixin: common_event_handler_methods.js
 *
 * toggleDetailBox            :
 * onScroll                   :
 *
 */

export default{
	methods : {
		/***************************************
		 * 各種一覧上のイベントハンドラメソッド群
		 **************************************/
		/**
		 *
		 * --- Dependencies data property---
		 *
		 * property		 
		 *      + user
		 *      + fetch_option
		 *
		 * methods
		 *      - setCurrentUser
		 *      - setCurrentUserHistories
		 *      - infiniteScroll
		 *
		 */
		toggleDetailBox(user){
			const self = this;

			// Detailがshowされる前に、Vuexにクリック対象のUserをセット
			self.setCurrentUser(user);

			axios.get('/api/fetch-respective-history',{
                params : {
                    id : user.id
                }
            }).then(res => {
            	self.setCurrentUserHistories(res.data.histories);
            });

			// Detail Box slide in
			this.$store.commit('toggleDetailComponent');
		},
		onScroll(event){
			if(this.fetch_option.scroll_prevent) return false;

			/**
			 * LodashのThrottle関数の自作。lodashのthrottleはVueのライフサイクルの問題からか
			 * うまく動作しないので自作。
			 * fn ... 実行したい関数
			 * wait ... 待ち時間。最後のインタラクション発火後、wait timeの経過時間後、１回だけfnが実行される。
			 */
			const throttle = (fn, wait) => {
				let time = Date.now();
				return () => {
					if((time + wait - Date.now()) < 0){
						fn();
						time = Date.now();
					}
				}
			}

			const target = document.querySelector('.p-list__table-body tr:last-of-type');
			const breakpoint = target.getBoundingClientRect().bottom + window.pageYOffset;

			if(((window.scrollY || window.pageYOffset) + window.innerHeight) >= breakpoint){
				throttle(this.infiniteScroll(), 1000);
			}
		},
	},
	created(){
		/***************************************
		 * 各種一覧上のイベントハンドラメソッド群
		 **************************************/
		const self = this;

		window.addEventListener('scroll',(event) => {
			if(self.$store.state.current_page !== 'mail-template' &&
			   self.$store.state.current_page !== 'dashboard'){

				self.onScroll(event);

			}	
		},false);
	},
}