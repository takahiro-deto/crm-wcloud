const moment = require('moment');
const city_address_rawdata = require('../config/cities.json');	

export default {
	data(){
		return {			
			pref_list : this.getPrefList(city_address_rawdata),
			age_list : this.getAgeList(),
			birth_year_list : this.getBirthYearList(),
			birth_month_list : ['01','02','03','04','05','06','07','08','09','10','11','12'],
			birth_day_list : ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'],
		}
	},
	methods: {
		/***************************************
		 * View Helper
		 **************************************/
		truncate(string){
			if(string.length > 19){
				return string.substr(0, 19) + '...';
			}else{
				return string;
			}				
		},		
		parseTime(time){
			return time.substr(0,10).replace(/-/g,'/');
		},			
		/***************************************
		 * Make Model Helper
		 **************************************/
		getAgeList(){
			const list = [];
			for(let i = 16; i <= 98; i++){
				list.push(i);
			}

			return list;
		},
		getPrefList(city_address_rawdata){
			const pref_list = [];
			const prefObj = Object.keys(city_address_rawdata);

			for(let key in prefObj){
				pref_list.push(prefObj[key]);
			}				
			return pref_list;
		},
		getIconClass(history_type){
			let result = "";

			switch(history_type){
				case 'memo':
					result = 'fa fa-file-text-o';
					break;
				case 'received_mail':
				case 'inquiry':
				case 'brochure':
				case 'tmp_entering':
					result = 'fa fa-arrow-circle-down';
					break;
				default:
					result = 'fa fa-arrow-circle-up';
					break
			}
			
			return result;
		},
		getBirthYearList(){
            const this_year         = moment().format('Y');
            const start_year        = this_year - 16 - 80;
            const last_year         = this_year - 16;
            const birth_year_list   = [];

			for(let i = start_year; i <= last_year; i++){
				birth_year_list.push(i);
			}

			return birth_year_list;
		},			
	}
}