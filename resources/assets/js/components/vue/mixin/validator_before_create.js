export default {
	methods : {
		validateBeforeCreate(target,validator){
			const results = [];
			let key = "";

			for(key in validator){
				const confirm_item_array = validator[key]['confirm'].split('|');

				if(confirm_item_array.length > 1){
					let done = false;

					for(let confirm_item of confirm_item_array){
						if(! done){

							switch(confirm_item){
								case "presence":
									if(!target[key]){
										results.push(validator[key]['err_msg']['presence']);

										done = true;
									}
									break;
								case "valid":
									if(!target[key].match(
										/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i)
									){
										results.push(validator[key]['err_msg']['valid']);

										done = true;
									}
								default:
									break;
							}		

						}						
					}					
					

				}else if(confirm_item_array.length == 1){

					const elem = confirm_item_array[0];

					switch(elem){
						case "presence":
							if(!target[key]){

								results.push(validator[key]['err_msg']);

							}
							break;
						case "valid":
							if(!target[key].match(
								/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i)
							){

								results.push(validator[key]['err_msg']);

							}
						default:
							break;
					}						
				}
				
			}
			return results;
		}
	}
}