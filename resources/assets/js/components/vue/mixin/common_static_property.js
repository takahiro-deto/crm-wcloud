/**
 *
 * Mixin: common_static_property.js
 *
 */

// 3rd Party Lib
const moment = require('moment')

export default{
	data(){
		return {
			csrf   : window.Laravel.csrfToken,
			today  : moment().format('YYYY-MM-DD'),
			format : 'yyyy-MM-dd',
		}
	}
}