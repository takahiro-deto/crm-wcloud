/**
 *
 * Mixin: canvas.js
 *
 * Dashboardページでのサマリーブロックでbarchartを描画する
 * barchartは全てCanvas2Dによって描画される。
 *
 * 描画は全てCanvas Classのメソッドによって行われ、外部からはdrawingメソッドを通じて描画の指令を出す
 *
 */ 

// 3rd Party Lib
const moment = require('moment')

export default{			
	data(){
		return {
			canvas : '',
			canvas_params : '',
			canvas_klass : '',			
		}
	},
	computed : {
		graph_data_src(){
			return this.$store.state.config__dashboard_colors_map;
		}
	},
	mounted : function(){
		const self = this;

		/**
		 * start class definition
		 */
		const configObj = {
			labelDefaultFont : "24px Meiryo",
			labelDefaultColor : "#727272",
		}		


		class Canvas{
			constructor(w,h){
				// this.dpr = window.devicePixelRatio || 1;
				// this.dpr = window.devicePixelRatio || 2;
				this.dpr = 2;

				// width, height
				this.width = w * this.dpr;
				this.height = h * this.dpr;
				
				/**
				 * each graph's position and size
				 *
				 * you can customizze below value
				 *
				 * 173 ... th tag width + both side border of table
				 * 79  ... td tag width + one side border of table
				 * 40  ... bar width, approximately equal to half of td tag width
				 */
		        this.leftOffset     = 173 * this.dpr;
		        this.columnWidth    = 79 * this.dpr;
		        this.barwidth       = 40 * this.dpr;

		        /**
		         * graph data setting
		         */
		        this.graphDataSrc = self.graph_data_src;

		        /**
		         * graph field setting
		         */
		        // offset
		        this.monthLabelHeight = 50;
		        this.paddingTop = 30;

		        // font style
		        this.monthLabelFontStyle = configObj.labelDefaultFont;
		        this.monthLabelFontColor = configObj.labelDefaultColor;

		        this.fieldFontStyle = configObj.labelDefaultFont;
		        this.fieldFontColor = configObj.labelDefaultColor;

		        this.sumLabelFontStyle = configObj.labelDefaultFont;
		        this.sumLabelFontColor = configObj.labelDefaultColor;



		        this.eachMonthBarHeight = [];
			}

			/**
			 * @var stage       => DOM object of canvas target
			 * 		 
			 */

			setUp(stage){
				if(typeof stage.getContext === 'undefined'){
					return;
				}

				this.ctx = stage.getContext('2d');

		        stage.width     = this.width;
		        stage.height    = this.height;

		        stage.style.width   = this.width / 2 + 'px';
		        stage.style.minWidth   = this.width / 2 + 'px';
		        stage.style.height  = this.height / 2 + 'px';
			}

			/**
			 * drawing barchart
			 *
			 * @var 
			 *    data        => object which is passed from laravel
			 *    field_max_y => graph field's max y axis of coordinates
			 */

			drawBarChart(data,from_month,to_month,field_max_y,interval,months){
				/**
				 * the counter of barchart x position,
				 * To draw, you need shift barchart x position gradually
				 */
				let bar_x_pos = 0;

				/**
				 * initialize for rerendering
				 */
				this.eachMonthBarHeight = [];

				/**
				 * calculate each bar height ratio
				 *
				 * e.g. height : 250, fields_max_y : 2000, data_a : 50
				 *
				 *      => ratio is (50 / 2000 * 250)
				 */
				function calcRatio(data,data_key,data_val_index,height,months){
					let ratio = 0;

					if(data.hasOwnProperty(data_key) && data[data_key].hasOwnProperty(data_val_index)){
						ratio = parseFloat(data[data_key][data_val_index] / height);					
					}
					
					return ratio;
				}

				/**
				 *
				 * interval is the number of subtracted month between start-date and end-date
				 * it means how many columns you need.
				 *
				 * Note:
				 * with the constraint to draw, this value is at most 12
				 *
				 * i.e. you can draw only 12 month before from end-date's month.
				 *
				 */

				// for(let k = 0; k < Math.min(months.length,12); k++){
				for(let k = 0; k < months.length; k++){
					/**
					 *
					 * you need shift barchart y position gradually
					 * because of this barchart type is stuck bar chart,
					 * you have got to draw next bar graph on the former,
					 * so you should store and increment `former barchart height` in "prev" variable
					 *
					 */
					let prev = 0;
					
					for(let i = 0; i < this.graphDataSrc.length; i++){
						let key = Object.keys(this.graphDataSrc[i])[0];

						this.ctx.fillStyle = this.graphDataSrc[i][key];
						this.ctx.fillRect(
							/**
							 * X position
							 * leftOffset(th tag width) + td tag width * each month's number + offset for locating bar in center of td tag.
							 */
							this.leftOffset + (this.columnWidth * bar_x_pos) + ((this.columnWidth - this.barwidth) / 2),

							/**
							 * Y position
							 * (this.height - this.monthLabelHeight) => calculate padding bottom of drawing field							 
							 * you need increment former barchart height in "prev"
							 */
							(this.height - this.monthLabelHeight) - calcRatio(data,key,months[k],field_max_y) * (this.height - this.monthLabelHeight - this.paddingTop) - prev,							
							
							/**
							 * barWidth
							 */

							this.barwidth,

							/**
							 * barHeight
							 */
							calcRatio(data,key,months[k],field_max_y) * (this.height - this.monthLabelHeight - this.paddingTop)
						);
						

						prev += calcRatio(data,key,months[k],field_max_y) * (this.height - this.monthLabelHeight - this.paddingTop);
					}


					// push each month's barchart height to below array			
					this.eachMonthBarHeight.push(prev);
					
					bar_x_pos++;

					// before shift to next month, reset prev data.
					prev = 0;
				}				
			}

			/**
			 * drawing month label
			 *
			 * @var 
			 *    to_month    => end-date's month
			 *    interval    => subtracted number between end-date's month and start-date's month
			 */

			drawMonthLabel(to_month,interval,months){
				/**				 
				 * drawing boundary between stuck barchart and month label
				 */
				this.ctx.beginPath();
				this.ctx.moveTo(0,this.height - this.monthLabelHeight);
				this.ctx.lineTo(this.width,this.height - this.monthLabelHeight);
				this.ctx.strokeStyle = "#CCC";
				this.ctx.stroke();
				this.ctx.closePath();

				this.ctx.beginPath();

				/**
				 * label area height is 50, font-size is 24, so 50 / 2 + 12
				 * centering by horizontal and by vertical
				 */
				const labelHeightWithAlignMiddled = this.height - 25 + 12;

				/**
				 * caluculate label's x offset
				 */
				const offset = function(month){					
					// return month < 10 ? 20 : 11;
					return 11;
				}
		        
		        this.ctx.font = this.monthLabelFontStyle;
				this.ctx.fillStyle = this.monthLabelFontColor;

				// for(let i = 0, max = Math.min(interval,12); i < max; i++){
				for(let i = 0; i < months.length; i++){
					let label = moment(months[i]).format('YYYY年MM月');					
					this.ctx.fillText(label,this.leftOffset + this.columnWidth * i + offset(i), labelHeightWithAlignMiddled)
				}		        
			}

			/**
			 * drawing month label
			 *
			 * @var 
			 *    data        => object which is passed from laravel
			 *    interval    => subtracted number between end-date's month and start-date's month
			 */

			drawSumLabel(data,interval,months){
				/**
				 * sum up each object key's value and to locale string
				 */
				const calcSum = function(data,data_val_index){
					let sum = 0;
					let val = 0;

					for(let key in data){						
						data[key][data_val_index] ? val = data[key][data_val_index] : val = 0;
						
						sum += val;
					}

					return sum.toLocaleString();
				}
				/**
				 * caluculate label's offset
				 */
				const offset = function(num_length){
					let offset = "";

					switch(num_length.length){
						case 1:
							offset = 70;
							break;
						case 2:
							offset = 65;
							break;
						case 3:						
							offset = 55;
							break;
						default:							
							offset = 45;
							break
					}

					return offset;
				}

				this.ctx.beginPath();
				this.ctx.font = this.sumLabelFontStyle;
				this.ctx.fillStyle = this.sumLabelFontColor;

				for(let i = 0; i <= months.length; i++){
					if(calcSum(data,months[i]) === "0") continue;

					this.ctx.fillText(
						// String
						calcSum(data,months[i]),						
						// X pos
						this.leftOffset + this.columnWidth * i + offset(calcSum(data,months[i])), 
						// Y pos
						this.height - this.eachMonthBarHeight[i] - this.monthLabelHeight - parseInt(this.ctx.font)
					);	
				}			
			}

			/**
			 * drawing background of stuck barchart
			 *
			 * @var
			 *    field_max_y => graph field's max y axis of coordinates
			 */ 
			drawField(field_max_y){
				/**
				 * set field label
				 * label interval is quarter of field_max_y
				 */
				let label_array = [];

				for(let i = 1; i <= 4; i++){
					// not use float value
					let elem = parseInt(field_max_y / 4 * i);
					label_array.push(elem.toLocaleString());
				}


				/**
				 * caluculate label's offset
				 */
				let ctx = this.ctx;
				const offset = function(num_length){
					const left_shift =  num_length < 4 ? 1.5 : 1.7;
					return (parseInt(ctx.font) / left_shift * num_length) + 4;
				}
				
				this.ctx.fillStyle = "#FAFAFA";			

				this.ctx.fillRect(
					this.leftOffset,
					this.paddingTop,
					this.width - this.leftOffset, 
					(this.height - this.monthLabelHeight - this.paddingTop)/ 4
				);
				this.ctx.fillRect(
					this.leftOffset,
					this.paddingTop + (this.height - this.monthLabelHeight - this.paddingTop)/ 4 * 2,
					this.width * 2 - this.leftOffset, 
					(this.height - this.monthLabelHeight - this.paddingTop)/ 4
				);

				this.ctx.beginPath();
				this.ctx.font = this.fieldFontStyle;
				this.ctx.fillStyle = this.fieldFontColor;

				// ** last number adjusts label x position.
				this.ctx.fillText(label_array[0],this.leftOffset - offset(label_array[0].length),this.paddingTop + (this.height - this.monthLabelHeight)/ 4 * 3 - 14)
				this.ctx.fillText(label_array[1],this.leftOffset - offset(label_array[1].length),this.paddingTop + (this.height - this.monthLabelHeight)/ 4 * 2 - 10)
				this.ctx.fillText(label_array[2],this.leftOffset - offset(label_array[2].length),this.paddingTop + (this.height - this.monthLabelHeight)/ 4 * 1 - 4)
				this.ctx.fillText(label_array[3],this.leftOffset - offset(label_array[3].length),this.paddingTop + (this.height - this.monthLabelHeight)/ 4 * 0 + 5)
			}
		}

		self.canvas_klass = Canvas;
		/**
		 * end up class definition
		 * then, start the mounted function
		 */		

		/**
		 * Important!
		 *
		 * $nextTick means you render this component after all view components have been rendered perfectly.
		 * because this component refer to `computed property and prop from parents` component - `params.data`
		 */
		this.$nextTick(function(){
			self.canvas_params = {				
				width       : window.innerWidth - 195 - (10 * 2) - (22 * 2) - 30,
				height      : 254,
				canvasdom   : document.querySelector('#stage'),
				from_month  : 0,
				to_month    : 12,
				months      : [],
				interval    : '',
				field_max_y : 2000,
				data        : ''
			}

			self.drawing();
		});				
	},
	methods : {
		drawing(data,dates,interval){			

			if(interval && interval > 12){
				this.canvas_params['width'] = 173 + 79 * interval;
			}

			const canvas = new this.canvas_klass(this.canvas_params['width'],this.canvas_params['height']);

			if(this.canvas) this.canvas = "";
			this.canvas = canvas;


			// update the parameter for drawing canvas
			// canvas_render_data is defined in StuckBarChart.vue
			this.updateCanvasParams(data || this.canvas_render_data, dates || this.$props.dates);



			const params = this.canvas_params;
			this.canvas.setUp(params['canvasdom']);

			this.canvas.drawField(params['field_max_y']);

			this.canvas.drawBarChart(params['data'],
									 params['from_month'],
									 params['to_month'],
									 params['field_max_y'],
									 params['interval'],
									 params['months']);

			this.canvas.drawMonthLabel(params['to_month'],
									   params['interval'],
									   params['months']);

			this.canvas.drawSumLabel(params['data'],									
									 params['interval'],
									 params['months']);

		},
		updateCanvasParams(data,dates){
			const self = this;

			this.canvas_params.data = data;
			this.canvas_params.from_month = dates.start_date;
			this.canvas_params.to_month   = dates.end_date;

			
			/**
			 * calculate interval
			 */
			const from_month = this.canvas_params.from_month.replace(/-\d{2}$/,'-01');
			const to_month = this.canvas_params.to_month.replace(/-\d{2}$/,'-01');

			const interval = moment(to_month).diff(moment(from_month), 'month') + 1;

			this.canvas_params.interval = interval;


			/**
			 * @depend interval
			 *
			 * calculate months
			 */
			const months = [];
			for(let j = 0; j < this.canvas_params.interval; j++){
				months.push(moment(this.canvas_params.to_month).subtract(j,'month').format('YYYY-MM'));	
			}

			months.reverse();			
			this.canvas_params.months = months;
			


			/**
			 * @depend interval
			 *
			 * calculate field_max_y
			 *
			 * calculate what month's sum data is the largest by Math.max.apply()
			 */			
			const sumArray = Array(this.canvas_params.interval).fill(0);

			Object.keys(data).map(function(key){
				for(let i = 0; i < self.canvas_params.months.length; i++){
					let month = self.canvas_params.months[i];

					if(data[key][month]){
						sumArray[i] += parseInt(data[key][month]);
					}else{
						sumArray[i] += 0;
					}					
				}
			});

			const max = Math.max.apply(null,sumArray);			
			this.canvas_params.field_max_y = this.roundUpUnsignedInteger(max);
		},
		roundUpUnsignedInteger(num){
			const digit = num.toString().length;
			let field_max_y = 0;

			const max_points = [10,25,50,75,100,250,500,750,1000,2500,5000,7500,10000,25000,50000,75000,100000];

			for(let i = 0; i < max_points.length; i++){
				if(num < max_points[0]){

					field_max_y = max_points[0];

				}else if(num > max_points[max_points.length - 1]){

					field_max_y = max_points[max_points.length - 1];

				}else if(num > max_points[i] && num <= max_points[i + 1]){

					field_max_y = max_points[i + 1];

				}
			}			
			return field_max_y;
		}
	}
}