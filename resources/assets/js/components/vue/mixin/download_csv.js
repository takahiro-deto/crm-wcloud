/**
 *
 * Mixin: download_csv.js
 * 
 * 各一覧上でのCSVデータ出力用
 */

// 3rd Party Lib
const moment = require('moment')

export default{
	computed : {
		csv_file_name(){
			return this.$store.state.current_page.split('-list')[0] + '_' + moment().format('YYYY-MM-DD');
		}
	},
	methods : {
		downloadCsv(){
            const condition     = this.$props.condition;
			const os            = window.navigator.platform.match(/Win/) ? "win" : "other";
            let params          = `?os=${os}&name=${this.csv_file_name}&`;
            let param_keys      = "";

			if(this.$props.freeword) condition.freeword = this.$props.freeword;						
			param_keys = Object.keys(condition);

			for(let i = 0, max = param_keys.length - 1; i <= max; i++){
				let key = param_keys[i];
				params += `${key}=${condition[key]}`;

				if(i != max){
					params += '&';
				}
			}				

			const url = window.location.protocol + '//'
					  + window.location.host 
					  + '/crm/csv-download-specified-users-data' 
					  + params;

			window.open(url,'_blank');				
		}
	}		
}