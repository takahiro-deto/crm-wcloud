/**
 *
 * Mixin: common_dynamic_property.js
 *
 * イミュータブルかつレンダリング時に一回限りしか利用しないデータは
 * props,Vuexでは使わずにmixinで定義する
 *
 */

export default {
	computed : {
    	entry_types_map(){
    		return this.$store.state.config__entry_types_map;
    	},
    	licence_types_map(){
    		return this.$store.state.config__licence_types_map;
    	},
        status_types_map(){
        	return this.$store.state.config__status_types_map;
        },
        history_types_map(){
        	return this.$store.state.config__history_types_map;
        },
        via_routes_map(){
        	return this.$store.state.config__via_routes_map;
        },
        general_map(){
        	return this.$store.state.config__general_maps;
        },
        showable_items(){
        	return this.$store.state.config__showable_items;
        }
	},	
}