/**
 *
 * Mixin: common_api_request_methods.js
 *
 * fetchUserData              :
 * fetchUserDataWithKeyword   :
 * refreshUserData            :
 * refreshMultiUsersData      :
 * infiniteScroll             :
 *
 */

export default{
	methods : {
		/***************************************
		 * ユーザーデータ取得用メソッド群
		 **************************************/
		/**
		 *
		 * --- Dependencies data property---
		 *
		 * user_records : '',
		 * fetch_option : {
		 *     api_url        : "/api/fetch-users-list",
		 *     fetch_length   : 50,    // １回の取得件数
		 *     max_count      : "",    // Math.round( 検索総件数 / １回の取得件数 )
		 *     counter        : 0,     // 現在何回目のrefetchか
		 *     scroll_prevent : false, // scrollが最下部に到達するまで、refetch Ajaxを制御する（prevent）するためのデータプロパティ
		 * },
		 * condition : {
		 *     react_status   : "",
		 *     entry_type     : "",
		 *     licence_type   : "",
		 *     need_no_action : false
		 * },
		 * freeword : "",              // フリーワードをconditionのプロパティにしてしまうと、入力中に検索が走ってしまうので、切り出し
		 *
		 */

		fetchUserData(condition, length = 50){
			const self = this;
			const option = this.fetch_option;

			// fetchUserDataが呼ばれるのは常に新規条件検索の時なのでカウンターを常にリセット
			if(option.counter !== 0) option.counter = 0;
			// 描画が終わるまで、refetchを防止する
			option.scroll_prevent = true;

			axios.get(option.api_url,
				{ params : _.merge(
					{init : 1, offset : 0, length : length,}
					, condition)
				}
			).then( res => {
				self.user_records       = [];
                self.user_records       = res.data.user_records;
                option.max_count        = res.data.max_count;
                option.scroll_prevent   = false;   // refetch許可
			})
		},
		fetchUserDataWithKeyword(){
			const condition    = _.clone(this.condition);
			condition.freeword = this.freeword;

			this.fetchUserData(condition);
		},
		refreshUserData(target_user){
			const self = this;

			axios.get('/api/fetch-specified-user-data',{
				params : {
					id : target_user
				}
			}).then(res => {

				const new_record = res.data;
				let insert_index = "";
				
				$.each(self.user_records,(index,record) => {
					if(record.id === new_record.id){
						insert_index = index;
						return false;
					}
				});	
				
				Vue.set(self.user_records, insert_index, new_record);
				self.$store.commit('fetchTooltipCounter');
			})			
		},
		refreshMultiUsersData(target_users){				
			const self = this;
			let ids = target_users;

			if(typeof target_users === "string" && target_users.match(/|/))
			{
				ids = target_users.split('|');
			}

			axios.get('/api/fetch-specified-multi-users-data',{
				params : {
					id : ids
				}
			}).then(res => {
				
				const new_records = res.data;
				let insert_index = "";					

				for(let i = 0; i < new_records.length; i++){
					$.each(self.user_records,(index,record) => {
						if(record.id === new_records[i].id){
							insert_index = index;								
					    	return false;
						}
					})

					Vue.set(self.user_records, insert_index, new_records[i]);
				}
				
			})
		},			
		infiniteScroll(){
			const self = this;
			const condition = this.condition;
			const option = this.fetch_option;

			if(option.counter < Math.ceil(option.max_count / option.fetch_length)){
				if(this.user_records.length !== option.max_count){

					option.scroll_prevent = true;

					axios.get(option.api_url,{
						params : _.merge({
							offset : (option.counter + 1) * option.fetch_length,
							length : option.fetch_length,
							init   : 0,
							need_no_action : condition.need_no_action
						}, condition)
					}).then( res => {
						self.user_records = self.user_records.concat(res.data.user_records);
						option.counter++;

						option.scroll_prevent = false;
					})
				}
			}
		},
	},
	mounted(){
		/***************************************
		 * ユーザーデータ取得用メソッド群
		 **************************************/
		this.fetchUserData(this.condition);
	}
}