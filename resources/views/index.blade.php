<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title><?php echo opt('base.school_name');?></title>
	<link href="{{ mix('/css/app.css') }}" rel="stylesheet" media="all" type="text/css">	
	<link rel="icon" type="image/x-icon" href="/images/favicon.ico">
	<script type="text/javascript">
        window.Laravel = window.Laravel || {};
        window.Laravel.csrfToken = "{{ csrf_token() }}";
    </script>    
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo opt('common.google_api_key')['map']; ?>"></script>
</head>
<body>
	<div id="app">
		<router-view></router-view>
	</div>
	<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
</body>
</html>