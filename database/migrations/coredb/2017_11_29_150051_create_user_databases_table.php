<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDatabasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('coredb')){
                Schema::create('user_databases', function (Blueprint $table) {
                $table->string('web_host', 255)->primary();
                $table->string('db_name', 255);
                $table->string('db_host', 255);
                $table->string('db_user', 255);
                $table->string('db_password', 255);
                $table->timestamps();
            });
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_databases');
    }
}
