<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('email')->index();
            $table->string('history_type')->nullable();
            $table->string('history_title')->nullable();
            $table->longText('history_content')->nullable();            
            $table->dateTime('history_date')->nullable();
            $table->string('subject')->nullable();
            $table->longText('body')->nullable();
            $table->longText('body_html')->nullable();            
            $table->string('reply_to')->nullable();
            $table->unsignedInteger('receive_mail_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
