<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('received_mails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mail_box_id')->unsigned();
            $table->string('message_id',1000)->charset('ascii')->collation('ascii_bin')->nullable()->index();
            $table->datetime('received_date')->nullable();
            $table->string('from', 255)->nullable();
            $table->string('from_name', 255)->nullable();
            $table->string('reply_to', 255)->nullable();
            $table->string('subject', 255)->nullable();
            $table->longtext('body')->nullable();
            $table->longtext('body_html')->nullable();
            $table->longtext('header_raw')->nullable();
            $table->longtext('body_raw')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('received_mails');
    }
}
