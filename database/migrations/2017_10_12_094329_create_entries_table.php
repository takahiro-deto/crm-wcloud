<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');

            /**
              * Basic Information
              */
            $table->string('student_no')->index()->nullable();
            $table->string('name')->index()->nullable();
            $table->string('kana')->index()->nullable();
            $table->string('email')->index();
            $table->string('entry_title')->nullable();
            $table->longText('entry_content')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('address_pref')->nullable();
            $table->string('address_city')->nullable();
            $table->string('zipcode_level_address')->nullable();
            $table->string('address_town')->nullable();
            $table->string('tel')->index()->nullable();
            $table->string('tel2')->index()->nullable();
            $table->integer('age')->nullable();
            $table->string('sex')->nullable();
            $table->date('birthday')->nullable();
            $table->string('occupation')->nullable();
            $table->string('school')->nullable();
            $table->string('via_routes')->nullable();
            $table->tinyInteger('dm_flag')->default(1); 

            /**
              * About Entry Data
              */
            $table->string('entry_type')->nullable();
            $table->string('licence_type')->nullable();
            $table->string('react_status')->nullable();
            $table->string('device', 255)->nullable();
            $table->longtext('user_agent')->nullable();

            $table->datetime('entry_date')->nullable();
            $table->date('react_date')->nullable();
            $table->date('entered_date')->nullable(); 

            $table->datetime('auto_send_date')->nullable();
            $table->unsignedInteger('disable_auto_send')->default(0);

            $table->text('general1')->nullable();
            $table->text('general2')->nullable();
            $table->text('general3')->nullable();
            $table->text('general4')->nullable();
            $table->text('general5')->nullable();
            $table->text('general6')->nullable();
            $table->text('general7')->nullable();
            $table->text('general8')->nullable();
            $table->text('general9')->nullable();
            $table->text('general10')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
