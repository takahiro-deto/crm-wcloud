<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();

            // Addition
            $table->string('login_id',191);
            $table->integer('role_general_user')->default(1);
            $table->integer('role_dashboard')->default(0);
            $table->integer('role_entries')->default(0);
            $table->integer('role_campaign')->default(0);
            $table->integer('role_templates')->default(0);
            $table->integer('role_csv')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
