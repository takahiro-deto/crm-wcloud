<?php

use Illuminate\Database\Seeder;

class ZipcodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coredb.zipcodes')->delete();

        $path = dirname(__FILE__) . "/csv/address.csv";

        $file = new \SplFileObject($path);
        $file->setFlags(\SplFileObject::READ_CSV);
        $file->setCsvControl("\t");
        $lines = new \LimitIterator($file,1);

        foreach($lines as $line)
        {
        	if(!is_null($line[0]))
        	{	        		
                
                $zipcode        = $line[0];
                $kana_address   = $line[1];
                $kanji_address  = $line[2];
                	            	            
	            	
            	DB::table('coredb.zipcodes')->insert([
                    'zipcode'           => $zipcode,
                    'kana_address'      => $kana_address,
                    'kanji_address'     => $kanji_address,
	            ]);		           	            
        	}        
    	}
    }
}
