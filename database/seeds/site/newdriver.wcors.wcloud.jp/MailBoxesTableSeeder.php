<?php

namespace Site_newdriver_gcp_wcloud_jp;

use Illuminate\Database\Seeder;
use App\Models\MailBox;
use Crypt;

use DB;

class MailBoxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_boxes')->delete();
        
        MailBox::insert([
            'mail_box' => '{sv2022.xserver.jp:993/imap/ssl/novalidate-cert}',
            'username' => 'ndk@newdriver.co.jp',
            'password' => Crypt::encrypt('knG7vAhqCR'),
        ]);
    }
}
