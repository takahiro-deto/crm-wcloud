<?php

namespace Site_newdriver_gcp_wcloud_jp;

use Illuminate\Database\Seeder;
use App\Models\Entry;

use DB;

class SoftDeleteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {     	
     	DB::table('entries')->inRandomOrder()->chunk(500,function($users){
            foreach($users as $user)
            {
                $id = $user->id;
                $target = DB::table('entries')->where('id',$id)->first();
                
                $target->update(['react_status' => 'no_action']);
                $target->delete();
            }
        });
    }
}
