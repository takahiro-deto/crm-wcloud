<?php

namespace Site_newdriver_gcp_wcloud_jp;

use Illuminate\Database\Seeder;
use App\User;
use Hash;

use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::insert([
            [
                'name' => 'WAKUWAKU',
                'email' => 'wcors-nd@wkwk.co.jp',
                'password' => Hash::make('WcAdmNds!'),
                'login_id' => 'wakuwaku',

                'role_dashboard'    => 1,
                'role_entries'      => 1,
                'role_campaign'     => 1,
                'role_templates'    => 1,
                'role_csv'          => 1,
            ],
            [
                'name' => 'マネージャー',
                'email' => 'info-mg@newdriver.co.jp',
                'password' => Hash::make('NDmnp#40'),
                'login_id' => 'manager',

                'role_dashboard'    => 1,
                'role_entries'      => 1,
                'role_campaign'     => 1,
                'role_templates'    => 1,
                'role_csv'          => 1,
            ],
            [
                'name' => '一般ユーザー',
                'email' => 'info-gn@newdriver.co.jp',
                'password' => Hash::make('QSjiv@83'),
                'login_id' => 'user',

                'role_dashboard'    => 0,
                'role_entries'      => 0,
                'role_campaign'     => 0,
                'role_templates'    => 0,
                'role_csv'          => 0,
            ],
        ]);
    }
}
