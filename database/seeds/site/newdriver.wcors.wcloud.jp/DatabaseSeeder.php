<?php

namespace Site_newdriver_gcp_wcloud_jp;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);        
        $this->call(EntriesTableSeeder::class);        
        $this->call(TemplateSeeder::class);        
        $this->call(OptionsTableSeeder::class);        
        $this->call(MailBoxesTableSeeder::class);        
        $this->call(CustomerTableSeeder::class);
        $this->call(SoftDeleteSeeder::class);        
    }
}
