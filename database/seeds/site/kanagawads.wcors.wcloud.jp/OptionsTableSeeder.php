<?php

namespace Site_kanagawads_gcp_wcloud_jp;

use Illuminate\Database\Seeder;
use App\Models\Option;

use DB;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('options')->delete();

        $insert_array = [           
            [
                'key' => 'base.school_key',
                'val' => 'kanagawads', 
                'remarks' => '教習所毎のキー'
            ],          
            [
                'key' => 'base.school_name',
                'val' => '神奈川ドライビングスクール', 
                'remarks' => '教習所名'
            ],          
            [
                'key' => 'base.school_address',
                'val' => [
                    'zipcode' => '2320033',
                    'address' => '神奈川県横浜市南区中村町5-312',
                    'lat'     => '35.4297041',
                    'lng'     => '139.6217519',
                    ], 
                'remarks' => '教習所の所在地データ'
            ],          
            [
                'key' => 'base.school_entering_type',
                'val' => '入校', 
                'remarks' => '入校か入所か'
            ],          
            [
                'key' => 'common.general',
                'val' => [
                    'general1' => 'メモ（検索対象）',
                    'general2' => '汎用2',
                    'general3' => '汎用3',
                    'general4' => '汎用4',
                    'general5' => '汎用5',
                    'general6' => '汎用6',
                    'general7' => '汎用7',
                    'general8' => '汎用8',
                    'general9' => '汎用9',
                    'general10' => '汎用10',
                    ],
                'remarks' => '汎用項目'
            ],          
            [
                'key' => 'common.google_api_key',
                'val' => [
                    'map' => 'AIzaSyDRwLoOkUsdu9TK48otqFn6we9B2471jDI',
                    'geo' => 'AIzaSyAZQDiQESo-ATzblqjHAiHMfoC7phWMLWM',
                    ], 
                'remarks' => 'google map apiのキー'
            ],  
            [
                'key' => 'view.columns_map.entry_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => 'エントリー一覧項目'
            ],
            [
                'key' => 'view.columns_map.inquiry_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '問い合わせ一覧項目'
            ],          
            [
                'key' => 'view.columns_map.brochure_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '資料請求一覧項目'
            ],
            [
                'key' => 'view.columns_map.campaign_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => 'キャンペーンメール一覧項目'
            ],
            [
                'key' => 'view.columns_map.tmp_entering_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '仮申し込み一覧項目'
            ],
            [
                'key' => 'view.columns_map.entering_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '入所状況', 'column' => 'entered_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '入所確認作業一覧項目'
            ],          
            [
                'key' => 'view.columns_map.no_action_list',
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'フリガナ', 'column' => 'kana'],                
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '職業', 'column' => 'occupation'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '対応不要一覧項目'
            ],          
            [
                'key' => 'view.columns_map.csv',
                'val' => [
                    ['name' => '種別', 'column' => 'entry_type'],
                    ['name' => 'エントリー日時', 'column' => 'entry_date'],
                    ['name' => '免許種類', 'column' => 'licence_type'],
                    ['name' => '教習生番号', 'column' => 'student_no'],
                    ['name' => '氏名', 'column' => 'name'],
                    ['name' => 'フリガナ', 'column' => 'kana'],
                    ['name' => 'メールアドレス', 'column' => 'email'],
                    ['name' => '郵便番号', 'column' => 'zipcode'],
                    ['name' => '住所', 'column' => 'address'],
                    ['name' => '電話番号', 'column' => 'tel'], 
                    ['name' => '電話番号2', 'column' => 'tel2'],           
                    ['name' => '年齢', 'column' => 'age'],            
                    ['name' => '職業', 'column' => 'occupation'],
                    ['name' => 'メモ', 'column' => 'general1'],
                    ['name' => 'エントリー経路', 'column' => 'via_routes'],
                    ['name' => '状況', 'column' => 'react_status'],
                    ['name' => '対応日時', 'column' => 'react_date'],
                    ['name' => '入所日', 'column' => 'entered_date'],            
                    ['name' => 'DM', 'column' => 'dm_flag'],            
                ],
                'remarks' => 'CSV項目',
            ],
            [
                'key' => 'view.master.entry_types',
                'val' => ['問い合わせ','資料請求','仮申し込み','その他'], 
                'remarks' => 'エントリー種別マスタ（array）'
            ],
            [
                'key' => 'view.master.status',
                'val' => ['対応中','対応済み','入所済み','対応不要'], 
                'remarks' => '対応状況（ステータス）（array）'
            ],
            [
                'key' => 'view.master.licence_types',
                'val' => [
                    '普通免許AT',
                    '普通免許MT',
                    '普通免許（AT/MT未定）',
                    '普通二輪免許AT',
                    '普通二輪免許MT',
                    '普通二輪免許（AT/MT未定）',            
                    '小型二輪免許AT',
                    '小型二輪免許MT',
                    '小型二輪免許（AT/MT未定）',
                    'AT限定解除',
                    'ペーパードライバー',
                ],
                'remarks' => '免許種類マスタ（array）'
            ],
            [
                'key' => 'view.master.via_routes',
                'val' => [
                    'ホームページ',
                    '直接来校',
                    '電話',
                    '教習所サーチ',
                    'DSE',
                    'ナンバメイト',
                    'その他',
                ],
                'remarks' => '経路マスタ（array）'
            ],      
            [
                'key' => 'view.master.db_licence_type_color',
                'val' => [
                        ['普通免許AT' => '#27c200'],
                        ['普通免許MT' => '#45ce23'],
                        ['普通免許（AT/MT未定）' => '#66da49'],
                        ['普通二輪免許AT' => '#0088c2'],
                        ['普通二輪免許MT' => '#1594ca'],
                        ['普通二輪免許（AT/MT未定）' => '#2ca0d1'],
                        ['小型二輪免許AT' => '#44add9'],
                        ['小型二輪免許MT' => '#5ebbe1'],
                        ['小型二輪免許（AT/MT未定）' => '#7ac8e8'],
                        ['AT限定解除' => '#ff9900'],
                        ['ペーパードライバー' => '#ffaf1b'],
                        ['その他' => '#cccccc'],
                    ], 
                'remarks' => 'ダッシュボード用免許種類カラー'
            ],
            [
                'key' => 'mail.sender_config',
                'val' => [
                    'from' => 'contact@kanagawa-ds.com',
                    'name' => '神奈川ドライビングスクール',
                    'reply' => null,
                    'to' => 'kds@kds.gr.jp'
                    ], 
                'remarks' => 'CRMから送信されるメールのFROM'
            ],
            [
                'key' => 'mail.replace_string',
                'val' => '$氏名$ 様\n', 
                'remarks' => 'メールテンプレートに自動挿入する文章'
            ],
            [
                'key' => 'mail.parser_class',
                'val' => ['KanagawadsMailParser','DssCustomMailParser','DseMailParser'],
                'remarks' => 'メール解析用クラス'
            ],
            [
                'key' => 'mail.licence_type_map_of_dss',
                'val' => [
                    "普通免許（MT）" => "regular_mt",
                    "普通免許（AT）" => "regular_at",
                    "普通免許（AT\/MT未定）" => "regular_tba",
                    "普通二輪免許（MT）" => "regular_bike_mt",
                    "普通二輪免許（AT）" => "regular_bike_at",
                    "普通二輪免許（AT\/MT未定）" => "regular_bike_tba",
                    "小型二輪免許（MT）" => "small_bike_mt",
                    "小型二輪免許（AT）" => "small_bike_at",
                    "小型二輪免許（AT\/MT未定）" => "small_bike_tba",
                    "大型二輪免許（MT）" => "large_bike_mt",
                    "大型二輪免許（AT）" => "large_bike_at",
                    "大型二輪免許（AT\/MT未定）" => "large_bike_tba",
                    "普通二種免許（MT）" => "regular_second_mt",
                    "普通二種免許（AT）" => "regular_second_at",
                    "普通二種免許（AT\/MT未定）" => "regular_second_tba",
                    "準中型免許" => "pre_middle",
                    "中型免許" => "middle",
                    "大型免許" => "large",
                    "中型二種免許" => "middle_second",
                    "大型二種免許" => "large_second",
                    "大型特殊免許" => "large_special",
                    "けん引免許" => "tow"
                    ], 
                'remarks' => 'メールの免許種類の関連付け（DSS）'
            ],
            [
                'key' => 'mail.licence_type_map_of_dse',
                'val' => [
                    '普通免許' => 'regular_mt',
                    '普通免許MT' => 'regular_mt',
                    '普通免許AT限定' => 'regular_at',
                    '準中型免許' => 'pre_middle',
                    '中型免許' => 'middle',
                    '大型免許' => 'large',
                    '普通二輪免許' => 'regular_bike_mt',
                    '普通二輪免許MT' => 'regular_bike_mt',
                    '普通二輪免許AT限定' => 'regular_bike_at',
                    '普通二輪免許小型限定' => 'small_bike_mt',
                    '普通二輪免許小型AT限定' => 'small_bike_at',
                    '小型二輪免許' => 'small_bike_mt',
                    '小型二輪免許MT' => 'small_bike_mt',
                    '小型二輪免許AT限定' => 'small_bike_at',
                    '大型二輪免許' => 'large_bike_mt',
                    '大型二輪免許MT' => 'large_bike_mt',
                    '大型二輪免許AT限定' => 'large_bike_at',
                    '普通二種免許' => 'regular_second_mt',
                    '普通二種免許MT' => 'regular_second_mt',
                    '普通二種免許AT限定' => 'regular_second_at',
                    '中型二種免許' => 'middle_second',
                    '大型二種免許' => 'large_second',
                    '大型特殊免許' => 'large_special',
                    'けん引免許' => 'tow',
                ],
                'remarks' => 'メールの免許種類の関連付け（DSENET）',
            ],
            [
                'key' => 'mail.licence_type_map_of_hp',
                'val' => [
                    '普通車MT' => 'regular_mt',
                    '普通車AT' => 'regular_at',
                    'AT限定解除' => 'litf_restrictions',
                    'ペーパードライバー' => 'paper_driver',
                    '二輪MT（～400cc）' => 'regular_bike_mt',
                    '二輪AT（～400cc）' => 'regular_bike_at',
                    '二輪MT（～125cc）' => 'small_bike_mt',
                    '二輪AT（～125cc）' => 'small_bike_at',            
                    ], 
                'remarks' => 'メールの免許種類の関連付け（HP）'
            ],
            [
                'key' => 'mail.parser_entry_types_object',
                'val' => [
                    'inquiry' => ['use' => true, 'name' => '問い合わせ', 'short' => '問い合せ'],
                    'brochure' => ['use' => true, 'name' => '資料請求', 'short' => '資料請求'],
                    'tmp_entering' => ['use' => true, 'name' => '仮申し込み', 'short' => '仮申込み'],
                    ], 
                'remarks' => 'エントリー種別'
            ],
            [
                'key' => 'mail.parser_status_types_object',
                'val' => [
                    'ongoing' => '対応中',
                    'reacted' => '対応済み',
                    'entered' => '入所済み',
                    'no_action' => '対応不要',
                    ], 
                'remarks' => 'ステータス種別'
            ],
            [
                'key' => 'mail.parser_via_routes_types_object',
                'val' => [
                    'hp' => 'ホームページ',
                    'dss' => '教習所サーチ',
                    'dse' => 'DSE',
                    'etc' => 'その他',
                    ], 
                'remarks' => 'エントリー種別'
            ],          
            ['key' => 'db_config.crm_using_flag','val' => true, 'remark' => 'CRM利用フラグ'],
            [
                'key' => 'db_config.crm_using_item',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'licence_type',
                    'student_no',
                    'name',
                    'kana',
                    'email',
                    'zipcode',
                    'address_pref',
                    'address_city',
                    'address_town',            
                    'tel',
                    'tel2',
                    'age',            
                    'occupation',
                    'general1',
                    'via_routes',            
                    'react_status',
                    'react_date',
                    'entered_date',
                    'dm_flag',
                ], 
                'remarks' => 'CRM利用項目'
            ],
            [
                'key' => 'db_config.freeword_search_target',
                'val' => ['name', 'tel', 'email', 'general1'], 
                'remarks' => 'フリーワード検索対象カラム'
            ],
            [
                'key' => 'db_config.status_en_jp_map',
                'val' => [
                        ['ongoing' => '対応中'],
                        ['reacted' => '対応済み'],                
                        ['entered' => '入所済み'],
                        ['no_action' => '対応不要']
                    ], 
                'remarks' => '対応状況（ステータス）EN-JPマップ'
            ],
            [
                'key' => 'db_config.via_en_jp_map',
                'val' => [
                        ['hp' => 'ホームページ'],
                        ['dss' => '教習所サーチ'],
                        ['dse' => 'DSE'],
                        ['etc' => 'その他'],               
                    ], 
                'remarks' => '経路マスタの関連付け EN-JPマップ'
            ],
            [
                'key' => 'db_config.entry_types_en_jp_map',
                'val' => [
                        ['inquiry' => '問い合わせ'],
                        ['brochure' => '資料請求'],
                        ['tmp_entering' => '仮入所申し込み'],
                        ['other' => 'その他'],
                    ], 
                'remarks' => 'エントリータイプ全リスト'
            ],
            [
                'key' => 'db_config.licence_types_en_jp_map',
                'val' => [                
                        ['regular_at' => '普通免許AT'],
                        ['regular_mt' => '普通免許MT'],
                        ['regular_tba' => '普通免許（AT/MT未定）'],
                        ['regular_bike_at' => '普通二輪免許AT'],
                        ['regular_bike_mt' => '普通二輪免許MT'],
                        ['regular_bike_tba' => '普通二輪免許（AT/MT未定）'],
                        ['small_bike_at' => '小型二輪免許AT'],
                        ['small_bike_mt' => '小型二輪免許MT'],
                        ['small_bike_tba' => '小型二輪免許（AT/MT未定）'],
                        ['litf_restrictions' => 'AT限定解除'],
                        ['paper_driver' => 'ペーパードライバー'],                
                    ], 
                'remarks' => '免許種別全リスト'
            ],
                [
                'key' => 'db_config.history_types_en_jp_map',
                'val' => [
                        ['reply_to_inquiry' => 'メール送信'],
                        ['send_campaign' => 'CPメール'],
                        ['reply_to_tmp_entering' => 'メール送信'],
                        ['memo' => '対応メモ'],
                        ['inquiry' => '問合わせ'],
                        ['brochure' => '資料請求'],
                        ['tmp_entering' => '仮申し込み'],
                        ['general_mail' => 'メール送信'],
                        ['received_mail' => 'メール受信'],
                        ['auto_sending' => '自動送信'],
                        ['send_brochure' => '資料送付'],
                    ], 
                'remarks' => '履歴テーブルタイプリスト'
            ],
            [
                'key' => 'db_config.customer_common_data_list',
                'val' => [                    
                    'name',
                    'kana',
                    'email',
                    'student_no',
                    'address_pref',
                    'address_city',
                    'address_town',
                    'birthday',
                    'tel',
                    'tel2',
                    'age',
                    'sex',
                    'occupation',
                    'school',
                    'general1',
                    'general2',                    
                    'dm_flag',
                    ],
                'remarks' => 'エントリーデータ更新時に同一メールアドレスの場合に一括更新されなければならない項目',
            ]
        ];

    	$callback = function (&$item) {
            $item['val'] = json_encode($item['val'], JSON_UNESCAPED_UNICODE);
        };

        array_walk($insert_array, $callback);
        Option::insert($insert_array);        
    }
}
