<?php

namespace Site_keiseids_wcors_wcloud_jp;

use Illuminate\Database\Seeder;
use App\Models\MailBox;
use Crypt;

use DB;

class MailBoxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_boxes')->delete();
        
        MailBox::insert([
            'mail_box' => '{keisei-ds.sakura.ne.jp:993/imap/ssl/novalidate-cert}',
            'username' => 'kds-crm@keisei-ds.co.jp',
            'password' => Crypt::encrypt('vSdg8dNw3H'),
        ]);
    }
}
