<?php

namespace Site_keiseids_wcors_wcloud_jp;

use Illuminate\Database\Seeder;
use App\Models\Option;

use DB;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->delete();

        $insert_array = [
            [
                'key' => 'base.school_key',
                'val' => 'keiseids', 
                'remarks' => '教習所毎のキー'
            ],          
            [
                'key' => 'base.school_name',
                'val' => '京成ドライビングスクール', 
                'remarks' => '教習所名'
            ],          
            [
                'key' => 'base.school_address',                
                'val' => [
                    'zipcode' => '1250054',
                    'address' => '東京都葛飾区高砂5丁目54番10号',
                    'lat'     => '35.752019',
                    'lng'     => '139.873209',
                    ], 
                'remarks' => '教習所の所在地データ'
            ],          
            [
                'key' => 'base.school_entering_type',
                'val' => '入校', 
                'remarks' => '入校か入所か'
            ],          
            [
                'key' => 'common.general',                
                'val' => [
                    'general1' => 'メモ（検索対象）',
                    'general2' => '汎用2',
                    'general3' => '汎用3',
                    'general4' => '汎用4',
                    'general5' => '汎用5',
                    'general6' => '汎用6',
                    'general7' => '汎用7',
                    'general8' => '汎用8',
                    'general9' => '汎用9',
                    'general10' => '汎用10',
                    ],
                'remarks' => '汎用項目'
            ],          
            [
                'key' => 'common.google_api_key',                
                'val' => [
                    'map' => 'AIzaSyDRwLoOkUsdu9TK48otqFn6we9B2471jDI',
                    'geo' => 'AIzaSyAZQDiQESo-ATzblqjHAiHMfoC7phWMLWM',
                    ], 
                'remarks' => 'google map apiのキー'
            ],  
            [
                'key' => 'view.columns_map.entry_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => 'エントリー一覧項目'
            ],
            [
                'key' => 'view.columns_map.inquiry_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '問い合わせ一覧項目'
            ],          
            [
                'key' => 'view.columns_map.brochure_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '資料請求一覧項目'
            ],
            [
                'key' => 'view.columns_map.campaign_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => 'キャンペーンメール一覧項目'
            ],
            [
                'key' => 'view.columns_map.tmp_entering_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => 'メールアドレス', 'column' => 'email'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '仮申し込み一覧項目'
            ],
            [
                'key' => 'view.columns_map.entering_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '入所状況', 'column' => 'entered_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '入所確認作業一覧項目'
            ],          
            [
                'key' => 'view.columns_map.no_action_list',                
                'val' => [
                        ['name' => '種別', 'column' => 'entry_type'],
                        ['name' => 'エントリー日時', 'column' => 'entry_date'],
                        ['name' => '氏名', 'column' => 'name'],
                        ['name' => '電話番号', 'column' => 'tel'],
                        ['name' => '郵便番号', 'column' => 'zipcode'],
                        ['name' => '住所', 'column' => 'address'],
                        ['name' => '免許種類', 'column' => 'licence_type'],
                        ['name' => 'メモ', 'column' => 'general1'],
                        ['name' => '対応日時', 'column' => 'react_date'],
                        ['name' => '状況', 'column' => 'react_status'],
                    ], 
                'remarks' => '対応不要一覧項目'
            ],
            [
                'key' => 'view.columns_array.entry_list',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'tel',
                    'zipcode',
                    'address',
                    'licence_type',
                    'general1',
                    'react_date',
                    'react_status'
                ], 
                'remarks' => 'エントリー一覧項目'
            ],
            [
                'key' => 'view.columns_array.inquiry_list',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'email',
                    'tel',
                    'licence_type',
                    'general1',
                    'react_date',
                    'react_status'
                ], 
                'remarks' => '問い合わせ一覧項目'
            ],
            [
                'key' => 'view.columns_array.brochure_list',                
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'tel',
                    'zipcode',
                    'address',
                    'licence_type',
                    'general1',
                    'react_date',
                    'react_status'
                ], 
                'remarks' => '資料請求一覧項目'
            ],
            [
                'key' => 'view.columns_array.campaign_list',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'email',
                    'tel',
                    'licence_type',
                    'general1',
                    'react_date',
                    'react_status'
                ], 
                'remarks' => 'キャンペーンメール一覧項目'
            ],
            [
                'key' => 'view.columns_array.tmp_entering_list',                
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'email',
                    'tel',
                    'licence_type',
                    'general1',
                    'react_date',
                    'react_status'
                ], 
                'remarks' => '仮申し込み一覧項目'
            ],
            [
                'key' => 'view.columns_array.entering_list',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'tel',
                    'zipcode',
                    'address',
                    'licence_type',
                    'general1',
                    'entered_date',
                    'react_status'
                ], 
                'remarks' => '入所確認作業一覧項目'
            ],
            [
                'key' => 'view.columns_array.no_action_list',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'name',
                    'tel',
                    'zipcode',
                    'address',
                    'licence_type',
                    'general1',
                    'react_date',
                    'react_status'
                ], 
                'remarks' => '対応不要一覧項目'
            ],
            [
                'key' => 'view.columns_map.csv',
                'val' => [
                    ['name' => '種別', 'column' => 'entry_type'],
                    ['name' => 'エントリー日時', 'column' => 'entry_date'],
                    ['name' => '免許種類', 'column' => 'licence_type'],
                    ['name' => '教習生番号', 'column' => 'student_no'],
                    ['name' => '氏名', 'column' => 'name'],
                    ['name' => 'メールアドレス', 'column' => 'email'],
                    ['name' => '郵便番号', 'column' => 'zipcode'],
                    ['name' => '住所', 'column' => 'address'],
                    ['name' => '電話番号', 'column' => 'tel'],
                    ['name' => '年齢', 'column' => 'age'],
                    ['name' => '生年月日', 'column' => 'birthday'],
                    ['name' => '性別', 'column' => 'sex'],
                    ['name' => '職業', 'column' => 'occupation'],
                    ['name' => '学校', 'column' => 'school'],
                    ['name' => 'メモ', 'column' => 'general1'],
                    ['name' => 'エントリー経路', 'column' => 'via_routes'],
                    ['name' => '状況', 'column' => 'react_status'],
                    ['name' => '対応日時', 'column' => 'react_date'],
                    ['name' => '入所日', 'column' => 'entered_date'],
                    ['name' => 'DM', 'column' => 'dm_flag'],
                ],
                'remarks' => 'CSV項目',
            ],
            [
                'key' => 'view.master.entry_types',
                'val' => ['問い合わせ','資料請求','仮申し込み','その他'], 
                'remarks' => 'エントリー種別マスタ（array）'
            ],
            [
                'key' => 'view.master.status',
                'val' => ['対応中','対応済み','入所済み','対応不要'], 
                'remarks' => '対応状況（ステータス）（array）'
            ],
            [
                'key' => 'view.master.licence_types',                
                'val' => [
                    '普通免許AT',
                    '普通免許MT',
                    '普通免許（AT/MT未定）',
                    '準中型免許',
                    '中型免許',
                    '大型免許',
                    '普通二輪免許AT',
                    '普通二輪免許MT',
                    '普通二輪免許（AT/MT未定）',
                    '小型二輪免許AT',
                    '小型二輪免許MT',
                    '小型二輪免許（AT/MT未定）',
                    '大型二輪免許AT',
                    '大型二輪免許MT',
                    '大型二輪免許（AT/MT未定）',
                    '普通二種免許AT',
                    '普通二種免許MT',
                    '普通二種免許（AT/MT未定）',
                    '中型二種免許',
                    '大型二種免許',
                    '大型特殊免許',
                    '不明',
                    'その他'
                ], 
                'remarks' => '免許種類マスタ（array）'
            ],
            [
                'key' => 'view.master.via_routes',
                'val' => [
                    '直接来所',
                    'ホームページ',
                    '生協',
                    '私校連',
                    '提携校',
                    '教習所サーチ',
                    'DSENET',
                    '東京で免許',
                    '教習所へ行こう',
                    'S&H',
                    '紹介所',
                    'GP',
                    '養成',
                    'その他'
                ],
                'remarks' => '経路マスタ（array）'
            ],      
            [
                'key' => 'view.master.db_licence_type_color',
                'val' => [
                        ["普通免許AT" => "#27c200"], 
                        ["普通免許MT" => "#45ce23"],
                        ["普通免許（AT/MT未定）" => "#66da49"],
                        ["準中型免許" => "#8be774"],
                        ["中型免許" => "#b3f3a3"],
                        ["大型免許" => "#deffd6"],
                        ["普通二輪免許AT" => "#0088c2"],
                        ["普通二輪免許MT" => "#1594ca"],
                        ["普通二輪免許（AT/MT未定）" => "#2ca0d1"],
                        ["小型二輪免許AT" => "#44add9"],
                        ["小型二輪免許MT" => "#5ebbe1"],
                        ["小型二輪免許（AT/MT未定）" => "#7ac8e8"],
                        ["大型二輪免許AT" => "#97d6f0"],
                        ["大型二輪免許MT" => "#b5e4f7"],
                        ["大型二輪免許（AT/MT未定）" => "#d6f3ff"],
                        ["普通二種免許AT" => "#ff9900"],
                        ["普通二種免許MT" => "#ffaf1b"],
                        ["普通二種免許（AT/MT未定）" => "#ffc336"],
                        ["中型二種免許" => "#ffd452"],
                        ["大型二種免許" => "#ffe26d"],
                        ["大型特殊免許" => "#ffed88"],
                        ["不明" => "#fff6a3"],                        
                    ], 
                'remarks' => 'ダッシュボード用免許種類カラー'
            ],
            [
                'key' => 'mail.sender_config',                
                'val' => [
                    'from' => 'kds-crm@keisei-ds.co.jp',
                    'name' => '京成ドライビングスクール',
                    'reply' => null,
                    'to' => 'info@keisei-ds.co.jp'
                    ], 
                'remarks' => 'CRMから送信されるメールのFROM'
            ],
            [
                'key' => 'mail.replace_string',
                'val' => '$氏名$ 様\n', 
                'remarks' => 'メールテンプレートに自動挿入する文章'
            ],
            [
                'key' => 'mail.parser_class',
                'val' => ['KeiseidsMailParser','DssMailParser','DseMailParser','TokyomenkyoMailParser','GomenkyoMailParser'],
                'remarks' => 'メール解析用クラス'
            ],
            [
                'key' => 'mail.licence_type_map_of_dss',
                'val' => [
                    "普通免許（MT）" => "regular_mt",
                    "普通免許（AT）" => "regular_at",
                    "普通免許（AT\/MT未定）" => "regular_tba",
                    "普通二輪免許（MT）" => "regular_bike_mt",
                    "普通二輪免許（AT）" => "regular_bike_at",
                    "普通二輪免許（AT\/MT未定）" => "regular_bike_tba",
                    "小型二輪免許（MT）" => "small_bike_mt",
                    "小型二輪免許（AT）" => "small_bike_at",
                    "小型二輪免許（AT\/MT未定）" => "small_bike_tba",
                    "大型二輪免許（MT）" => "large_bike_mt",
                    "大型二輪免許（AT）" => "large_bike_at",
                    "大型二輪免許（AT\/MT未定）" => "large_bike_tba",
                    "普通二種免許（MT）" => "regular_second_mt",
                    "普通二種免許（AT）" => "regular_second_at",
                    "普通二種免許（AT\/MT未定）" => "regular_second_tba",
                    "準中型免許" => "pre_middle",
                    "中型免許" => "middle",
                    "大型免許" => "large",
                    "中型二種免許" => "middle_second",
                    "大型二種免許" => "large_second",
                    "大型特殊免許" => "large_special",
                    "けん引免許" => "tow"
                    ], 
                'remarks' => 'メールの免許種類の関連付け（DSS）'
            ],
            [
                'key' => 'mail.licence_type_map_of_hp',
                'val' => [
                    "普通免許(MT)" => "regular_mt",
                    "普通免許(AT限定)" => "regular_at",
                    "準中型免許" => "pre_middle",
                    "準中型(AT5t限定解除)" => "pre_middle",
                    "準中型(ATのみ限定解除)" => "pre_middle",
                    "中型免許" => "middle",
                    "中型免許(AT8t限定解除)" => "middle",
                    "中型免許(ATのみ限定解除)" => "middle",
                    "大型免許" => "large",
                    "普通二輪免許(MT)" => "regular_bike_mt",
                    "普通二輪免許(AT限定)" => "regular_bike_at",
                    "小型二輪免許(MT)" => "small_bike_mt",
                    "小型二輪免許(AT限定)" => "small_bike_at",
                    "大型二輪免許(MT)" => "large_bike_mt",
                    "大型二輪免許(AT限定)" => "large_bike_at",
                    "普通二種免許(MT)" => "regular_second_mt",
                    "普通二種免許(AT限定)" => "regular_second_at",
                    "中型二種免許" => "middle_second",
                    "大型二種免許" => "large_second",
                    "大型特殊免許" => "large_special",
                    "不明" => "unknown",
                    "普通車" => "regular_mt",
                    "普通車（ＡＴ）" => "regular_at",
                    "準中型車" => "pre_middle",
                    "中型車" => "middle",
                    "大型車" => "large",
                    "普通二輪車" => "regular_bike_mt",
                    "普通二輪車（ＡＴ）" => "regular_bike_at",
                    "小型二輪車" => "small_bike_mt",
                    "小型二輪車（ＡＴ）" => "small_bike_at",
                    "大型二輪車" => "large_bike_mt",
                    "大型二輪車（ＡＴ）" => "large_bike_at",
                    "普通二種" => "regular_second_mt",
                    "普通二種（ＡＴ）" => "regular_second_at",
                    "中型二種" => "middle_second",
                    "大型二種" => "large_second",
                    "大型特殊" => "large_special"           
                    ], 
                'remarks' => 'メールの免許種類の関連付け（HP）'
            ],
            [
                'key' => 'mail.licence_type_map_of_dse',
                'val' => [
                    "普通免許" => "regular_mt",
                    "普通免許MT" => "regular_mt",
                    "普通免許AT限定" => "regular_at",
                    "準中型免許" => "pre_middle",
                    "中型免許" => "middle",
                    "大型免許" => "large",
                    "普通二輪免許" => "regular_bike_mt",
                    "普通二輪免許MT" => "regular_bike_mt",
                    "普通二輪免許AT限定" => "regular_bike_at",
                    "普通二輪免許小型限定" => "small_bike_mt",
                    "普通二輪免許小型AT限定" => "small_bike_at",
                    "小型二輪免許" => "small_bike_mt",
                    "小型二輪免許MT" => "small_bike_mt",
                    "小型二輪免許AT限定" => "small_bike_at",
                    "大型二輪免許" => "large_bike_mt",
                    "大型二輪免許MT" => "large_bike_mt",
                    "大型二輪免許AT限定" => "large_bike_at",
                    "普通二種免許" => "regular_second_mt",
                    "普通二種免許MT" => "regular_second_mt",
                    "普通二種免許AT限定" => "regular_second_at",
                    "中型二種免許" => "middle_second",
                    "大型二種免許" => "large_second",
                    "大型特殊免許" => "large_special",
                    "けん引免許" => "tow"          
                    ], 
                'remarks' => 'メールの免許種類の関連付け（DSENET）'
            ],
            [
                'key' => 'mail.licence_type_map_of_tokyomenkyo',
                'val' => [
                        "普通一種" => "regular_tba",
                        "準中型" => "pre_middle",
                        "中型一種" => "middle",
                        "大型一種" => "large",
                        "普通二輪" => "regular_bike_tba",
                        "小型二輪" => "small_bike_tba",
                        "大型二輪" => "large_bike_tba",
                        "普通二種" => "regular_second_tba",
                        "中型二種" => "middle_second",
                        "大型二種" => "large_second",
                        "大型特殊" => "large_special",
                        "大型特種" => "large_special"           
                    ], 
                'remarks' => 'メールの免許種類の関連付け（東京で免許）'
            ],                        
            [
                'key' => 'mail.licence_type_map_of_gomenkyo',
                'val' => [
                        "普通車" => "regular_mt",
                        "普通車MT" => "regular_mt",
                        "普通車AT" => "regular_at",
                        "準中型車" => "pre_middle",
                        "中型車" => "middle",
                        "大型車" => "large",
                        "普通二輪" => "regular_bike_mt",
                        "普通二輪(MT)" => "regular_bike_mt",
                        "普通二輪(AT)" => "regular_bike_at",
                        "小型二輪" => "small_bike_mt",
                        "小型二輪(MT)" => "small_bike_mt",
                        "小型二輪(AT)" => "small_bike_at",
                        "大型二輪" => "large_bike_mt",
                        "大型二輪(MT)" => "large_bike_mt",
                        "大型二輪(AT)" => "large_bike_at",
                        "普通二種" => "regular_second_mt",
                        "普通二種(MT)" => "regular_second_mt",
                        "普通二種(AT)" => "regular_second_at",
                        "中型二種" => "middle_second",
                        "大型二種" => "large_second",
                        "大型特殊" => "large_special",                        
                        "その他" => "unknown"      
                    ], 
                'remarks' => 'メールの免許種類の関連付け（教習所へ行こう）'
            ],                        
            [
                'key' => 'mail.parser_entry_types_object',
                'val' => [                    
                    'inquiry' => ['use' => true, 'name' => 'inquiry', 'short' => '問い合せ'],
                    'brochure' => ['use' => true, 'name' => 'brochure', 'short' => '資料請求'],
                    'tmp_entering' => ['use' => true, 'name' => 'tmp_entering', 'short' => '仮申込み'],
                    ], 
                'remarks' => 'エントリー種別'
            ],
            [
                'key' => 'mail.parser_status_types_object',
                'val' => [
                    'ongoing' => '対応中',
                    'reacted' => '対応済み',                    
                    'entered' => '入所済み',
                    'no_action' => '対応不要',
                    ], 
                'remarks' => 'ステータス種別'
            ],
            [
                'key' => 'mail.parser_via_routes_types_object',
                'val' => [
                    'hp' => 'ホームページ',
                    'dss' => '教習所サーチ',
                    'dse' => 'DSENET',
                    'tokyomenkyo' => '東京で免許',
                    'gomenkyo' => '教習所へ行こう',
                    'etc' => 'その他',
                    ], 
                'remarks' => '経路種別'
            ],
            [
                'key' => 'db_config.pdo_info',
                'val' => [
                    'db_name' => 'laravel',
                    'db_host' => 'wcors-test.gcpdb.wcloud.jp',
                    'charset' => 'utf8mb4'
                    ], 
                'remarks' => 'PDOでのデータベース接続用情報'
            ],
            ['key' => 'db_config.crm_using_flag','val' => true, 'remark' => 'CRM利用フラグ'],
            [
                'key' => 'db_config.crm_using_item',
                'val' => [
                    'entry_type',
                    'entry_date',
                    'entry_title',
                    'entry_content',
                    'licence_type',
                    'student_no',
                    'name',
                    'email',
                    'zipcode',
                    'address_pref',
                    'address_city',
                    'address_town',
                    'tel',
                    'age',
                    'birthday',
                    'school',
                    'sex',
                    'occupation',
                    'school',
                    'via_routes',
                    'general1',
                    'react_status',
                    'react_date',
                    'entered_date',
                    'dm_flag',
                    'zipcode_level_address'
                ], 
                'remarks' => 'CRM利用項目'
            ],
            [
                'key' => 'db_config.freeword_search_target',
                'val' => ["name","tel","email","general1"], 
                'remarks' => 'フリーワード検索対象カラム'
            ],
            [
                'key' => 'db_config.status_en_jp_map',
                'val' => [
                        ['ongoing' => '対応中'],
                        ['reacted' => '対応済み'],
                        ['entered' => '入所済み'],
                        ['no_action' => '対応不要']
                    ], 
                'remarks' => '対応状況（ステータス）EN-JPマップ'
            ],
            [
                'key' => 'db_config.via_en_jp_map',
                'val' => [
                        ['direct' => '直接来校'],
                        ['hp' => 'ホームページ'],
                        ['corp' => '生協'],
                        ['shikoren' => '私校連'],
                        ['alliance' => '提携校'],
                        ['dss' => '教習所サーチ'],
                        ['dse' => 'DSENET'],
                        ['tokyomenkyo' => '東京で免許'],
                        ['gomenkyo' => '教習所へ行こう'],                        
                        ['sh' => 'S&H'],
                        ['agency' => '紹介所'],
                        ['gp' => 'GP'],
                        ['training' => '養成'],
                        ['etc' => 'その他'],
                    ], 
                'remarks' => '経路マスタの関連付け EN-JPマップ'
            ],
            [
                'key' => 'db_config.entry_types_en_jp_map',
                'val' => [
                        ['inquiry' => '問い合わせ'],
                        ['brochure' => '資料請求'],
                        ['tmp_entering' => '仮入所申し込み'],
                        ['other' => 'その他'],
                    ], 
                'remarks' => 'エントリータイプ全リスト'
            ],
            [
                'key' => 'db_config.licence_types_en_jp_map',
                'val' => [
                        ["regular_at" => "普通免許AT"],
                        ["regular_mt" => "普通免許MT"],
                        ["regular_tba" => "普通免許（AT/MT未定）"],
                        ["pre_middle" => "準中型免許"],
                        ["middle" => "中型免許"],
                        ["large" => "大型免許"],
                        ["regular_bike_at" => "普通二輪免許AT"],
                        ["regular_bike_mt" => "普通二輪免許MT"],
                        ["regular_bike_tba" => "普通二輪免許（AT/MT未定）"],
                        ["small_bike_at" => "小型二輪免許AT"],
                        ["small_bike_mt" => "小型二輪免許MT"],
                        ["small_bike_tba" => "小型二輪免許（AT/MT未定）"],
                        ["large_bike_at" => "大型二輪免許AT"],
                        ["large_bike_mt" => "大型二輪免許MT"],
                        ["large_bike_tba" => "大型二輪免許（AT/MT未定）"],
                        ["regular_second_at" => "普通二種免許AT"],
                        ["regular_second_mt" => "普通二種免許MT"],
                        ["regular_second_tba" => "普通二種免許（AT/MT未定）"],
                        ["middle_second" => "中型二種免許"],
                        ["large_second" => "大型二種免許"],
                        ["large_special" => "大型特殊免許"],                        
                        ["unknown" => "不明"]                        
                    ], 
                'remarks' => '免許種別全リスト'
            ],
            [
                'key' => 'db_config.history_types_en_jp_map',
                'val' => [
                        ['reply_to_inquiry' => 'メール送信'],
                        ['send_campaign' => 'CPメール'],
                        ['reply_to_tmp_entering' => 'メール送信'],
                        ['memo' => '対応メモ'],
                        ['inquiry' => '問合わせ'],
                        ['brochure' => '資料請求'],
                        ['tmp_entering' => '仮申し込み'],
                        ['general_mail' => 'メール送信'],
                        ['received_mail' => 'メール受信'],
                        ['auto_sending' => '自動送信'],
                        ['send_brochure' => '資料送付'],
                    ], 
                'remarks' => '履歴テーブルタイプリスト'
            ],
            [
                'key' => 'db_config.customer_common_data_list',
                'val' => [                    
                    'name',
                    'kana',
                    'email',
                    'student_no',
                    'address_pref',
                    'address_city',
                    'address_town',
                    'birthday',
                    'tel',
                    'tel2',
                    'age',
                    'sex',
                    'occupation',
                    'school',
                    'general1',
                    'general2',                    
                    'dm_flag',
                    ],
                'remarks' => 'エントリーデータ更新時に同一メールアドレスの場合に一括更新されなければならない項目',
            ]
        ];

        $callback = function (&$item) {
            $item['val'] = json_encode($item['val'], JSON_UNESCAPED_UNICODE);
        };

        array_walk($insert_array, $callback);
        Option::insert($insert_array);    
    
    }
}
