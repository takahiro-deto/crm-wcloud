<?php

namespace Site_keiseids_wcors_wcloud_jp;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $this->call(UsersTableSeeder::class);        
        $this->call(OptionsTableSeeder::class);
        $this->call(MailBoxesTableSeeder::class);
    }
}
