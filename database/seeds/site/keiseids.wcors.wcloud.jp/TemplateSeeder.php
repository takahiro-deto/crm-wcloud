<?php

namespace Site_keiseids_wcors_wcloud_jp;

use Illuminate\Database\Seeder;
use App\Models\Template;

use DB;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('templates')->delete();

        $content_1 = <<<EOM
わくわくドライビングスクールです。
資料は到着しましたでしょうか？
EOM;

        $content_2 = <<<EOM
わくわくドライビングスクールです。
資料は到着しましたでしょうか？2
EOM;

        $content_3 = <<<EOM
わくわくドライビングスクールです。
お待ちしております。
EOM;

        $content_4 = <<<EOM
わくわくドライビングスクールです。
10,000円キャッシュバックしてます。
EOM;

    	$templates = [
    		[
                'template_name'             => 'テンプレート１',
                'auto_sending_interval'     => '3',
                'auto_sending_flag'         => '1',
                'purpose'                   => '0',
                'mail_title'                => '【わくわくDS】資料到着確認',
                'mail_content'              => $content_1,
    		],
    		[
    			'template_name'             => 'テンプレート２',
                'auto_sending_interval'     => '3',
                'auto_sending_flag'         => '1',
                'purpose'                   => '0',
                'mail_title'                => '【わくわくDS】資料到着確認2',
                'mail_content'              => $content_2,
    		],
    		[
    			'template_name'             => 'テンプレート３',
                'auto_sending_interval'     => '0',
                'auto_sending_flag'         => '0',
                'purpose'                   => '1',
                'mail_title'                => '【わくわくDS】入校手続きのご案内',
                'mail_content'              => $content_3,
    		],
    		[
    			'template_name'             => 'テンプレート４',
                'auto_sending_interval'     => '0',
                'auto_sending_flag'         => '0',
                'purpose'                   => '1',
                'mail_title'                => '【わくわくDS】キャンペーンのご案内',
                'mail_content'              => $content_4,
    		],
    	];

    	foreach($templates as $template)
    	{
    		Template::create($template);
    	}
        
    }
}
