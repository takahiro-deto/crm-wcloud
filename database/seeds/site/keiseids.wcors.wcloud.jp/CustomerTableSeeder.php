<?php

namespace Site_keiseids_wcors_wcloud_jp;

use Illuminate\Database\Seeder;

use App\Models\Customer;
use App\Models\Entry;

use DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();

        $base_lng_lat_set = [
        	["35.3952","139.4119"],
			["35.3948","139.4057"],
			["35.3930","139.4109"],
			["35.3921","139.4139"],
			["35.3946","139.4232"],
			["35.4007","139.4252"],
			["35.3950","139.4317"],
			["35.3932","139.4346"],
        ];

        $lng_lat_set = [];

        foreach($base_lng_lat_set as $key => $val)
        {            
        	for($i = 0; $i < 240; $i++)
        	{
        		$lat = $val[0];
        		$lng = $val[1];

        		$lat = round($lat - (mt_rand() / 10**10),4);
        		$lng = round($lng - (mt_rand() / 10**10),4);

        		$lng_lat_set[] = [$lat,$lng];
        	}
        }        
        
        // DB::table('entries')->orderBy('id','DESC')->chunk(2000,function($users) use ($lng_lat_set){

        //     $k = 1;

        //     foreach($users as $user)
        //     {
        //         $email = $user->email;
        //         $address_pref = $user->address_pref;
        //         $address_city = $user->address_city;
        //         $address_town = $user->address_town;

        //         Customer::create([
        //             'email' => $email,
        //             'address_pref' => $address_pref,
        //             'address_city' => $address_city,
        //             'address_town' => $address_town,
        //             'lat' => $lng_lat_set[$k - 1][0],
        //             'lng' => $lng_lat_set[$k - 1][1],
        //         ]);

        //         $k++;
        //     }
        // });        
    }
}
