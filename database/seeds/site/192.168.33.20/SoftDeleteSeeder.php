<?php

namespace Site_192_168_33_20;

use Illuminate\Database\Seeder;
use App\Models\Entry;

class SoftDeleteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {     	
     	DB::table('entries')->inRandomOrder()->chunk(500,function($users){
            foreach($users as $user)
            {
                $user->update(['react_status' => 'no_action']);
                $user->delete();
            }
        });
    }
}
