<?php

namespace Site_192_168_33_20;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Entry;
use App\Models\History;

class EntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('entries')->delete();

        // Dummy data
    	$univ = ['大谷大学','京都文教大学','京都医療科学大学','京都華頂大学','京都看護大学','京都外国語大学','京都学園大学','京都教育大学','京都工芸繊維大学','京都産業大学','京都市立芸術大学','京都女子大学','京都精華大学','京都造形芸術大学','京都橘大学','京都大学','京都ノートルダム女子大学','京都美術工芸大学','京都府立医科大学','京都府立大学','京都薬科大学','京都光華女子大学','嵯峨美術大学','種智院大学','同志社女子大学','同志社大学','花園大学','福知山公立大学','佛教大学','平安女学院大学','明治国際医療大学','立命館大学','龍谷大学','武蔵大学','天理大学','神戸松蔭女子学院大学','追手門学院大学'];
    	$occp = ['会社員','会社役員','公務員','自営業','大学生','パート・アルバイト','無職','専門学生'];
    	$routes = ['直接来所','ホームページ','教習所サーチ','生協','DM'];


    	$etype = ['inquiry','brochure','tmp_entering','other'];
    	$lics  = ['regular_mt','regular_at','regular_tba','regular_bike_mt','regular_bike_at','regular_bike_tba'];


    	// Read Target File
    	$paths = [
    		dirname(__FILE__) . "/csv/personal_infomation_1.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_2.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_3.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_4.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_5.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_6.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_7.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_8.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_9.csv",
    		// dirname(__FILE__) . "/csv/personal_infomation_10.csv",
    	];        


    	foreach($paths as $path){    		

	        $file = new \SplFileObject($path);
	        $file->setFlags(\SplFileObject::READ_CSV);				       
	        $lines = new \LimitIterator($file,1);

	        foreach($lines as $line)
	        {
	        	if(!is_null($line[0]))
	        	{	        		
	                $student_no     = mt_rand(0,100000);	                

	                $name           = $line[1];
	                $kana           = $line[2];
	                $sex            = $line[3];
	                $tel            = $line[4];
	                $tel2           = $line[5];
	                $email          = $line[6];
	                $zipcode        = strval(str_replace('-','',$line[7]));	                
	                $address_pref   = strval($line[8]);
	                $address_city   = strval($line[9]);
	                $zipcode_level_address = DB::table('coredb.zipcodes')->select('kanji_address')->where('zipcode',$zipcode)->first()->kanji_address;
	                $address_town   = strval($line[10]) . strval($line[11]) . strval($line[12]);
		            $birthday       = strval($line[13]);
		            $age            = strval($line[14]);

		            $school     = $univ[mt_rand(0,count($univ) - 1)];
		            $occupation = $occp[mt_rand(0,count($occp) - 1)];
		            $via_routes = $routes[mt_rand(0,count($routes) - 1)];

		            $dm_flag    = intval($line[0]) % 5 == 0 ? '0' : '1';


		            $entry_date   = Carbon::now()->subWeeks(mt_rand(3,30))->toDateString();		            
		            $entry_type   = $etype[mt_rand(0,count($etype) - 1)];
		            $licence_type = $lics[mt_rand(0,count($lics) - 1)];		            


		            if(intval($line[0]) % 8 == 0){
		            	$react_date       = Carbon::parse($entry_date)->addDays(mt_rand(0,10))->toDateString(); 
		            	$react_status     = 'reacted';
		            	
		            	$entry_id = DB::table('entries')->insertGetId([
                            'student_no'                => $student_no,
                            'name'                      => $name,
                            'kana'                      => $kana,
                            'email'                     => $email,
                            'zipcode'                   => $zipcode,
                            'address_pref'              => $address_pref,
                            'address_city'              => $address_city,
                            'zipcode_level_address'     => $zipcode_level_address,
                            'address_town'              => $address_town,
                            'tel'                       => $tel,
                            'tel2'                      => $tel2,
                            'age'                       => $age,
                            'birthday'                  => $birthday,
                            'sex'                       => $sex,
                            'school'                    => $school,
                            'occupation'                => $occupation,
                            'via_routes'                => $via_routes,
                            'dm_flag'                   => $dm_flag,
                            'entry_type'                => $entry_type,
                            'entry_date'                => $entry_date,
                            'react_status'              => $react_status,
                            'licence_type'              => $licence_type,
                            'react_status'              => $react_status,
                            'react_date'                => $react_date,
			            ]);


			            switch($entry_type){
			            	case 'inquiry':
			            		Entry::where('id',$entry_id)->update([
			            			'entry_title' => 'テストメール',
			            			'entry_content' => 'これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。',
			            		]);
			            		$email = Entry::where('id',$entry_id)->first()->email;
			            		History::create([
			            			// 'entry_id' => $entry_id,
			            			'email' => $email,
			            			'history_type' => 'received_mail',
			            			'history_title' => 'テストメール',
			            			'history_content' => 'これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。',
			            			'history_date' => Carbon::now()->toDateString(),
			            		]);
			            		History::create([
			            			// 'entry_id' => $entry_id,
			            			'email' => $email,
			            			'history_type' => 'has_got_inquiry',
			            			'history_title' => 'テスト問い合わせ',
			            			'history_content' => 'これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。',
			            			'history_date' => Carbon::now()->toDateString(),
			            		]);

			            		break;
			            	case 'brochure':
			            		break;
			            	case 'tmp_entering':
			            		break;
			            	default:
			            		break;
			            }

		            }else if(intval($line[0]) % 190 == 0){

		            	$react_date       = Carbon::parse($entry_date)->addDays(mt_rand(0,10))->toDateString(); 
		            	$react_status     = 'entered';
		            	
		            	$entry_id = DB::table('entries')->insertGetId([
                            'student_no'                => $student_no,
                            'name'                      => $name,
                            'kana'                      => $kana,
                            'email'                     => $email,
                            'zipcode'                   => $zipcode,
                            'address_pref'              => $address_pref,
                            'address_city'              => $address_city,
                            'zipcode_level_address'     => $zipcode_level_address,
                            'address_town'              => $address_town,
                            'tel'                       => $tel,
                            'tel2'                      => $tel2,
                            'age'                       => $age,
                            'birthday'                  => $birthday,
                            'sex'                       => $sex,
                            'school'                    => $school,
                            'occupation'                => $occupation,
                            'via_routes'                => $via_routes,
                            'dm_flag'                   => $dm_flag,
                            'entry_type'                => $entry_type,
                            'entry_date'                => $entry_date,
                            'entered_date'              => $react_date,
                            'react_status'              => $react_status,
                            'licence_type'              => $licence_type,
                            'react_status'              => $react_status,
                            'react_date'                => $react_date,
			            ]);


			            switch($entry_type){
			            	case 'inquiry':
			            		Entry::where('id',$entry_id)->update([
			            			'entry_title' => 'テストメール',
			            			'entry_content' => 'これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。',
			            		]);
			            		$email = Entry::where('id',$entry_id)->first()->email;
			            		History::create([
			            			// 'entry_id' => $entry_id,
			            			'email' => $email,
			            			'history_type' => 'received_mail',
			            			'history_title' => 'テストメール',
			            			'history_content' => 'これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。',
			            			'history_date' => Carbon::now()->toDateString(),
			            		]);
			            		History::create([
			            			// 'entry_id' => $entry_id,
			            			'email' => $email,
			            			'history_type' => 'has_got_inquiry',
			            			'history_title' => 'テスト問い合わせ',
			            			'history_content' => 'これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。',
			            			'history_date' => Carbon::now()->toDateString(),
			            		]);

			            		break;
			            	case 'brochure':
			            		break;
			            	case 'tmp_entering':
			            		break;
			            	default:
			            		break;
			            }

		            }else{		            	
		            	$react_status     = 'ongoing';
		            	
		            	$entry_id = DB::table('entries')->insertGetId([
                            'student_no'                => $student_no,
                            'name'                      => $name,
                            'kana'                      => $kana,
                            'email'                     => $email,
                            'zipcode'                   => $zipcode,
                            'address_pref'              => $address_pref,
                            'address_city'              => $address_city,
                            'zipcode_level_address'     => $zipcode_level_address,
                            'address_town'              => $address_town,
                            'tel'                       => $tel,
                            'tel2'                      => $tel2,
                            'age'                       => $age,
                            'birthday'                  => $birthday,
                            'sex'                       => $sex,
                            'school'                    => $school,
                            'occupation'                => $occupation,
                            'via_routes'                => $via_routes,
                            'dm_flag'                   => $dm_flag,
                            'entry_type'                => $entry_type,
                            'entry_date'                => $entry_date,
                            'react_status'              => $react_status,
                            'licence_type'              => $licence_type,
			            ]);

			            switch($entry_type){
			            	case 'inquiry':
			            		Entry::where('id',$entry_id)->update([
			            			'entry_title' => 'テストメール',
			            			'entry_content' => 'これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。',
			            		]);
			            		$email = Entry::where('id',$entry_id)->first()->email;
			            		History::create([			            			
			            			// 'entry_id' => $entry_id,
			            			'email' => $email,
			            			'history_type' => 'received_mail',
			            			'history_title' => 'テストメール',
			            			'history_content' => 'これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。これはテストメールです。',
			            			'history_date' => Carbon::now()->toDateString(),
			            		]);
			            		History::create([
			            			// 'entry_id' => $entry_id,
			            			'email' => $email,
			            			'history_type' => 'has_got_inquiry',
			            			'history_title' => 'テスト問い合わせ',
			            			'history_content' => 'これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。これはテスト問い合わせです。',
			            			'history_date' => Carbon::now()->toDateString(),
			            		]);

			            		break;
			            	case 'brochure':
			            		break;
			            	case 'tmp_entering':
			            		break;
			            	default:
			            		break;
			            }       
		            }	            
	        	}
	        
	    	}
	    }	    
    }
}
