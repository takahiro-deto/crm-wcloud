<?php

namespace Site_192_168_33_20;

use Illuminate\Database\Seeder;
use App\Models\MailBox;
use Crypt;

class MailBoxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MailBox::insert([
            'mail_box' => '{sv2022.xserver.jp:993/imap/ssl/novalidate-cert}',
            'username' => 'ndk@newdriver.co.jp',
            'password' => Crypt::encrypt('knG7vAhqCR'),
        ]);
    }
}
