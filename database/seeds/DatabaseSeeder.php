<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserDatabasesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(HistoriesTableSeeder::class);
        $this->call(ZipcodesTableSeeder::class);
        $this->call(EntriesTableSeeder::class);        
        $this->call(TemplateSeeder::class);        
        $this->call(OptionsTableSeeder::class);        
        $this->call(MailBoxesTableSeeder::class);
        $this->call(ZipsTableSeeder::class);        
        $this->call(CustomerTableSeeder::class);
        $this->call(SoftDeleteSeeder::class);        
    }
}
