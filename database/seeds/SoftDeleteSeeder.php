<?php

use Illuminate\Database\Seeder;
use App\Models\Entry;

class SoftDeleteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {     	     	
        foreach(DB::table('entries')->inRandomOrder()->limit(500)->get() as $user)
        {
            Entry::where('id',$user->id)->update(['react_status' => 'no_action']);
            Entry::where('id',$user->id)->delete();                
        }        
    }
}
