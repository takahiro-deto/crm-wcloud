<?php

namespace Product;

use Illuminate\Database\Seeder;
use App\Models\UserDatabase;
use Crypt;

class UserDatabasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserDatabase::insert([
            [
                'web_host' => 'gcp.demo.wcloud.jp',
                'db_name' => 'laravel',
                'db_host' => 'wcors-test.gcpdb.wcloud.jp',
                'db_user' => 'wakuwaku',
                'db_password' => Crypt::encrypt('wakuwaku'),
            ],
            // [
            //     'web_host' => 'newdriver.wcors.wcloud.jp',
            //     'db_name' => 'newdriver',
            //     'db_host' => 'wcors-cluster.cluster-cvxspzt4c12u.ap-northeast-1.rds.amazonaws.com',
            //     'db_user' => 'wcors',
            //     'db_password' => Crypt::encrypt('wkwkWcors11'),
            // ]
        ]);
    }
}
