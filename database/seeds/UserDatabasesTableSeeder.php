<?php

use Illuminate\Database\Seeder;
use App\Models\UserDatabase;

class UserDatabasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coredb.user_databases')->delete();
        
        UserDatabase::insert([ 
            [
                'web_host' => '192.168.33.20',
                'db_name' => 'laravel',
                'db_host' => '192.168.33.20',
                'db_user' => 'wakuwaku',
                'db_password' => Crypt::encrypt('wakuwaku'),
            ],            
            [
                'web_host' => 'demo.gcp.wcloud.jp',
                'db_name' => 'laravel',
                'db_host' => 'wcors.gcpdb.wcloud.jp',
                'db_user' => 'wakuwaku',
                'db_password' => Crypt::encrypt('wakuwaku'),
            ],
            // [
            //     'web_host' => 'newdriver.gcp.wcloud.jp',
            //     'db_name' => 'newdriver',
            //     'db_host' => 'wcors.gcpdb.wcloud.jp',
            //     'db_user' => 'wakuwaku',
            //     'db_password' => Crypt::encrypt('wakuwaku'),
            // ],
            [
                'web_host' => 'keiseids.wcors.wcloud.jp',
                'db_name' => 'keiseids',
                'db_host' => 'wcors.gcpdb.wcloud.jp',
                'db_user' => 'wakuwaku',
                'db_password' => Crypt::encrypt('wakuwaku'),
            ],
            
        ]);
    }
}
