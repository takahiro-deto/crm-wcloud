<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::insert([
        	[
                'name'              => 'WAKUWAKU_ADMIN',
                'email'             => 'crm@wkwk.co.jp',
                'password'          => Hash::make('wkwktest'),
                'login_id'          => 'wakuwaku_admin',
                'role_dashboard'    => 1,
                'role_entries'      => 1,
                'role_campaign'     => 1,
                'role_templates'    => 1,
                'role_csv'          => 1,
            ],
            [
                'name'              => 'WAKUWAKU',
                'email'             => 'wcors@wkwk.co.jp',
                'password'          => Hash::make('wkwktest'),
                'login_id'          => 'wakuwaku',
                'role_dashboard'    => 1,
                'role_entries'      => 1,
                'role_campaign'     => 1,
                'role_templates'    => 1,
                'role_csv'          => 0,
            ],
            [
                'name'              => 'マネージャー',
                'email'             => 'wcors2@wkwk.co.jp',
                'password'          => Hash::make('test'),
                'login_id'          => 'manager',
                'role_dashboard'    => 1,
                'role_entries'      => 1,
                'role_campaign'     => 1,
                'role_templates'    => 0,
                'role_csv'          => 0,
            ],
            [
                'name'              => '一般ユーザー',
                'email'             => 'wcors3@wkwk.co.jp',
                'password'          => Hash::make('test'),
                'login_id'          => 'user',
                'role_dashboard'    => 0,
                'role_entries'      => 0,
                'role_campaign'     => 0,
                'role_templates'    => 0,
                'role_csv'          => 0,
            ],
        ]);

        // User::insert([
            // [
            //     'name' => 'WAKUWAKU',
            //     'email' => 'wcors@wkwk.co.jp',
            //     'password' => Hash::make('WcAdmKds!'),
            //     'login_id' => 'wakuwaku',

            //     'role_dashboard'    => 1,
            //     'role_entries'      => 1,
            //     'role_campaign'     => 1,
            //     'role_templates'    => 1,
            //     'role_csv'          => 1,
            // ],
            // [
            //     'name' => 'マネージャー',
            //     'email' => 'kds-crm@keisei-ds.co.jp',
            //     'password' => Hash::make('KDmnp#37'),
            //     'login_id' => 'manager',

            //     'role_dashboard'    => 1,
            //     'role_entries'      => 1,
            //     'role_campaign'     => 1,
            //     'role_templates'    => 1,
            //     'role_csv'          => 1,
            // ],
            // [
            //     'name' => '一般ユーザー',
            //     'email' => 'kds-crm@keisei-ds.co.jp',
            //     'password' => Hash::make('TDakf@26'),
            //     'login_id' => 'user',

            //     'role_dashboard'    => 0,
            //     'role_entries'      => 0,
            //     'role_campaign'     => 0,
            //     'role_templates'    => 0,
            //     'role_csv'          => 0,
            // ],
        // ]);
    }
}
