<?php

use Illuminate\Database\Seeder;
use App\Models\MailBox;

class MailBoxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MailBox::insert([
            'mail_box' => '{sv1432.xserver.jp:993/imap/ssl/novalidate-cert}',
            'username' => 'wcors@whp.wcloud.jp',
            'password' => Crypt::encrypt('wakuwaku'),
        ]);

        // MailBox::insert([
        //     'mail_box' => '{keisei-ds.sakura.ne.jp:993/imap/ssl/novalidate-cert}',
        //     'username' => 'kds-crm@keisei-ds.co.jp',
        //     'password' => Crypt::encrypt('vSdg8dNw3H'),
        // ]);
    }
}
