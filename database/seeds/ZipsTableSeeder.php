<?php

use Illuminate\Database\Seeder;
use App\Models\Zip;
use League\Csv\Reader;

class ZipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = [
            0 => [
                'path' => 'KEN_ALL.CSV',
                'column' => ['jis', 'zip_old', 'zip', 'pref_kana', 'city_kana', 'town_kana', 'pref', 'city', 'town', 'flg1', 'flg2', 'flg3', 'flg4', 'flg5', 'reason'],
                'scope' => true,
            ],
            1 => [
                'path' => 'JIGYOSYO.CSV',
                'column' => ['jis', 'company_kana', 'company', 'pref', 'city', 'town', 'street', 'zip', 'zip_old', 'post_office', 'flg6', 'multi'],
                'scope' => false,
            ],
        ];

        DB::beginTransaction();

        try {
            Zip::truncate();

            foreach ($config as $type => $info) {
                // Strageのpathを作成＆存在確認
                $path = storage_path('app/zip/' . $info['path']);
                if (! File::exists($path)) {
                    continue;
                }

                // CSV Open
                $csv = new SplFileObject($path);
                $csv->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY);
                $reader = Reader::createFromFileObject($csv);
                $reader->setInputEncoding('CP932');
                $records = $reader->fetchAssoc($info['column'], function ($row)
                {
                    foreach ($row as &$str) {
                        $str = mb_convert_encoding($str, 'UTF-8', 'CP932');
                    };
                    return $row;
                });

                // レコード登録
                $join = false;
                $zip = '';
                foreach ($records as $record) {
                    if ($join && $zip == $record['zip']) {
                        // 2行に渡る場合の処理
                        $town .= $record['town'];
                        if (! str_contains($town, '）')) {
                            continue;
                        }
                    } else {
                        // 通常処理
                        $zip = $record['zip'];
                        $pref = $record['pref'];
                        $city = $record['city'];
                        $town = $record['town'] . ($record['town2'] ?? '');

                        // 括弧がある場合の処理
                        if ($info['scope'] && str_contains($town, '（')) {
                            // 閉じ括弧がない場合は2行目に続く
                            if (! str_contains($town, '）')) {
                                $join = true;
                                continue;
                            }
                        }
                    }

                    $join = false;

                    // townから括弧を分離
                    $scope = '';
                    if ($info['scope']) {
                        if (preg_match('/(.+)（(.+)）/', $town, $matches)) {
                            $town = $matches[1];
                            $scope = $matches[2];
                        }
                    }

                    Zip::insert([
                        'zip' => $zip,
                        'type' => $type,
                        'pref' => $pref,
                        'city' => $city,
                        'town' => $town,
                        'scope' => $scope,
                    ]);
                }

            }
            DB::commit();
            $this->command->info('zip data imported.');

        } catch (Exception $e) {
            DB::rollback();
            $this->command->error($e->getMessage());
        }
    }
}
