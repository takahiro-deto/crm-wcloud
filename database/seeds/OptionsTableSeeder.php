<?php

use Illuminate\Database\Seeder;
use App\Models\Option;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('options')->delete();        

    	$insert_array = [    		
    		[
    			'key' => 'base.school_key',
    			'val' => 'wkwk', 
    			'remarks' => '教習所毎のキー'
    		],    		
    		[
    			'key' => 'base.school_name',
    			'val' => 'わくわくドライビングスクール', 
    			'remarks' => '教習所名'
    		],    		
    		[
    			'key' => 'base.school_address',
    			'val' => [
    				'zipcode' => '1500011',
    				'address' => '東京都渋谷区東1-2-23 MA東ビル5F',
    				'lat'     => '35.311188',
    				'lng'     => '139.313478',
    				], 
    			'remarks' => '教習所の所在地データ'
    		],    		
    		[
    			'key' => 'base.school_entering_type',
    			'val' => '入所', 
    			'remarks' => '入校か入所か'
    		],    		
    		[
    			'key' => 'common.general',
    			'val' => [
    				'general1' => 'メモ（検索対象）',
    				'general2' => '汎用2',
    				'general3' => '汎用3',
    				'general4' => '汎用4',
    				'general5' => '汎用5',
    				'general6' => '汎用6',
    				'general7' => '汎用7',
    				'general8' => '汎用8',
    				'general9' => '汎用9',
    				'general10' => '汎用10',
    				],
    			'remarks' => '汎用項目'
    		],    		
    		[
    			'key' => 'common.google_api_key',
    			'val' => [
    				'map' => 'AIzaSyCh5YPrrlLSo6WEz4uKvb4Qi4zEOggo9LU',
    				'geo' => 'AIzaSyBPrzHMR4fqtDeZtBGARB_u-01xn1AKP_I',
    				], 
    			'remarks' => 'google map apiのキー'
    		],	
    		[
    			'key' => 'view.columns_map.entry_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '対応日時', 'column' => 'react_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => 'エントリー一覧項目'
    		],
    		[
    			'key' => 'view.columns_map.inquiry_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '対応日時', 'column' => 'react_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => '問い合わせ一覧項目'
    		],    		
    		[
    			'key' => 'view.columns_map.brochure_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '対応日時', 'column' => 'react_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => '資料請求一覧項目'
    		],
    		[
    			'key' => 'view.columns_map.campaign_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '対応日時', 'column' => 'react_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => 'キャンペーンメール一覧項目'
    		],
    		[
    			'key' => 'view.columns_map.tmp_entering_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '対応日時', 'column' => 'react_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => '仮申し込み一覧項目'
    		],
    		[
    			'key' => 'view.columns_map.entering_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '入所状況', 'column' => 'entered_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => '入所確認作業一覧項目'
    		],    		
    		[
    			'key' => 'view.columns_map.no_action_list',
    			'val' => [
    					['name' => '種別', 'column' => 'entry_type'],
    					['name' => 'エントリー日時', 'column' => 'entry_date'],
    					['name' => '氏名', 'column' => 'name'],
    					['name' => 'メールアドレス', 'column' => 'email'],
    					['name' => '郵便番号', 'column' => 'zipcode'],
    					['name' => '住所', 'column' => 'address'],
    					['name' => '免許種類', 'column' => 'licence_type'],
    					['name' => '対応日時', 'column' => 'react_date'],
    					['name' => '状況', 'column' => 'react_status'],    					
    				], 
    			'remarks' => '対応不要一覧項目'
    		],    		
    		[
                'key' => 'view.columns_map.csv',
                'val' => [
                    ['name' => '種別', 'column' => 'entry_type'],
                    ['name' => 'エントリー日時', 'column' => 'entry_date'],
                    ['name' => '免許種類', 'column' => 'licence_type'],
                    ['name' => '氏名', 'column' => 'name'],
                    ['name' => '氏名カナ', 'column' => 'kana'],
                    ['name' => 'メールアドレス', 'column' => 'email'],
                    ['name' => '郵便番号', 'column' => 'zipcode'],
                    ['name' => '住所', 'column' => 'address'],
                    ['name' => '電話番号', 'column' => 'tel'],
                    ['name' => '電話番号2', 'column' => 'tel2'],
                    ['name' => '年齢', 'column' => 'age'],
                    ['name' => '性別', 'column' => 'sex'],
                    ['name' => '学校', 'column' => 'school'],
                    ['name' => 'メモ', 'column' => 'general1'],
                    ['name' => '対応日時', 'column' => 'react_date'],
                    ['name' => '入所日', 'column' => 'entered_date'],
                    ['name' => '状況', 'column' => 'react_status'],
                    ['name' => 'DM', 'column' => 'dm_flag'],
                    ['name' => 'デバイス', 'column' => 'device'],
                ],
                'remarks' => 'CSV項目',
            ],
    		[
    			'key' => 'view.master.entry_types',
    			'val' => ['問い合わせ','資料請求','仮申し込み','その他'], 
    			'remarks' => 'エントリー種別マスタ（array）'
    		],
    		[
    			'key' => 'view.master.status',
    			'val' => ['対応中','対応済み','入所済み','対応不要'], 
    			'remarks' => '対応状況（ステータス）（array）'
    		],
    		[
    			'key' => 'view.master.licence_types',
    			'val' => ['普通免許MT','普通免許AT','普通免許（AT/MT未定）','普通二輪免許MT','普通二輪免許AT','普通二輪免許（AT/MT未定）'], 
    			'remarks' => '免許種類マスタ（array）'
    		],
    		[
    			'key' => 'view.master.via_routes',
    			'val' => ['直接来所','ホームページ','生協','教習所サーチ','その他'],
    			'remarks' => '経路マスタ（array）'
    		],		
    		[
    			'key' => 'view.master.db_licence_type_color',
    			'val' => [
    					['普通免許MT' => '#5ba330'],
    					['普通免許AT' => '#64af36'],
    					['普通免許（AT/MT未定）' => '#6cba3a'],
    					['普通二輪免許MT' => '#7ac745'],
    					['普通二輪免許AT' => '#97d866'],
    					['普通二輪免許（AT/MT未定）' => '#9dda6e'],
    				], 
    			'remarks' => 'ダッシュボード用免許種類カラー'
    		],
    		[
    			'key' => 'mail.sender_config',
    			'val' => [
    				'from' => 'test@wkwk.co.jp',
    				'name' => 'わくわくドライビングスクール',
    				'reply' => null,
    				'to' => 'test@wkwk.co.jp'
    				], 
    			'remarks' => 'CRMから送信されるメールのFROM'
    		],
    		[
    			'key' => 'mail.replace_string',
    			'val' => '$氏名$ 様\n', 
    			'remarks' => 'メールテンプレートに自動挿入する文章'
    		],
    		[
    			'key' => 'mail.parser_class',
    			'val' => ['DssMailParser','NewdriverMailParser'],
    			'remarks' => 'メール解析用クラス'
    		],
    		[
    			'key' => 'mail.licence_type_map_of_dss',
    			'val' => [
    				'普通免許（MT）' => 'regular_mt',
    				'普通免許（AT）' => 'regular_at',
    				'普通免許（AT\/MT未定）' => 'regular_tba',
    				'普通二輪免許（MT）' => 'regular_bike_mt',
    				'普通二輪免許（AT）' => 'regular_bike_at',
    				'普通二輪免許（AT\/MT未定）' => 'regular_bike_tba',
    				], 
    			'remarks' => 'メールの免許種類の関連付け（DSS）'
    		],
    		[
    			'key' => 'mail.licence_type_map_of_hp',
    			'val' => [
    				'普通免許MT' => 'regular_mt',
    				'普通免許AT' => 'regular_at',
    				'普通二輪免許MT' => 'regular_bike_mt',
    				'普通二輪免許AT' => 'regular_bike_at',    			
    				], 
    			'remarks' => 'メールの免許種類の関連付け（HP）'
    		],
    		[
    			'key' => 'mail.parser_entry_types_object',
    			'val' => [
    				'inquiry' => ['use' => true, 'name' => '問い合わせ', 'short' => '問い合せ'],
    				'brochure' => ['use' => true, 'name' => '資料請求', 'short' => '資料請求'],
    				'tmp_entering' => ['use' => true, 'name' => '仮申し込み', 'short' => '仮申込み'],
    				], 
    			'remarks' => 'エントリー種別'
    		],
    		[
    			'key' => 'mail.parser_status_types_object',
    			'val' => [
    				'ongoing' => '対応中',
    				'reacted' => '対応済み',
    				'student' => '対応済み（在校生）',
    				'entered' => '入所済み',
    				'no_action' => '対応不要',
    				], 
    			'remarks' => 'ステータス種別'
    		],
    		[
    			'key' => 'mail.parser_via_routes_types_object',
    			'val' => [
    				'hp' => 'ホームページ',
    				'dss' => '教習所サーチ',
    				'etc' => 'その他'
    				], 
    			'remarks' => 'エントリー種別'
    		],    		
    		['key' => 'db_config.crm_using_flag','val' => true, 'remark' => 'CRM利用フラグ'],
    		[
    			'key' => 'db_config.crm_using_item',
    			'val' => [
    				'entry_type',
    				'entry_date',
    				'licence_type',
    				'name',
    				'kana',
    				'email',
    				'student_no',
    				'address_pref',
    				'address_city',
    				'address_town',
    				'birthday',
    				'tel',
    				'tel2',
    				'age',
    				'sex',
    				'occupation',
    				'school',
    				'via_routes',
    				'general1',
    				'general2',
    				'react_status',
    				'react_date',
    				'entered_date',
    				'dm_flag',
    			], 
    			'remarks' => 'CRM利用項目'
    		],
    		[
    			'key' => 'db_config.freeword_search_target',
    			'val' => ['name','kana','tel','tel2','email','general1','general2'], 
    			'remarks' => 'フリーワード検索対象カラム'
    		],
    		[
    			'key' => 'db_config.status_en_jp_map',
    			'val' => [
	    				['ongoing' => '対応中'],
	    				['reacted' => '対応済み'],
	    				['student' => '対応済み（在校生）'],
	    				['entered' => '入所済み'],
	    				['no_action' => '対応不要']
    				], 
    			'remarks' => '対応状況（ステータス）EN-JPマップ'
    		],
    		[
    			'key' => 'db_config.via_en_jp_map',
    			'val' => [
	    				['hp' => 'ホームページ'],
	    				['dss' => '教習所サーチ'],
	    				['corp' => '生協'],
	    				['direct' => '直接来所'],
	    				['dm' => 'DM'],
	    				['etc' => 'その他']
    				], 
    			'remarks' => '経路マスタの関連付け EN-JPマップ'
    		],
    		[
    			'key' => 'db_config.entry_types_en_jp_map',
    			'val' => [
    					['inquiry' => '問い合わせ'],
    					['brochure' => '資料請求'],
    					['tmp_entering' => '仮入所申し込み'],
    					['other' => 'その他'],
    				], 
    			'remarks' => 'エントリータイプ全リスト'
    		],
    		[
    			'key' => 'db_config.licence_types_en_jp_map',
    			'val' => [
    					['regular_at' => '普通免許AT'],
    					['regular_mt' => '普通免許MT'],
    					['regular_tba' => '普通免許（AT/MT未定）'],
    					['regular_bike_at' => '普通二輪免許AT'],
    					['regular_bike_mt' => '普通二輪免許MT'],
    					['regular_bike_tba' => '普通二輪免許（AT/MT未定）'],
    				], 
    			'remarks' => '免許種別全リスト'
    		],
       		[
    			'key' => 'db_config.history_types_en_jp_map',
    			'val' => [
    					['reply_to_inquiry' => 'メール送信'],
    					['send_campaign' => 'CPメール'],
    					['reply_to_tmp_entering' => 'メール送信'],
    					['memo' => '対応メモ'],
    					['inquiry' => '問合わせ'],
                        ['brochure' => '資料請求'],
                        ['tmp_entering' => '仮申し込み'],
    					['general_mail' => 'メール送信'],
    					['received_mail' => 'メール受信'],
    					['auto_sending' => '自動送信'],
                        ['send_brochure' => '資料送付'],
    				], 
    			'remarks' => '履歴テーブルタイプリスト'
    		],
            [
                'key' => 'db_config.customer_common_data_list',
                'val' => [                    
                    'name',
                    'kana',
                    'email',
                    'student_no',
                    'address_pref',
                    'address_city',
                    'address_town',
                    'birthday',
                    'tel',
                    'tel2',
                    'age',
                    'sex',
                    'occupation',
                    'school',
                    'general1',
                    'general2',                    
                    'dm_flag',
                    ],
                'remarks' => 'エントリーデータ更新時に同一メールアドレスの場合に一括更新されなければならない項目',
            ]
    	];

    	$callback = function (&$item) {
            $item['val'] = json_encode($item['val'], JSON_UNESCAPED_UNICODE);
        };

        array_walk($insert_array, $callback);
        Option::insert($insert_array);    	
    }
}
