<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Login
 */
Route::post('/login','LoginController@login')->middleware('login_log');


/**
 * Entry Model
 */
Route::get('/fetch-dashboard-data','DashboardController@index');
Route::get('/fetch-dashboard-barchart-data','DashboardController@fetchBarChartData');
Route::get('/fetch-dashboard-configuration','DashboardController@fetchConfiguration');
Route::get('/fetch-no-action-tooltip','CommonApiController@fetchTooltipCounter');
Route::get('/fetch-users-list','CommonApiController@fetchUsersList');
Route::get('/fetch-specified-user-data','CommonApiController@fetchSpecifiedUserData');
Route::get('/fetch-specified-multi-users-data','CommonApiController@fetchSpecifiedMultiUsersData');
// ---------------------------------------------------------
Route::post('/create-user','CommonApiController@createUser');
// ---------------------------------------------------------
Route::put('/update-user','CommonApiController@updateUser');
Route::put('/update-deleted-user','CommonApiController@updateDeletedUser');
Route::put('/bulk-update-user','CommonApiController@bulkUpdateUser');
/*-----------------------------------------------------------*/

Route::post('/csv-upload-file','CommonApiController@uploadFile');

/**
 * Template Model
 */
Route::get('/fetch-template','MailTemplateController@fetchTemplate');
Route::get('/delete-mail-template','MailTemplateController@deleteMailTemplate');
// ---------------------------------------------------------
Route::post('/create-mail-template','MailTemplateController@createMailTemplate');
// ---------------------------------------------------------
Route::put('/update-mail-template','MailTemplateController@updateMailTemplate');
/*-----------------------------------------------------------*/


/**
 * Mail Model
 */
Route::post('/send-general-mail','CommonMailController@sendGeneralMail');
Route::post('/reply-to-inquiry','CommonMailController@replyToInquiry');
Route::post('/send-campaign','CommonMailController@sendCampaign');
Route::post('/reply-to-tmp-entering','CommonMailController@replyToTmpEntering');


/**
 * History Model
 */
Route::get('/fetch-respective-history','CommonApiController@fetchRespectiveHistory');
Route::post('/write-notes','CommonApiController@writeNotes');
Route::put('/update-notes','CommonApiController@updateNotes');


/**
 * Other
 */
Route::get('/fetch-config','CommonApiController@fetchConfig');
Route::get('/fetch-showable-columns','CommonApiController@fetchShowableColumns');