<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/crm/csv-download-area-data','CsvDownloadController@dashboardAreaData');
Route::get('/crm/csv-download-specified-users-data','CsvDownloadController@downloadSpecifiedUsersData');
Route::get('/crm/csv-download-import-fmt','CsvDownloadController@downloadImportFmt');

Route::get('/{any?}', function () { return view('index'); });
Route::get('/crm/{any?}', function () { return view('index'); });