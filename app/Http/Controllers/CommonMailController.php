<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\History;
use Carbon\Carbon;

use App\Mail\ReplyToInquiry;
use App\Mail\SendCampaign;
use App\Mail\ReplyToTmpEntering;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommonMailController extends Controller
{
    /**
     * send general mail
     */
    public function sendGeneralMail(Request $request)
    {        
        try{
            // メール送信  *** 対応不要リスト上でも対応するためwithTrashed
            $user = Entry::withTrashed()->where('id',$request->params['id'])->first();
            Mail::to($user)->send(new ReplyToInquiry($request->params));            


            // 履歴追加
            $email = Entry::withTrashed()->where('id',$request->params['id'])->first()->email;

            History::create([
                'email'             => $email,
                'history_type'      => 'general_mail',
                'history_date'      => Carbon::now()->toDateTimeString(),
                'history_title'     => $request->params['title'],
                'history_content'   => $request->params['content'],
            ]);                        
            
            return ['msg' => 'success'];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }     
    }


    /**
     * Reply To Inquiry
     */
    public function replyToInquiry(Request $request)
    {        
        try{
            // メール送信
            $user = Entry::where('id',$request->params['id'])->first();
            Mail::to($user)->send(new ReplyToInquiry($request->params));

            // ステータス更新
            $user->update([
                'react_status' => 'reacted',
                'react_date'   => Carbon::now()->format('Y-m-d'),
            ]);

            $email = $user->email;

            // 履歴追加
            History::create([
                'email'             => $email,
                'history_date'      => Carbon::now()->toDateTimeString(),
                'history_type'      => 'reply_to_inquiry',
                'history_title'     => $request->params['title'],
                'history_content'   => $request->params['content'],
            ]);                        
            

            return ['msg' => 'success'];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }


    /**
     * Send Campaign
     */
    public function sendCampaign(Request $request)
    {
        $users = "";
        $id_list = [];        

        try{
            if(empty($request->params['id']) && isset($request->params['condition']))
            {

                $condition = $request->params['condition'];

                if(isset($condition['freeword']))
                {
                    if($condition["react_status"] == "no_action")
                    {
                        $query = Entry::onlyTrashed()
                                ->entryType($condition['entry_type'])
                                ->licenceType($condition['licence_type'])
                                ->freeword($condition['freeword']);
                    }
                    else if($condition["need_no_action"]){
                        $query = Entry::withTrashed()
                                ->status($condition['react_status'])
                                ->entryType($condition['entry_type'])
                                ->licenceType($condition['licence_type'])
                                ->freeword($condition['freeword']);
                    }
                    else if(! $condition["need_no_action"]){
                        $query = Entry::status($condition['react_status'])
                                ->entryType($condition['entry_type'])
                                ->licenceType($condition['licence_type'])
                                ->freeword($condition['freeword']);
                    }   
                }
                else{
                    if($condition["react_status"] == "no_action")
                    {
                        $query = Entry::onlyTrashed()
                                ->entryType($condition['entry_type'])
                                ->licenceType($condition['licence_type']);
                    }
                    else if($condition["need_no_action"]){
                        $query = Entry::withTrashed()
                                ->status($condition['react_status'])
                                ->entryType($condition['entry_type'])
                                ->licenceType($condition['licence_type']);
                    }
                    else if(! $condition["need_no_action"]){
                        $query = Entry::status($condition['react_status'])
                                ->entryType($condition['entry_type'])
                                ->licenceType($condition['licence_type']);
                    }            
                }
                
                // Dm flag check
                $users = $query->where('dm_flag',1)->select('email')->get()->toArray();                

            }else if(isset($request->params['id'])){
                
                // 同一メールアドレスユーザーには一回しかメールは送信しないため、重複を外す
                $users = Entry::select('email')->whereIn('id',$request->params['id'])->get()->toArray();                

            }

            $users = array_flatten($users);  // 多次元から一次元へ
            $users = array_unique($users);   // 重複削除
            $users = array_values($users);   // キーが飛び飛びになっているので振り直す
                        

            foreach($users as $user)
            {
                $options = $request->params;
                $options['content'] = preg_replace('/\$氏名\$/', Entry::where('email',$user)->first()->name, $options['content']);
                Mail::to($user)->send(new SendCampaign(['title' => $options['title'], 'content' => $options['content']]));                
                // 履歴追加
                $title      = $request->params['title'];

                History::create([
                    'email'             => $user,
                    'history_date'      => Carbon::now()->toDateTimeString(),
                    'history_type'      => 'send_campaign',
                    'history_title'     => $title,
                    'history_content'   => $options['content'],
                ]);    
            }

            $id_list = Entry::whereIn('email',$users)->select('id')->get()->toArray();
            $id_list = array_flatten($id_list);

            return ['msg' => 'success', 'id' => $id_list];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }


    /**
     * Reply to Tmp Entering
     */
    public function replyToTmpEntering(Request $request)
    {             
        try{
            $id_list = $request->params['id'];

            // 単一ユーザーなら
            if(gettype($id_list) !== 'array'){
                // メール送信
                $user = Entry::where('id',$request->params['id'])->first();
                Mail::to($user)->send(new ReplyToTmpEntering($request->params));

                // ステータス更新
                $user->update([
                    'react_status' => 'reacted',
                    'react_date'   => Carbon::now()->format('Y-m-d'),
                ]);

                // 履歴追加
                $email = $user->email;

                History::create([
                    'email'             => $email,
                    'history_date'      => Carbon::now()->toDateTimeString(),
                    'history_type'      => 'reply_to_tmp_entering',
                    'history_title'     => $request->params['title'],
                    'history_content'   => $request->params['content'],
                ]);    
            }
            // 複数ユーザーなら
            else if(gettype($id_list) === 'array'){
                // メール送信
                // $users = Entry::whereIn('id',$id_list)->get();
                $users = Entry::select('email')->whereIn('id',$id_list)->get()->toArray();
                $users = array_flatten($users);  // 多次元から一次元へ
                $users = array_unique($users);   // 重複削除
                $users = array_values($users);   // キーが飛び飛びになっているので振り直す
                
                foreach($users as $user){                
                    $options = $request->params;
                    $options['content'] = preg_replace('/\$氏名\$/', Entry::where('email',$user)->first()->name, $options['content']);
                    Mail::to($user)->send(new ReplyToTmpEntering(['title' => $options['title'], 'content' => $options['content']]));

                    // 履歴追加
                    $title      = $request->params['title'];

                    History::create([
                        'email'             => $user,
                        'history_date'      => Carbon::now()->toDateTimeString(),
                        'history_type'      => 'reply_to_tmp_entering',
                        'history_title'     => $title,
                        'history_content'   => $options['content'],
                    ]);    
                    
                }


                // ステータス更新
                // foreach($users as $user)
                foreach(Entry::whereIn('email',$users)->get() as $user)
                {
                    $user->update([
                        'react_status' => 'reacted',
                        'react_date'   => Carbon::now()->format('Y-m-d'),
                    ]);    
                }                
            }            

            return ['msg' => 'success'];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }
}
