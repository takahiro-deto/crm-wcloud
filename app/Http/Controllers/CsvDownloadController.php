<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Excel;
use DB;
use Carbon\Carbon;

use App\Models\Entry;

class CsvDownloadController extends Controller
{
	private $entry_types = "";
    private $licence_types = "";


    /**
     * 教習所ごとに異なる entry_types及びlicence_typesを取得しprivateに保存
     */
    public function __construct()
    {                
        $this->entry_types   = array_divide(array_collapse(opt("db_config.entry_types_en_jp_map")))[0];
        $this->licence_types = array_divide(array_collapse(opt("db_config.licence_types_en_jp_map")))[0];
    }

    
    public function dashboardAreaData(Request $request)
    {
        /**
        * Request Params
        */        
        $dates = [
            'dt_from'  => $request->start_date,
            'dt_until' => $request->end_date
        ];

        $filetype = $request->os == 'win' ? 'xlsx' : 'csv';
        
        /*----------- pulldownの内容判別(SQL選定用) -----------*/
        $regExp = "/(";

        foreach($this->entry_types as $index => $type)
        {
            if($index !== count($this->entry_types) - 1)
            {
                $regExp .= $type . '|';    
            }else{
                $regExp .= $type;
            }            
        }

        $regExp .= ")/";

        preg_match($regExp,$request->content,$matches);

        $entry_type = "";
        if($matches){ $entry_type = $matches[1]; }
        
        $item = "";


        /*--------------$request->contentの中身によってクエリ選定----------------*/

        $data = [];


        if(preg_match('/^web_entry$/',$request->content))
        {            
            $data['col_header']  = "WEBエントリー数";
            
            $data['respective']  = DB::table('entries')
                                    ->select(DB::raw("zipcode,zipcode_level_address,COUNT(id) as sum_up"))
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->groupBy('zipcode','zipcode_level_address')
                                    ->orderBy('sum_up','DESC')
                                    ->get();
            $data['gross']       = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->count();

        }else if(preg_match('/^entering_count_of_web_entry$/',$request->content))
        {            
            $data['col_header']  = "入所数（WEBエントリー）";
            
            $data['respective']  = DB::table('entries')
                                    ->select(DB::raw("zipcode,zipcode_level_address,COUNT(id) as sum_up"))
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('react_status','entered')
                                    ->whereNotNull('entered_date')
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->groupBy('zipcode','zipcode_level_address')
                                    ->orderBy('sum_up','DESC')
                                    ->get();
            $data['gross']       = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->whereNotNull('entered_date')
                                    ->where('react_status','entered')
                                    ->count();

        }else if(preg_match('/^entering_rate_of_web_entry/',$request->content))
        {            
            $data['col_header']  = "入所率（WEBエントリー）";
            
            $entry               = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->count();

            $entered             = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->where('react_status','entered')
                                    ->whereNotNull('entered_date')
                                    ->count();

            $data['respective']  = DB::table('entries')
                                    ->select(DB::raw("zipcode,zipcode_level_address,(COUNT(id) * 100/ ${entry} * 100) as sum_up"))
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('react_status','entered')
                                    ->whereNotNull('entered_date')
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->groupBy('zipcode','zipcode_level_address')
                                    ->orderBy('sum_up','DESC')
                                    ->get();

            $data['gross']       = $entry == 0 ? 0 : ($entered / $entry) * 100;
            $data['is_rate']     = true;
            

        }else if(preg_match('/entering_count/', $request->content))
        {
            // 入所数 x エントリータイプ

            foreach(opt('db_config.entry_types_en_jp_map') as $et)
            {
                foreach($et as $key => $val)
                {
                    if(preg_match("/{$key}/", $request->content))
                    {
                        $data['col_header'] = "入所数（" . $val. "）";
                    }
                }
            }
            
            $data['respective'] = DB::table('entries')
                                    ->select(DB::raw("zipcode,zipcode_level_address,COUNT(id) as sum_up"))
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('entry_type',$entry_type)
                                    ->where('react_status','entered')
                                    ->whereNotNull('entered_date')
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->groupBy('zipcode','entry_type','zipcode_level_address')
                                    ->orderBy('sum_up','DESC')
                                    ->get();

            $data['gross']      = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->whereNotNull('entered_date')
                                    ->where('react_status','entered')
                                    ->where('entry_type',$entry_type)
                                    ->count();

        }else if(preg_match('/entering_rate/', $request->content))
        {
            // 入所率 x エントリータイプ

            foreach(opt('db_config.entry_types_en_jp_map') as $et)
            {
                foreach($et as $key => $val)
                {
                    if(preg_match("/{$key}/", $request->content))
                    {
                        $data['col_header'] = "入所率（" . $val . "）";
                    }
                }
            }           
            
            $entry               = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('entry_type',$entry_type)
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->count();

            $entered             = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('entry_type',$entry_type)
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->where('react_status','entered')
                                    ->whereNotNull('entered_date')
                                    ->count();

            $data['respective']  = DB::table('entries')
                                    ->select(DB::raw("zipcode,zipcode_level_address,(COUNT(id) * 100/ ${entry} * 100) as sum_up"))
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('entry_type',$entry_type)
                                    ->where('react_status','entered')
                                    ->whereNotNull('entered_date')
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->groupBy('zipcode','zipcode_level_address')
                                    ->orderBy('sum_up','DESC')
                                    ->get();

            $data['gross']       = $entry == 0 ? 0 : ($entered / $entry) * 100;
            $data['is_rate']     = true;

        }else
        {
            // エントリータイプ別エントリー数

            foreach(opt('db_config.entry_types_en_jp_map') as $et)
            {
                foreach($et as $key => $val)
                {
                    if(preg_match("/{$key}/", $request->content))
                    {
                        $data['col_header'] = $val;
                    }
                }
            }

            $data['respective'] = DB::table('entries')
                                    ->select(DB::raw("zipcode,zipcode_level_address,COUNT(id) AS sum_up, COUNT(id) / (SELECT COUNT(*) FROM entries WHERE entry_type = \"${entry_type}\") AS share"))
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->where('entry_type',$entry_type)
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->groupBy('zipcode','entry_type','zipcode_level_address')
                                    ->orderBy('sum_up','DESC')
                                    ->get();

            $data['gross']      = DB::table('entries')
                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                    ->whereNotNull('zipcode')
                                    ->whereNotNull('zipcode_level_address')
                                    ->where('entry_type',$entry_type)
                                    ->count();
        }
                
        /*---------------------------------------------------*/
        $header = [];
        $header[0] = [$dates['dt_from'] . ' - ' .$dates['dt_until'],'','','',''];   // 集計期間
        $header[1] = ['no','郵便番号','住所',$data['col_header'],"割合\r\n"];  // カラムのヘッダー        
        $filename = 'area_data' . Carbon::now()->format('Y-m-d');
        $filename = mb_convert_encoding($filename, 'utf8');


        return Excel::create($filename,function($excel) use ($filename,$data,$header,$filetype){

            $excel->sheet($filename,function($sheet) use ($data,$header){

                // Header出力
                $sheet->row(1,$header[0]);
                $sheet->row(2,$header[1]);
                $no = 1;

                if(isset($data['is_rate']))
                {

                    foreach($data['respective'] as $row_data)
                    {                        
                        $sheet->appendRow([
                            $no,
                            $row_data->zipcode,
                            $row_data->zipcode_level_address,
                            $row_data->sum_up,
                            ($row_data->sum_up / $data['gross'] / 100)
                        ]);

                        $no += 1;
                    }          

                }else{

                    foreach($data['respective'] as $row_data)
                    {                        
                        $sheet->appendRow([
                            $no,
                            $row_data->zipcode,
                            $row_data->zipcode_level_address,
                            $row_data->sum_up,
                            ($row_data->sum_up / $data['gross'])
                        ]);

                        $no += 1;
                    }   

                }
            });

        })->download($filetype);
    }


    public function downloadSpecifiedUsersData(Request $request)
    {
        $condition          = $request->query->all();

        // view.columns_map.csv の nameとcolumnを分割
        $csv_columns_list   = opt("view.columns_map.csv");
        $header             = [];
        $fetch_columns      = [];

        foreach($csv_columns_list as $item)
        {
            $header[]         = $item['name'];
            $fetch_columns[]  = $item['column'];
        }


        // 住所がCSVの対象であれば、pref/city/townを連結する必要がある
        $address_index = array_search('address',$fetch_columns);

        if($address_index !== false) // 単にfalseだとindexが0の場合もfalseになってしまう
        {
            $fetch_columns[$address_index] = "address_pref,address_city,address_town";
        }


        // データの取得
        $data = $this->makeQueryBuilder($request)->select(DB::raw(implode($fetch_columns,",")))->get()->toArray();        
        foreach($data as &$record)
        {
            // 日本語変換
            if(isset($record['entry_type']))
            {
                foreach(array_collapse(opt('db_config.entry_types_en_jp_map')) as $key => $val)
                {
                    if($key == $record['entry_type'])
                    {
                        $record['entry_type'] = $val;
                    }
                }
            }

            if(isset($record['licence_type']))
            {
                foreach(array_collapse(opt('db_config.licence_types_en_jp_map')) as $key => $val)
                {
                    if($key == $record['licence_type'])
                    {
                        $record['licence_type'] = $val;
                    }
                }
            }

            if(isset($record['react_status']))
            {
                foreach(array_collapse(opt('db_config.status_en_jp_map')) as $key => $val)
                {
                    if($key == $record['react_status'])
                    {
                        $record['react_status'] = $val;
                    }
                }
            }


            // 住所が対象ならば連結
            if($address_index !== false)
            {
                $address = $record['address_pref'] . $record['address_city'] . $record['address_town'];
                $this->array_insert($record,['address' => $address],$address_index);

                unset($record['address_pref'],$record['address_city'],$record['address_town']);
            }
        }


        // CSV出力
        $filetype = $request->os == 'win' ? 'xlsx' : 'csv';
        $filename = $condition['name'];

        return Excel::create($filename,function($excel) use ($header,$data,$filetype,$filename){
            $excel->sheet($filename,function($sheet) use ($header,$data){
                $sheet->row(1,$header);                

                foreach($data as $record)
                {
                    $sheet->appendRow(array_values($record));
                }
            });
        })->download($filetype);
    }


    public function downloadImportFmt(Request $request)
    {
        $file_path = storage_path() . '/app/downloads/fmt.xlsx';        

        if( file_exists($file_path) )
        {
            return \Response::download($file_path,'fmt.xlsx');
        }else{
            exit( 'Requested file does not exist on our server!' );
        }
    }


    /**
      * @return \Illuminate\Database\Eloquent\Builder
      * -- fetchUserData用 QueryBuilderファクトリ
      */
    private function makeQueryBuilder(Request $request)
    {        
        $condition = $request->query->all();        
        
        if(isset($condition['freeword']))
        {
            if($condition["react_status"] == "no_action")
            {
                $query = Entry::onlyTrashed()                        
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type'])
                        ->freeword($condition['freeword']);
            }
            else if($condition["need_no_action"] == "true"){
                $query = Entry::withTrashed()                        
                        ->status($condition['react_status'])
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type'])
                        ->freeword($condition['freeword']);
            }
            else if($condition["need_no_action"] == "false"){
                $query = Entry::status($condition['react_status'])
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type'])
                        ->freeword($condition['freeword']);
            }   
        }
        else{
            if($condition["react_status"] == "no_action")
            {
                $query = Entry::onlyTrashed()                        
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type']);                     
            }
            else if($condition["need_no_action"] == "true"){
                $query = Entry::withTrashed()                        
                        ->status($condition['react_status'])
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type']);
            }
            else if($condition["need_no_action"] == "false" || ! $condition["need_no_action"]){
                $query = Entry::status($condition['react_status'])                        
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type']);                     
            }            
        }

                
        if(isset($condition['request_from']))
        {
            switch($condition['request_from'])
            {
                case 'entering-list':
                    $query->whereNull('entered_date');
                    break;
                case 'campaign-list':
                    $query->where('dm_flag',1);
                    break;
                default:
                    break;
            }    
        }
        


        return $query;
    }


    /**
     * 配列（連想配列にも対応）の指定位置に要素（配列にも対応）を挿入して、挿入後の配列を返す
     * 
     * @param array &$base_array 挿入したい配列
     * @param mixed $insert_value 挿入する値（文字列、数値、配列のいずれか）
     * @param int $position 挿入位置（省略可能。先頭は0、省略時は配列末尾に挿入される）
     * @return boolean 挿入成功時にtrue
     *
     * ref:https://qiita.com/ka215/items/dcd2e1b0fb8c626c9e44
     **/
    private function array_insert(&$base_array, $insert_value, $position=null)
    {
        if (!is_array($base_array))
        {
            return false;
        }

        $position       = is_null($position) ? count($base_array) : intval($position);
        $base_keys      = array_keys($base_array);
        $base_values    = array_values($base_array);

        if (is_array($insert_value))
        {
            $insert_keys    = array_keys($insert_value);
            $insert_values  = array_values($insert_value);
        } else {
            $insert_keys   = array(0);
            $insert_values = array($insert_value);
        }

        $insert_keys_after      = array_splice($base_keys, $position);
        $insert_values_after    = array_splice($base_values, $position);

        foreach ($insert_keys as $insert_keys_value)
        {
            array_push($base_keys, $insert_keys_value);
        }
        foreach ($insert_values as $insert_values_value)
        {
            array_push($base_values, $insert_values_value);
        }

        $base_keys = array_merge($base_keys, $insert_keys_after);
        $is_key_numric = true;

        foreach ($base_keys as $key_value)
        {
            if (!is_integer($key_value))
            {
                $is_key_numric = false;
                break;
            }
        }

        $base_values = array_merge($base_values, $insert_values_after);

        if ($is_key_numric)
        {
            $base_array = $base_values;
        } else {
            $base_array = array_combine($base_keys, $base_values);
        }

        return true;
    }
}
