<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\Template;
use App\Models\Customer;
use App\Models\History;
use App\Models\Zip;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;
use Excel;

use DB;

class CommonApiController extends Controller
{
    private $common_update_list = [];


    public function __construct(Request $request)
    {
        $this->common_update_list = opt('db_config.customer_common_data_list');
    }


	/**
	 * sidebarの未読ツールチップ件数取得用
	 */
    public function fetchTooltipCounter()
    {
    	$result = [];

    	$counters = DB::table('entries')
    				->select(DB::raw('entry_type,count(*) as count'))
    				->where('react_status','ongoing')
    				->groupBy('entry_type')
    				->get();

    	foreach($counters as $counter)
    	{
    		if($counter->entry_type !== 'other')
    		{
    			$result["ongoing_{$counter->entry_type}"] = $counter->count;
    		}
    	}

    	return $result;
    }


    public function fetchConfig()
    {
        $entry_types_map            = array_collapse(opt("db_config.entry_types_en_jp_map"));
        $licence_types_map          = array_collapse(opt("db_config.licence_types_en_jp_map"));        
        $status_types_map           = array_collapse(opt("db_config.status_en_jp_map"));
        $history_types_map          = array_collapse(opt("db_config.history_types_en_jp_map"));
        $via_routes_map             = array_collapse(opt("db_config.via_en_jp_map"));
        $entering_type              = opt('base.school_entering_type');
        $dashboard_colors           = array_collapse(opt('view.master.db_licence_type_color'));
        $dashboard_colors_map       = opt('view.master.db_licence_type_color');
        $items                      = opt('db_config.crm_using_item');
        $general_maps               = opt('common.general');        

        return ['entry_types_map'           => $entry_types_map,
                'licence_types_map'         => $licence_types_map,                
                'status_types_map'          => $status_types_map,
                'history_types_map'         => $history_types_map,
                'via_routes_map'            => $via_routes_map,
                'general_maps'              => $general_maps,

                'dashboard_colors'          => $dashboard_colors,
                'dashboard_colors_map'      => $dashboard_colors_map,
                'items'                     => $items,

                'school_entering_type'      => $entering_type,
                'school_key'                => opt('base.school_key'),
                'school_name'               => opt('base.school_name')];
    }


    public function fetchShowableColumns(Request $request)
    {
        $page = $request->page;
        
        $data = [];

        foreach(opt("view.columns_map.${page}_list") as $elem)
        {
            $data[] = $elem['column'];
        }
        
        return $data;
    }


    /**
     * listページにてuser_recordsを取得するAPI
     */
    public function fetchUsersList(Request $request)
    {        
        $entry_types_map    = array_collapse(opt("db_config.entry_types_en_jp_map"));
        $licence_types_map  = array_collapse(opt("db_config.licence_types_en_jp_map"));
        $status_types_map   = array_collapse(opt("db_config.status_en_jp_map"));        


        $baseQuery = $this->makeQueryBuilder($request);
        $condition = $request->query->all();

        if(!isset($condition["offset"]) && !isset($condition["length"])){
            $result = [                
                'user_records'              => $baseQuery
                                              ->orderBy("entry_date","DESC")
                                              ->orderBy("id","DESC")
                                              ->get(),
                'max_count'                 => $baseQuery->count()
            ];
        }else{            
            $result = [                
                'user_records'              => $baseQuery
                                              ->offset($condition["offset"])
                                              ->limit($condition["length"])
                                              ->orderBy("entry_date","DESC")
                                              ->orderBy("id","DESC")
                                              ->get(),
                'max_count'                 => $baseQuery->count()
            ];
        }

        return $result;
    }


    /**
     * listページにて特定ユーザーを取得するAPI
     */
    public function fetchSpecifiedUserData(Request $request)
    {
        $id = $request->query()['id'];

        if(gettype($id) !== 'array'){
            $result = Entry::withTrashed()->where('id',$id)->first();    
        }else{
            $result = Entry::withTrashed()->whereIn('id',$id)->get();
        }
        

        return $result;
    }


    /**
     * listページにて特定の複数ユーザーを取得するAPI
     */
    public function fetchSpecifiedMultiUsersData(Request $request)
    {
        $id = $request->query()['id'];

        if(gettype($id) !== 'array'){
            $result = Entry::withTrashed()->where('id',$id)->first();
        }else{
            $result = Entry::withTrashed()->whereIn('id',$id)->get();
        }
        
        
        return $result;
    }


    /**
     * Userの更新用メソッド
     */
    public function updateUser(Request $request)
    {
        $as_is_user = Entry::withTrashed()->where('id',$request->params['id'])->first()->toArray();
        $to_be_user = $request['params'];
        $do_soft_delete = false;
        
        $diff = [];

        foreach($to_be_user as $key => $value)
        {
            // 差分を抽出
            if($as_is_user[$key] !== $value)
            {
                $diff[$key] = $value;
            }
        }
        // 対応不要の場合
        if(isset($diff['react_status']) && $diff['react_status'] == 'no_action')
        {
            $do_soft_delete = true;
        }
                

        try{
            if($do_soft_delete){
                // 差分更新
                Entry::where('id',$request->params['id'])->update($diff);

                // 共通データ同時更新処理
                $email = $this->updateCommonDataSameTime($diff,$as_is_user,$to_be_user);

                // Soft Delete
                Entry::destroy($request->params['id']);
                
            }else{
                // 差分更新
                Entry::withTrashed()->where('id',$request->params['id'])->update($diff);

                // 共通データ同時更新処理
                $email = $this->updateCommonDataSameTime($diff,$as_is_user,$to_be_user);
            }

            // 資料送付完了の際の対応履歴更新処理
            if(isset($to_be_user['entry_type']) && isset($to_be_user['react_status']))
            {
                if($to_be_user['entry_type'] == 'brochure' &&
                    $as_is_user['react_status'] == 'ongoing' &&
                    $to_be_user['react_status'] == 'reacted')
                {
                    $email = isset($to_be_user['email']) ? $to_be_user['email'] : $as_is_user['email'];

                    // 自動返信テンプレートがない場合、自動返信する設定は無効とする
                    if(! Template::where('purpose',0)->where('auto_sending_flag',1)->first())
                    {
                        Entry::withTrashed()->where('id',$request->params['id'])->update(['disable_auto_send' => 1]);
                    }

                    History::create([
                        'email'             => $email,
                        'history_date'      => Carbon::now()->toDateTimeString(),
                        'history_type'      => 'send_brochure',
                    ]);
                }
            }

            // 変更内容に免許種別が含まれる場合の同一ユーザーの免許種別項目 Update処理
            if(isset($diff['licence_type']))
            {
                Entry::withTrashed()->where('email',$email)
                                    ->where('licence_type',$as_is_user['licence_type'])
                                    ->update(['licence_type' => $diff['licence_type']]);
            }

            // Refresh用 Entryデータ返却
            $target_user = array_flatten(Entry::withTrashed()->select('id')->where('email',$email)->get()->toArray());
            
            return ['msg' => 'updated','target_user' => $target_user];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }


    /**
     * Userの更新用メソッド
     */
    public function updateDeletedUser(Request $request)
    {        
        $as_is_user = Entry::withTrashed()->where('id',$request->params['id'])->first()->toArray();
        $to_be_user = $request['params'];
        $restore = false;

        $diff = [];        
        foreach($to_be_user as $key => $value)
        {
            // 差分を抽出
            if($as_is_user[$key] !== $value)
            {
                $diff[$key] = $value;
            }            
        }

        // 対応不要 -> 元に戻す場合
        if(isset($diff['react_status']) && $diff['react_status'] != 'no_action')
        {
            $restore = true;
        }


        try{
            if($restore){
                // リストア
                Entry::withTrashed()->where('id',$request->params['id'])->restore();

                // 差分更新
                Entry::where('id',$request->params['id'])->update($diff);

                // 共通データ同時更新処理
                $email = $this->updateCommonDataSameTime($diff,$as_is_user,$to_be_user);
            }else{
                // 差分更新
                Entry::withTrashed()->where('id',$request->params['id'])->update($diff);

                // 共通データ同時更新処理
                $email = $this->updateCommonDataSameTime($diff,$as_is_user,$to_be_user);                
            }

            // 変更内容に免許種別が含まれる場合の同一ユーザーの免許種別項目 Update処理
            if(isset($diff['licence_type']))
            {
                Entry::withTrashed()->where('email',$email)
                                    ->where('licence_type',$as_is_user['licence_type'])
                                    ->update(['licence_type' => $diff['licence_type']]);
            }

            // Refresh用 Entryデータ返却
            $target_user = array_flatten(Entry::withTrashed()->select('id')->where('email',$email)->get()->toArray());
            
            return ['msg' => 'updated','target_user' => $target_user];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }


    /**
     * Userの一括更新用メソッド
     */
    public function bulkUpdateUser(Request $request)
    {
        $target_users = $request['params']['ids'];
        $date         = $request['params']['date'];
        $status       = $request['params']['react_status'];
        $brochure     = "";

        // brochureのbulkUpdateの時しかentry_typeは入ってこない
        if(isset($request['params']['entry_type']))
        {
            $brochure = $request['params']['entry_type'];
        }
        

        foreach($target_users as $id)
        {
            try{
                if($brochure)
                {
                    $email = Entry::withTrashed()->where('id',$id)->first()->email;
                    $disable_auto_send = $request['params']['disable_auto_send'] ? 1 : 0;
                    
                    History::create([
                        'email'             => $email,                        
                        'history_date'      => Carbon::now()->toDateTimeString(),
                        'history_type'      => 'send_brochure',
                    ]);

                    switch($status){
                        case 'reacted':
                            Entry::where('id',$id)->update(['react_date' => $date,'react_status' => 'reacted','disable_auto_send' => $disable_auto_send]);
                            break;
                        case 'entered':
                            Entry::where('id',$id)->update(['entered_date' => $date,'react_status' => 'entered','disable_auto_send' => $disable_auto_send]);
                            break;                    
                        default:
                            break;
                    }

                }else{

                    switch($status){
                        case 'reacted':
                            Entry::where('id',$id)->update(['react_date' => $date,'react_status' => 'reacted']);
                            break;
                        case 'entered':
                            Entry::where('id',$id)->update(['entered_date' => $date,'react_status' => 'entered']);
                            break;                    
                        default:
                            break;
                    }

                }                
                
            }catch(Exception $e){
                return ['msg' => 'failed'];
            }
            
        }
        return ['msg' => 'updated'];
    }    


    /**
     * User新規作成用メソッド
     */
    public function createUser(Request $request)
    {
        $user  = $request->params;
        
        try{            
            $id = Entry::insertGetId($user);

            $same_user = Entry::withTrashed()->where('email',$user['email'])->where('id','<>',$id)->orderBy('id','DESC')->first();

            if($same_user)
            {
                $same_user = $same_user->toArray();

                $additional_update_items = [];
                $overwrite_update_items = [];

                foreach($this->common_update_list as $item)
                {
                    if(empty($user[$item]))
                    {
                        $additional_update_items[$item] = $same_user[$item];
                    }

                    if(isset($user[$item]) && $user[$item] !== $same_user[$item])
                    {
                        $overwrite_update_items[$item] = $user[$item];
                    }
                }
                
                Entry::where('id',$id)->update($additional_update_items);
                Entry::where('email',$user['email'])->update($overwrite_update_items);
            }
            


            if($user['react_status'] == 'no_action')
            {
                Entry::destroy($id);
            }

            return ["msg" => "created"];
        }catch(Exception $e){
            return ["msg" => "failed"];
        }
    }


    /**
     * エントリーデータCSV一括アップロード用
     */
    public function uploadFile(Request $request)
    {        
        $file = $request->file('file');
        $hasError = false;

        $pref_list = DB::table('coredb.zips')->select(DB::raw("DISTINCT pref"))->pluck('pref')->toArray();
        
        try{
            Excel::load($file->getRealPath(),function($reader) use ($pref_list){
                $rows = $reader->toArray();
                $len  = count($rows);
                
                /**
                 * 登録前バリデーション
                 *
                 * <Presence>
                 *  - email
                 *  - if city, pref exists and in_array(pref, pref_list)
                 *  - if town, pref and city exists
                 *  - entry_date
                 *
                 * <Format>
                 *  - age          ... 16 <= age <= 98
                 *  - tel,tel2     ... preg_match(/[0-9]{10,11}/,$val)
                 *  - pref         ... in_array(SELECT DISTINCT pref FROM coredb.zipcodes)
                 *  - city         ... in_array(SELECT DISTINCT city FROM coredb.zipcodes WHERE pref = 'XXX')
                 *  - sex          ... preg_match('/(男|女)/',$sex)
                 *  - entry_type   ... array_values of opt('db_config.entry_types_en_jp_map') includes it ?
                 *  - licence_type ... array_values of opt('db_config.licence_types_en_jp_map') includes it ?
                 *  - react_status ... array_values of opt('mail.parser_status_types_object') includes it ?
                 *
                 * <Already Exists>
                 *                 ... if data already exists, don't overwrite them and return alert
                 */
                for($i = 0; $i < $len; $i++)
                {
                    $index = $i + 1;

                    /******************************************************
                     * CAST
                     ******************************************************/
                    if(isset($rows[$i]['zipcode']))
                    {
                        $rows[$i]['zipcode'] = strval($rows[$i]['zipcode']);
                    }
                    if(isset($rows[$i]['tel']))
                    {
                        $rows[$i]['tel'] = strval($rows[$i]['tel']);
                    }
                    if(isset($rows[$i]['tel2']))
                    {
                        $rows[$i]['tel2'] = strval($rows[$i]['tel2']);
                    }
                    if(isset($rows[$i]['birthday']))
                    {
                        $rows[$i]['birthday'] = strval($rows[$i]['birthday']);
                    }


                    /******************************************************
                     * validation by presence
                     ******************************************************/
                    // 都道府県名が存在するか

                    if(isset($rows[$i]['address_pref']))
                    { 
                        if(! in_array($rows[$i]['address_pref'],$pref_list))
                        {
                            throw new \Exception("${index}行目の都道府県名は存在しません。");
                        }
                        if(empty($rows[$i]['address_city']))
                        {
                            throw new \Exception("${index}行目の市区町村名が空になっています。");
                        }
                    }

                    // 市区町村名バリデーション

                    if(isset($rows[$i]['address_city']))
                    {
                        if(empty($rows[$i]['address_pref']))
                        {
                            throw new \Exception("${index}行目の都道府県名が未記入です。\r\n市区町村を設定する場合は都道府県名も設定してください。"); 
                        }
                        
                        $city = Zip::where('pref',$rows[$i]['address_pref'])
                                ->where('city',$rows[$i]['address_city'])
                                ->value('city');

                        if(! $city)
                        {
                            throw new \Exception("${index}行目の市区町村名は存在しません。"); 
                        }

                    }

                    if(isset($rows[$i]['address_town']))
                    {
                        if(empty($rows[$i]['address_pref']))
                        {
                            throw new \Exception("${index}行目の都道府県名が空になっています。\r\n住所を設定する場合は、都道府県名・市区町村名も設定してください。"); 
                        }
                        if(empty($rows[$i]['address_city']))
                        {
                            throw new \Exception("${index}行目の市区町村名が空になっています。\r\n住所を設定する場合は、都道府県名・市区町村名も設定してください。"); 
                        }
                    }

                    
                    // Emailが空なら例外をスロー
                
                    if(empty($rows[$i]['email']))
                    {
                        throw new \Exception("${index}行目のメールアドレスが空になっています。\r\nメールアドレスは必須項目です。"); 
                    }

                    // Emailアドレスが形式外なら例外をスロー
                    if(! preg_match('/^[a-zA-Z0-9_.+-]+[@][a-zA-Z0-9.-]+$/',$rows[$i]['email']))
                    {
                        throw new \Exception("${index}行目のメールアドレスの形式が正しくありません。"); 
                    }

                    // エントリー日時が空なら例外をスロー
                    if(empty($rows[$i]['entry_date']))
                    {
                        throw new \Exception("${index}行目のエントリー日時が空になっています。\r\nエントリー日時は必須項目です。");
                    }
                    

                    /******************************************************
                     * validation by format
                     ******************************************************/

                    // エントリー日時が未来の日付でないか
                    $entry_date = Carbon::parse($rows[$i]['entry_date'])->toDateTimeString();
                    $today      = Carbon::today()->toDateTimeString();
                    
                    if($entry_date > $today)
                    {
                        throw new \Exception("${index}行目のエントリー日時が正しくありません。\r\n今日より先の日付は設定できません。");
                    }

                    // 年齢のバリデーション
                    if(isset($rows[$i]['age']))
                    {
                        if($rows[$i]['age'] < 16 || 98 < $rows[$i]['age'])
                        {
                            throw new \Exception("${index}行目の年齢が正しくありません。\r\n16歳以上98歳以下の対象者のみしか登録はできません。");
                        }
                    }                    
                    // 電話番号形式のバリデーション
                    if(isset($rows[$i]['tel']) && ! preg_match('/^[0-9]{10,11}$/',$rows[$i]['tel']))
                    {
                        throw new \Exception("${index}行目の電話番号の表記が正しくありません。");
                    }
                    if(isset($rows[$i]['tel2']) && ! preg_match('/^[0-9]{10,11}$/',$rows[$i]['tel2']))
                    {
                        throw new \Exception("${index}行目の電話番号２の表記が正しくありません。");
                    }

                    // Entry typeの形式のバリデーション
                    $entry_type = "";
                    $entry_types_map = array_collapse(opt('db_config.entry_types_en_jp_map'));
                    $converted = false;

                    foreach($entry_types_map as $key => $val)
                    {
                        if($rows[$i]['entry_type'] == $val)
                        {
                            $entry_type = $key;
                            $converted = true;
                            break;
                        }
                    }
                    
                    if(! $converted){ throw new \Exception("${index}行目のデータのエントリータイプが正しくありません。"); }


                    // Licence typeの形式のバリデーション
                    $licence_type = "";
                    $licence_types_map = array_collapse(opt('db_config.licence_types_en_jp_map'));
                    $converted = false;

                    foreach($licence_types_map as $key => $val)
                    {
                        if($rows[$i]['licence_type'] == $val)
                        {
                            $licence_type = $key;
                            $converted = true;
                            break;
                        }
                    }
                    if(! $converted){ throw new \Exception("${index}行目のデータの免許種別が正しくありません。"); }


                    // React Statusの形式のバリデーション
                    $react_status = "";
                    $status_types_map = opt('mail.parser_status_types_object');
                    $converted = false;
                    
                    foreach($status_types_map as $key => $val)
                    {
                        if($rows[$i]['react_status'] == $val)
                        {
                            $react_status = $key;
                            $converted = true;
                            break;
                        }
                    }
                    if(! $converted){ throw new \Exception("${index}行目のデータの対応状況が正しくありません。"); }


                    // 性別の表記
                    if(isset($rows[$i]['sex']))
                    {
                        $bool = preg_match('/(男|女)/', $rows[$i]['sex']);

                        if(! $bool){
                            throw new \Exception("${index}行目のデータの性別が正しくありません。");
                        }
                    }



                    // 既にあるEntryなら例外をスロー
                    if( Entry::where('email',$rows[$i]['email'])
                        ->where('licence_type',$licence_type)
                        ->where('entry_type',$entry_type)->first() )
                    {
                        throw new \Exception("${index}行目のデータはすでに登録されています。");
                    }


                    

                    $entry = $rows[$i];
                    $entry['entry_type']    = $entry_type;
                    $entry['licence_type']  = $licence_type;
                    $entry['react_status']  = $react_status;

                    $entry['entry_date']    = Carbon::now()->toDateTimeString();
                    $entry['imported_at']   = Carbon::now()->toDateTimeString();


                    if(isset($entry['birthday']))
                    {
                        $entry['birthday']  = Carbon::parse($entry['birthday'])->format('Y-m-d');
                    }
                    if(isset($entry['react_date']))
                    {
                        $entry['react_date']  = Carbon::parse($entry['react_date'])->format('Y-m-d');
                    }
                    if(isset($entry['entered_date']))
                    {
                        $entry['entered_date']  = Carbon::parse($entry['entered_date'])->format('Y-m-d');
                    }
                    

                    Entry::create($entry);

                    // Customerに投入
                    $customer = [
                        'email'        => $rows[$i]['email'],
                        'address_pref' => $rows[$i]['address_pref'],
                        'address_city' => $rows[$i]['address_city'],
                        'address_town' => $rows[$i]['address_town'],
                    ];

                    Customer::create($customer);


                    if($entry['entry_type'] !== 'other')
                    {
                        History::create([
                            'email'             => $entry['email'],
                            'history_type'      => $entry['entry_type'],
                            'history_date'      => $entry['entry_date']
                        ]);            
                    }                    
                }                
            });
        }catch(\Exception $e){

            return ['msg' => $e->getMessage()];
        }
        
        return ['msg' => 'CSVインポートは正常に終了しました。'];
    }


    /**
     * 対応メモ用メソッド
     */
    public function writeNotes(Request $request)
    {
        try{
            $email = Entry::withTrashed()->where('id',$request->params['id'])->first()->email;

            History::create([
                'email'             => $email,
                'history_date'      => $request->params['history_date'],
                'history_type'      => 'memo',
                'history_title'     => $request->params['title'],
                'history_content'   => $request->params['content'],
            ]);
            
            return ['msg' => 'success'];    
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }

    public function updateNotes(Request $request)
    {
        try{            
            History::where('id',$request->params['id'])
                    ->where('history_type','memo')
                    ->update([
                        'history_title'     => $request->params['title'],
                        'history_content'   => $request->params['content'],
                    ]);
            
            return ['msg' => 'success'];    
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
    }
    

    public function fetchRespectiveHistory(Request $request)
    {
        $email = Entry::withTrashed()->where('id',$request->query()['id'])->first()->email;

        $histories = History::where('email',$email)->orderBy('history_date')->orderBy('id')->get();
        $results = [];

        foreach($histories as $history)
        {
            array_push($results,
                ['id'            => $history->id,
                 'date'          => $history->history_date,
                 'history_type'  => $history->history_type,
                 'title'         => $history->history_title,
                 'content'       => $history->history_content,
                ]);
        }        
        return ['histories' => $results];
    }


    /**
      * @return \Illuminate\Database\Eloquent\Builder
      * -- fetchUserData用 QueryBuilderファクトリ
      */
    private function makeQueryBuilder(Request $request)
    {        
        $condition = $request->query->all();        
        
        if(isset($condition['freeword']))
        {
            if($condition["react_status"] == "no_action")
            {
                $query = Entry::onlyTrashed()                        
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type'])
                        ->freeword($condition['freeword']);
            }
            else if($condition["need_no_action"] == "true"){
                $query = Entry::withTrashed()                        
                        ->status($condition['react_status'])
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type'])
                        ->freeword($condition['freeword']);
            }
            else if($condition["need_no_action"] == "false"){
                $query = Entry::status($condition['react_status'])
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type'])
                        ->freeword($condition['freeword']);
            }   
        }
        else{
            if($condition["react_status"] == "no_action")
            {
                $query = Entry::onlyTrashed()                        
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type']);                     
            }
            else if($condition["need_no_action"] == "true"){
                $query = Entry::withTrashed()                        
                        ->status($condition['react_status'])
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type']);
            }
            else if($condition["need_no_action"] == "false" || ! $condition["need_no_action"]){
                $query = Entry::status($condition['react_status'])                        
                        ->entryType($condition['entry_type'])
                        ->licenceType($condition['licence_type']);                     
            }            
        }

                
        if(isset($condition['request_from']))
        {
            switch($condition['request_from'])
            {
                case 'entering-list':
                    $query->whereNull('entered_date');
                    break;
                case 'campaign-list':
                    $query->where('dm_flag',1);
                    break;
                default:
                    break;
            }    
        }
        


        return $query;
    }


    /**
      * @return void
      * -- Update User時に、Customer共通項目があればエントリー横断でUpdateする処理
      */
    private function updateCommonDataSameTime($diff,$as_is_user,$to_be_user)
    {
        $email = "";

        // Eメールアドレスそのものが変更される場合は$to_be_userを利用しなければならない

        if(isset($to_be_user['email']))
        {
            $email = $to_be_user['email'];
            $ex_email = $as_is_user['email'];
        }else{
            $email = $as_is_user['email'];
        }
        
        // 共通データ同時更新処理
        $common_update_item = [];

        foreach($diff as $each_diff_key => $each_diff_val)
        {
            if(in_array($each_diff_key,$this->common_update_list))
            {
                $common_update_item[$each_diff_key] = $each_diff_val;
            }
        }

        Entry::withTrashed()->where('email',$email)->update($common_update_item);
                
        // 暫定処理。最終的にはエントリーをrelationで書き直す
        $customer_common_columns = ['address_pref','address_city','address_town'];

        foreach($customer_common_columns as $column)
        {
            if(isset($common_update_item[$column]))
            {
                Customer::where('email',$email)->update([$column => $common_update_item[$column]]);
            }
        }


        if(isset($ex_email))
        {
            History::where('email',$ex_email)->update(['email' => $email]);
            Customer::where('email',$ex_email)->update(['email' => $email]);
        }

        return $email;
    }
}