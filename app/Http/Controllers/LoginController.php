<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {    
    	$credential = $request->params;

    	$user = User::where('login_id',$credential['id'])->first();
    	
    	if(is_null($user))
    	{
    		return response(['msg' => 'failed']);
    	}


    	if(Hash::check($credential['password'], $user->password))
    	{
    		$roles = [
                'role_admin'            => $user->role_admin ? 1 : 0,
                'role_campaign'         => $user->role_campaign,
                'role_csv'              => $user->role_csv,
                'role_dashboard'        => $user->role_dashboard,
                'role_entries'          => $user->role_entries,
                'role_general_user'     => $user->role_general_user,
                'role_templates'        => $user->role_templates,
    		];    		

    		return response()->json(['msg' => 'success', 'roles' => $roles]);
    	}else{
    		return response(['msg' => 'failed']);
    	}
    }
}
