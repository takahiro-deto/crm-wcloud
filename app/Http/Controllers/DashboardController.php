<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use PDO;
use Excel;

use App\Models\Entry;
use App\Models\Option;
use App\Models\Customer;

class DashboardController extends Controller
{
    private $entry_types = [];
    private $licence_types = [];


    /**
     * 教習所ごとに異なる entry_types及びlicence_typesを取得しprivateに保存
     */
    public function __construct()
    {        
        $this->entry_types   = array_divide(array_collapse(opt("db_config.entry_types_en_jp_map")))[0];
        $this->licence_types = array_divide(array_collapse(opt("db_config.licence_types_en_jp_map")))[0];
    }


    /**
    * /api/fetch-dashboard-dataのhandle先
    * ここから各private methodにデータの取得を依頼する
    */
    public function index(Request $request)
    {     
        /**
        * Request Params
        */        
        $dates = [
            'dt_from'  => $request->start_date . ' 00:00:00',
            'dt_until' => $request->end_date . ' 23:59:59'
        ];

        $key = $request->key;
        
        $result = [
            'key' => $key,
            'content' => $this->dataHandler($key,$dates)
        ];

        return $result;
    }


    /**
    * 単純なOptデータの返却はoptユーティリティから直接返却する    
    */
    public function fetchConfiguration(Request $request)
    {
        $key = $request->key;

        return opt($key);
    }
    
    

    private function dataHandler($key,$dates)
    {
        switch($key){
            case 'entry_types':
                return opt('db_config.entry_types_en_jp_map');
                break;            

            case 'counts':
                return $this->getCounts($dates);
                break;

            case 'stuck_bar_chart':
                return $this->getStuckBarChart($dates);
                break;

            case 'school':                
                return $this->getBarchartData($dates,'school');
                break;

            case 'channel':
                return $this->getBarchartData($dates,'via_routes');
                break;

            case 'area':
                return $this->getBarchartData($dates,'zipcode_level_address');
                break;

            case 'school_geo':
                $school_geo = [];       
                $school_geo['lat'] = opt('base.school_address')['lat'];
                $school_geo['lng'] = opt('base.school_address')['lng'];

                return $school_geo;
                break;

            case 'user_geodatas':
                return $this->getUserGeoDatas($dates);
                break;
        }
    }

    /**
    * countsオブジェクトを返す。
    * 総エントリー数、資料請求数、仮申し込み数、問い合わせ数、入所数の配列
    */
    private function getCounts($dates)
    {
        /**
         * Initialize & get `Counts`
         *
         * Entry typeの種類の数分  $entry_typename => 0を作る
         *
         * i.e.
         *    [total => 0,inquiry => 0,brochure => 0,tmp_entering => 0...]
         */        
        $entry_types_count_map = ['total' => DB::table('entries')->select(DB::raw("COUNT(DISTINCT email) as total"))->first()->total];

        foreach($this->entry_types as $type)
        {
            $entry_types_count_map[$type] = 0;
        }

        $counts = [
            'entry' => $entry_types_count_map,
            'entering' => DB::table('entries')
                        ->select(DB::raw("COUNT(DISTINCT email) as count"))
                        ->whereNotNull('entered_date')
                        ->whereBetween('entered_date',[$dates['dt_from'],$dates['dt_until']])
                        ->first()
                        ->count
        ];

        /**
         * リクエスト期間内のものをentry_typeでGroupBy
         */        
        $segments = DB::table('entries')
                    ->select(DB::raw('entry_type,COUNT(email) as count'))
                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                    ->groupBy('entry_type')
                    ->get();

        foreach($segments as $segment)
        {
            $counts['entry'][$segment->entry_type] += intval($segment->count);            
        }

        return $counts;
    }


    /**
    * canvas描画用のデータを返却
    * (エントリータイプ別総数|エントリータイプ別入所数|エントリータイプ別入所率) x 免許種別 x 期間
    */
    private function getStuckBarChart($dates)
    {
        /**
         * licence_typesの数だけ、 licence_typename => []を作る
         *
         * i.e.
         *   ['regular_at' => [],
         *  'regular_mt' => [],
         *  'regular_tba' => [],
         *  'regular_bike_at' => [],
         *  'regular_bike_mt' => [],
         *  'regular_bike_tba' => []]
         */
        $respective_lc_type_counts_map = [];
        foreach($this->licence_types as $name)
        {
            $respective_lc_type_counts_map[$name] = [];
        }        


        /**
        * Initialize `StuckBarChart`
        */
        $stuckBarChart = [
            'web_entry'                       => $respective_lc_type_counts_map,            
            'entering_count_of_web_entry'     => $respective_lc_type_counts_map,            
            'entering_rate_of_web_entry'      => $respective_lc_type_counts_map,            
        ];

        /**
         * Entry_typeの種別分だけプロパティ追加
         */
        foreach($this->entry_types as $entry_type)
        {
            $stuckBarChart[$entry_type] = $respective_lc_type_counts_map;
            $stuckBarChart["entering_count_of_" . $entry_type] = $respective_lc_type_counts_map;
            $stuckBarChart["entering_rate_of_" . $entry_type] = $respective_lc_type_counts_map;
        }


        $aggregation_period = [];
        $interval = Carbon::parse($dates['dt_until'])->diffInMonths(Carbon::parse($dates['dt_from']));

        for($i = 0; $i <= $interval; $i++)
        {
            $aggregation_period[] = Carbon::parse($dates['dt_from'])->addMonths($i)->format('Y-m');
        }

        $subQuery = DB::table('entries')
                        ->select(DB::raw(
                            "licence_type,
                            SUBSTRING(entry_date,1,7) AS month,
                            entry_type,
                            IF(entered_date,1,0) AS entering_f,
                            CONCAT(email,'_',licence_type) AS email,
                            entry_date,
                            entered_date"))
                        ->groupBy('licence_type','month','entry_type','entering_f','email','entry_date','entered_date')
                        ->orderBy('entry_date');
        
        $query_1 = DB::table(DB::raw("({$subQuery->toSql()}) as U"))
                                ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                ->select(DB::raw("U.licence_type,COUNT(DISTINCT email) as count,month"))
                                ->groupBy('licence_type','month')
                                ->get();

        $query_2 = DB::table(DB::raw("({$subQuery->toSql()}) as U"))
                                ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                ->select(DB::raw("U.licence_type,COUNT(DISTINCT email) as count,month,entry_type"))
                                ->groupBy('licence_type','month','entry_type')
                                ->get();

        $query_3 = DB::table(DB::raw("({$subQuery->toSql()}) as U"))
                                ->whereBetween('entered_date',[$dates['dt_from'],$dates['dt_until']])
                                ->where('entering_f',1)
                                ->select(DB::raw("U.licence_type,COUNT(DISTINCT email) as count,month"))
                                ->groupBy('licence_type','month')
                                ->get();

        $query_4 = DB::table(DB::raw("({$subQuery->toSql()}) as U"))
                                ->whereBetween('entered_date',[$dates['dt_from'],$dates['dt_until']])
                                ->where('entering_f',1)
                                ->select(DB::raw("U.licence_type,COUNT(DISTINCT email) as count,month,entry_type"))
                                ->groupBy('licence_type','month','entry_type')
                                ->get();

        foreach($query_1 as $elem)        
        {
            $licence_type   = $elem->licence_type;
            $month          = $elem->month;
            $count          = $elem->count;                        

            // web_entry
            // arrayにkeyがなければ新規追加、あれば足し上げ
            if(isset($stuckBarChart['web_entry'][$licence_type][$month]))
            {
                $stuckBarChart['web_entry'][$licence_type][$month] += $count;
            }else{
                $stuckBarChart['web_entry'][$licence_type][$month] = $count;
            }            
        }

        foreach($query_2 as $elem)
        {
            $entry_type     = $elem->entry_type;
            $licence_type   = $elem->licence_type;
            $month          = $elem->month;
            $count          = $elem->count;

            if(in_array($entry_type,$this->entry_types))
            {
                // arrayにkeyがなければ新規追加、あれば足し上げ
                if(isset($stuckBarChart[$entry_type][$licence_type][$month]))
                {
                    $stuckBarChart[$entry_type][$licence_type][$month] += $count; 
                }else{
                    $stuckBarChart[$entry_type][$licence_type][$month] = $count;  
                }          
            } 
        }

        foreach($query_3 as $elem)
        {
            $licence_type   = $elem->licence_type;
            $month          = $elem->month;
            $count          = $elem->count;            

            // entering_count_of_web_entry            
            // arrayにkeyがなければ新規追加、あれば足し上げ
            if(isset($stuckBarChart['entering_count_of_web_entry'][$licence_type][$month])){
                $stuckBarChart['entering_count_of_web_entry'][$licence_type][$month] += $count;
            }else{
                $stuckBarChart['entering_count_of_web_entry'][$licence_type][$month] = $count;
            }            
        }

        foreach($query_4 as $elem)
        {
            $entry_type     = $elem->entry_type;
            $licence_type   = $elem->licence_type;
            $month          = $elem->month;
            $count          = $elem->count;            

            if(in_array($entry_type,$this->entry_types))
            {
                // arrayにkeyがなければ新規追加、あれば足し上げ
                if(isset($stuckBarChart["entering_count_of_" . $entry_type][$licence_type][$month]))
                {
                    $stuckBarChart["entering_count_of_" . $entry_type][$licence_type][$month] += $count;   
                }else{
                    $stuckBarChart["entering_count_of_" . $entry_type][$licence_type][$month] = $count;
                }
            } 
        }        

        /**
        * month => countsの形式だが
        * あるmonthの件数が0の場合、DBファサードは month => 0とは返さず、その月が中抜けしてしまうので
        * 手動で0で埋める
        */
        foreach($stuckBarChart as $key => &$val_array)
        {
            foreach($val_array as $key => &$child_val_array)
            {
                // e.g.
                // regular_mt => []　のように,val_arrayそのものが空の時
                if(empty($child_val_array))
                {
                    for($i = 0; $i < count($aggregation_period); $i++)
                    {
                        $child_val_array[$aggregation_period[$i]] = 0;
                    }

                // e.g.
                // regular_mt => ['2017-06' => [...],'2017-08' => [...]]　のように,
                // ある月が飛びで入っている時。
                }else{
                    for($i = 0; $i < count($aggregation_period); $i++)
                    {
                        if(!isset($child_val_array[$aggregation_period[$i]]))
                        {
                            $child_val_array[$aggregation_period[$i]] = 0;
                        }
                    }
                }
                unset($child_val_array);
            }
            unset($val_array);
        }        


        return $stuckBarChart;
    }
 


    /**
    * グラフ描画用データの返却
    * (エントリータイプ別総数|エントリータイプ別入所数|エントリータイプ別入所率) x 地域
    */
    private function getBarchartData($dates,$type)
    {

        // SubQuery用SQLの作成
        switch($type)
        {
            case 'zipcode_level_address':
                $indiv_sql = DB::table('entries')
                        ->select(DB::raw(
                            "CASE WHEN $type IS NULL THEN '不明' ELSE $type END AS $type,
                            zipcode,
                            entry_type,
                            CONCAT(email,'_',licence_type) AS email,
                            entry_date,
                            entered_date,
                            react_status"))
                        ->groupBy($type,'zipcode','entry_type','email','entry_date','entered_date','react_status','licence_type')
                        ->orderBy('entry_date');

                $total_sql = DB::table('entries')
                        ->select(DB::raw(
                            "CASE WHEN $type IS NULL THEN '不明' ELSE $type END AS $type,
                            zipcode,
                            CONCAT(email,'_',licence_type) AS email,
                            entry_date,
                            entered_date,
                            react_status"))
                        ->groupBy($type,'zipcode','email','entry_date','entered_date','react_status','licence_type')
                        ->orderBy('entry_date');
                break;

             default:
                $indiv_sql = DB::table('entries')
                        ->select(DB::raw(
                            "CASE WHEN $type IS NULL THEN '不明' ELSE $type END AS $type,
                            entry_type,
                            CONCAT(email,'_',licence_type) AS email,
                            entry_date,
                            entered_date,
                            react_status"))
                        ->groupBy($type,'entry_type','email','entry_date','entered_date','react_status','licence_type')
                        ->orderBy('entry_date');

                $total_sql = DB::table('entries')
                        ->select(DB::raw(
                            "CASE WHEN $type IS NULL THEN '不明' ELSE $type END AS $type,
                            CONCAT(email,'_',licence_type) AS email,
                            entry_date,
                            entered_date,
                            react_status"))
                        ->groupBy($type,'email','entry_date','entered_date','react_status','licence_type')
                        ->orderBy('entry_date');
                break;
        }
        

        $indiv_subQuery = DB::raw("({$indiv_sql->toSql()}) AS U");
        $total_subQuery = DB::raw("({$total_sql->toSql()}) AS U");
        
        // エントリー数 * Entry_type別
        $entry_counts          = DB::table($indiv_subQuery)
                                    ->whereNotNull($type)
                                    ->where($type,'<>','不明')
                                    ->whereBetween('entry_date',[$dates['dt_from'], $dates['dt_until']])
                                    ->select(DB::raw("$type,COUNT(DISTINCT email) AS metrics,entry_type"))
                                    ->groupBy($type,'entry_type')
                                    ->orderBy('metrics','DESC')
                                    ->limit(count($this->entry_types) * 20)
                                    ->get();

        // エントリー数 総合
        $total_entry_counts    = DB::table($total_subQuery)
                                    ->whereNotNull($type)
                                    ->where($type,'<>','不明')
                                    ->whereBetween('entry_date',[$dates['dt_from'], $dates['dt_until']])
                                    ->select(DB::raw("$type,COUNT(DISTINCT email) AS metrics"))
                                    ->groupBy($type)
                                    ->orderBy('metrics','DESC')
                                    ->limit(20)
                                    ->get();

        // 入所数 * Entry_type別
        $entering_counts       = DB::table($indiv_subQuery)
                                    ->whereNotNull($type)
                                    ->where($type,'<>','不明')
                                    ->whereBetween('entered_date',[$dates['dt_from'], $dates['dt_until']])
                                    ->select(DB::raw("$type,entry_type,COUNT(DISTINCT CASE WHEN react_status = 'entered' THEN email ELSE NULL END) AS metrics"))
                                    ->groupBy($type,'entry_type')
                                    ->orderBy('metrics','DESC')
                                    ->limit(count($this->entry_types) * 20)
                                    ->get();

        // 入所数 総合
        $total_enter_counts    = DB::table($total_subQuery)
                                    ->whereNotNull($type)
                                    ->where($type,'<>','不明')
                                    ->whereBetween('entered_date',[$dates['dt_from'], $dates['dt_until']])                                    
                                    ->select(DB::raw("$type,COUNT(DISTINCT CASE WHEN react_status = 'entered' THEN email ELSE NULL END) AS metrics"))
                                    ->groupBy($type)
                                    ->orderBy('metrics','DESC')
                                    ->limit(20)
                                    ->get();        

        switch($type)
        {            
            case 'zipcode_level_address':
                $entry = DB::table($total_subQuery)
                        ->whereNotNull($type)                        
                        ->where($type,'<>','不明')                        
                        ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                        ->whereNotNull('zipcode')
                        ->whereNotNull('zipcode_level_address')
                        ->select(DB::raw("COUNT(DISTINCT email) as count"))
                        ->first()
                        ->count;
                break;
            default:
                $entry = DB::table($total_subQuery)                        
                        ->whereNotNull($type)
                        ->where($type,'<>','不明')
                        ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                        ->select(DB::raw("COUNT(DISTINCT email) as count"))
                        ->first()
                        ->count;
                break;
        }
        

        $entering_rate         = DB::table($indiv_subQuery)
                                    ->whereNotNull($type)
                                    ->where($type,'<>','不明')
                                    ->whereBetween('entry_date',[$dates['dt_from'], $dates['dt_until']])                                    
                                    ->select(DB::raw("$type,entry_type,COUNT(DISTINCT CASE WHEN react_status = 'entered' THEN email ELSE NULL END) / ${entry} AS metrics"))
                                    ->groupBy($type,'entry_type')
                                    ->orderBy('metrics','DESC')
                                    ->limit(count($this->entry_types) * 20)
                                    ->get();        

        $total_enter_rate      = DB::table($total_subQuery)
                                    ->whereNotNull($type)
                                    ->where($type,'<>','不明')
                                    ->whereBetween('entry_date',[$dates['dt_from'], $dates['dt_until']])
                                    ->select(DB::raw("$type,COUNT(DISTINCT CASE WHEN react_status = 'entered' THEN email ELSE NULL END) / ${entry} AS metrics"))
                                    ->groupBy($type)
                                    ->orderBy('metrics','DESC')
                                    ->limit(20)
                                    ->get();
        

        $sum = ['web_entry' => 0,
                'entering_count_of_web_entry' => 0,
                'entering_rate_of_web_entry' => 0];


        foreach($entry_counts as $indiv_entry_count)
        {
            if(isset($sum[$indiv_entry_count->entry_type]))
            {
                $sum[$indiv_entry_count->entry_type] += $indiv_entry_count->metrics;
            }else{
                $sum[$indiv_entry_count->entry_type] = $indiv_entry_count->metrics;
            }            
        }

        foreach($total_entry_counts as $elem)
        {
            $sum['web_entry'] += $elem->metrics;
        }
        

        foreach($entering_counts as $indiv_entering_count)
        {
            if(isset($sum["entering_count_of_{$indiv_entering_count->entry_type}"]))
            {
                $sum["entering_count_of_{$indiv_entering_count->entry_type}"] += $indiv_entering_count->metrics;  
            }else{
                $sum["entering_count_of_{$indiv_entering_count->entry_type}"] = $indiv_entering_count->metrics;  
            }
        }

        foreach($total_enter_counts as $elem)
        {
            $sum['entering_count_of_web_entry'] += $elem->metrics;
        }


        foreach($entering_rate as $indiv_entering_rate)
        {
            if(isset($sum["entering_rate_of_{$indiv_entering_rate->entry_type}"]))
            {
                $sum["entering_rate_of_{$indiv_entering_rate->entry_type}"] += $indiv_entering_rate->metrics;
            }else{
                $sum["entering_rate_of_{$indiv_entering_rate->entry_type}"] = $indiv_entering_rate->metrics;
            }            
        }

        foreach($total_enter_rate as $elem)
        {
            $sum['entering_rate_of_web_entry'] += $elem->metrics;
        }

        

        return ['each' => 
                    [
                        'entry' => $entry_counts,
                        'enter' => $entering_counts,
                        'rate'  => $entering_rate
                    ],                
                'web_entry' => 
                    [
                        'entry' => $total_entry_counts,
                        'enter' => $total_enter_counts,
                        'rate'  => $total_enter_rate,
                    ],
                'sum'  => $sum
                ];
    }


    /**
     * Google Map描画用Geo Dataの取得
     */
    private function getUserGeoDatas($dates)
    {
        $entry_types = [];
        foreach(opt('db_config.entry_types_en_jp_map') as $key => $val_array)
        {
            foreach($val_array as $key => $val)
            {
                $entry_types[] = $key;
            }
        }

        $licence_types = [];
        foreach(opt('db_config.licence_types_en_jp_map') as $key => $val_array)
        {
            foreach($val_array as $key => $val)
            {
                $licence_types[] = $key;
            }
        }



        $result = [];


        foreach($licence_types as $licence_type)
        {
            foreach($entry_types as $entry_type)
            {
                $result[$licence_type . '_' . $entry_type] = DB::table('customers')
                                                            ->join('entries','customers.email','=','entries.email')
                                                            ->select('lat','lng')
                                                            ->where('entries.react_status','<>','entered')
                                                            ->where('entries.entry_type','=',$entry_type)
                                                            ->where('entries.licence_type','=',$licence_type)
                                                            ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                                            ->orderBy('customers.updated_at','DESC')
                                                            ->limit(1000)
                                                            ->get();
            }
            $result[$licence_type . '_entered'] = DB::table('customers')
                                                    ->join('entries','customers.email','=','entries.email')
                                                    ->select('lat','lng')
                                                    ->where('entries.react_status','=','entered')
                                                    ->where('entries.licence_type','=',$licence_type)
                                                    ->whereBetween('entry_date',[$dates['dt_from'],$dates['dt_until']])
                                                    ->orderBy('customers.updated_at','DESC')
                                                    ->limit(1000)
                                                    ->get();
        }        
        
        return $result;
    }    
}
