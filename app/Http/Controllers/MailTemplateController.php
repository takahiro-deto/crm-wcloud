<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Template;

class MailTemplateController extends Controller
{
    /**
     * Template 一覧を取得するAPI
     */
    public function fetchTemplate(Request $request)
    {           
        $brochure_templates = Template::where('purpose',0)->get()->toArray();
        $other_templates    = Template::where('purpose',1)->get()->toArray();

        return ['brochure' => $brochure_templates, 'other' => $other_templates];
    }


    /**
     * Mail Template更新用メソッド
     */
    public function updateMailTemplate(Request $request)
    {        
        $update_reason = $request->params['update_reason'];
        $return_msg = '';

        switch($update_reason)
        {
            case 'change_auto_reply_template':
                $id = $request->params['id'];

                Template::where('auto_sending_flag',1)->update(['auto_sending_flag' => 0]);
                
                if(intval($id) !== 0){                
                    Template::find($id)->update(['auto_sending_flag' => 1]); 
                }

                $return_msg = "success_changed_auto_reply_template";

                break;

            case 'change_mail_template_content':

                if($request->params['auto_sending_interval']){
                    $after = [
                        'template_name'             => $request->params['template_name'],
                        'auto_sending_interval'     => $request->params['auto_sending_interval'],
                        'mail_title'                => $request->params['mail_title'],
                        'mail_content'              => $request->params['mail_content'],
                    ];    
                }else{
                    $after = [
                        'template_name'             => $request->params['template_name'],                      
                        'mail_title'                => $request->params['mail_title'],
                        'mail_content'              => $request->params['mail_content'],
                    ];
                }
                

                Template::find($request->params['id'])->update($after);

                $return_msg = "success_changed_mail_template_content";
            default:
                break;
        }

        return ['msg' => $return_msg];
    }


    /**
     * Mailtemplate削除用
     */
    public function deleteMailTemplate(Request $request)
    {        
        try{            
            Template::destroy($request->query()['id']);
            return ['msg' => 'success_deleting'];
        }catch(Exception $e){
            return ['msg' => 'failed_deleteing'];
        }
        
    }


    /**
     * Mail Template 新規作成
     */
    public function createMailTemplate(Request $request)
    {
        $params = $request->params;        

        // Nullの場合にDefault値を適用
        if(! isset($params['auto_sending_interval']))
        {
            $params['auto_sending_interval'] = 1;   
        }
        if(! isset($params['auto_sending_flag']))
        {
            $params['auto_sending_flag'] = 0;
        }

        try{            
            Template::create($params);
            return ['msg' => 'success'];
        }catch(Exception $e){
            return ['msg' => 'failed'];
        }
        
    } 



}
