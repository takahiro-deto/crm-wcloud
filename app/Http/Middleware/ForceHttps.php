<?php

namespace App\Http\Middleware;

use Closure;

class ForceHttps
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // production 及び staging 環境では https を強制
        if (env('APP_ENV') === 'production' || env('APP_ENV') === 'staging') {
            if (env('HTTP_X_FORWARDED_PROTO') !== 'https') {
                return redirect()->secure($request->getRequestUri());
            }
        }

        return $next($request);
    }
}
