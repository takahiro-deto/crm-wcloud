<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class RestrictionIPAddress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = \Request::ip();

        $admited_ip_array = preg_split('/,/', DB::table('configures')->select('accsesible_ip_address')->first()->accsesible_ip_address);


        foreach($admited_ip_array as $admited_ip)
        {
            if($ip !== $admited_ip)
            {
                abort('403');
            }
        }


        return $next($request);
    }
}
