<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Crypt;
use App\Models\UserDatabase;

class ChangeDatabase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = $request->getHttpHost();
        UserDatabase::change($host);

        return $next($request);
    }
}
