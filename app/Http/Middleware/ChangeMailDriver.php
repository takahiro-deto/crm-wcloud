<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Illuminate\Mail\MailServiceProvider;

class ChangeMailDriver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = $request->getHttpHost();

        // demo環境は、logドライバーでメール送信
        if ($host === 'demo.gcp.wcloud.jp') {
            Config::set('mail.driver', 'log');
            (new MailServiceProvider(app()))->register();
        }

        return $next($request);
    }
}
