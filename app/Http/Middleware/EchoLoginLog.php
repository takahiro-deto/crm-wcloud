<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

class EchoLoginLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        DB::table('loginlogs')
            ->insert([
                'ip_address'        => \Request::ip(),
                'account_name'      => $request->params['id'],
                'host'              => $request->getSchemeAndHttpHost(),
                'access_datetime'   => Carbon::now()->format('Y-m-d H:i:s'),
                'user_agent'        => $request->header('User-Agent'),
            ]);

        return $response;
    }
}
