<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Crypt;
use Config;

class UserDatabase extends Model
{
    protected $connection = 'coredb';

	protected $guarded = [];

    public static function change($host)
    {        
        $database = UserDatabase::where('web_host', $host)->firstOrFail();
        UserDatabase::reconnect($database);

        return $database;

    }

    public static function reconnect($database)
    {
        Config::set('database.connections.mysql.host', $database->db_host);
        Config::set('database.connections.mysql.database', $database->db_name);
        Config::set('database.connections.mysql.username', $database->db_user);
        Config::set('database.connections.mysql.password', Crypt::decrypt($database->db_password));

        DB::purge('mysql');
        DB::reconnect('mysql');
    }
}
