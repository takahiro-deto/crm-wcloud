<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $casts = [
        'val' => 'array'
    ];
    /**
     * キーに対応するオプションを取得
     *
     * @param string $key
     * @return mixed
     */
    public static function val($key)
    {
        $row = Option::where('key', $key)->first();
        if (empty($row)) {
            return '';
        }
        return $row->val;
    }
}
