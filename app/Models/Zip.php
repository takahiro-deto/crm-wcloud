<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Zip extends Model
{
    protected $connection = 'coredb';

	protected $guarded = [];

	/**
     * 住所の文字列を、都道府県/市区郡/それ以下に分割（pref,city,townキー）
     *
     * @param string $address
     * @return array
     */
    public static function splitAddress($address)
    {
        $address = preg_replace('/[\r\n]+/', '', trim($address));

        if (! preg_match('/^(...??[都道府県])(.*)/u', $address, $matches)) {
            // 都道府県名から始まらない場合は、townに全て格納して戻す
            return ['pref' => '', 'city' => '', 'town' => $address];
        }

        $pref = $matches[1];

        $len = Zip::where('type', 0)->max(DB::raw('char_length(city)'));
        if (empty($len)) {
            // 市区郡データがない場合は、判定できないためprefとtownのみ分ける
            $town = mb_substr($address, mb_strlen($pref));
            return ['pref' => $pref, 'city' => '', 'town' => $matches[2]];
        }

        // 市区郡を1文字ずつ減らしながら、該当する市区郡を探す
        do {
            $city = mb_substr($matches[2], 0, $len);

            $found = Zip::where([
                ['type', 0],
                ['pref', $pref],
                ['city', $city],
            ])->count();

            if (! empty($found)) {
                break;
            }
        } while (--$len > 0);

        if (empty($len)) {
            // 市区郡に該当しない場合は、判定できないためprefとtownのみ分ける
            $town = mb_substr($address, mb_strlen($pref));
            return ['pref' => $pref, 'city' => '', 'town' => $town];
        }

        $town = mb_substr($address, mb_strlen($pref . $city));
        return ['pref' => $pref, 'city' => $city, 'town' => $town];
    }	    
}
