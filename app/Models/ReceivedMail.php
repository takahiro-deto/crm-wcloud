<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReceivedMail extends Model
{
    protected $guarded = [];
}
