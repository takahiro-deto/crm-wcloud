<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use SoftDeletes;

    /**
    * @var array
    */
    protected $dates = ['deleted_at'];
    protected $guarded = [];


    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++
    +
    +  Relation
    +
    +++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    public function histories(){
        return $this->hasMany('App\Models\History');
    }



    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++
    +
    +  Query Scope Methods
    +
    +++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    /**
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */

    public function scopeStatus($query,$type)
    {        
        if($type)
        {
            // Status types
            $status_types = [];            
            $st_maps = opt("db_config.status_en_jp_map");
            foreach($st_maps as $st)
            {
                foreach($st as $key => $val)
                {
                    $status_types[] = $key;
                }
            }
            

            if(in_array($type, $status_types))
            {
                return $query->where('react_status',$type);
            }
        }

        return $query;
    }

    /**
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */

    public function scopeEntryType($query,$type)
    {
        if($type)
        {
            // Entry_types
            $entry_types = [];
            $et_maps = opt("db_config.entry_types_en_jp_map");
            foreach($et_maps as $et)
            {
                foreach($et as $key => $val)
                {
                    $entry_types[] = $key;
                }
            }
            $entry_types[] = 'other';


            if(in_array($type, $entry_types))
            {
                return $query->where('entry_type',$type);
            }
        }        

        return $query;
    }

    /**
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeLicenceType($query,$type)
    {        
        if($type)
        {
            // Licence types
            $licence_types = [];
            $lc_maps = opt("db_config.licence_types_en_jp_map");
            foreach($lc_maps as $lc){
                foreach($lc as $key => $val)
                {
                    $licence_types[] = $key;
                }
            }


            if(in_array($type, $licence_types))
            {
                return $query->where('licence_type',$type);
            }
        }

        return $query;
    }

    /**
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeFreeword($query,$type)    
    {
        $enable_freeword_search = opt("db_config.freeword_search_target");

        if($type)
        {            
            $type = urldecode($type);
            
            $tel_is_enable       = in_array('tel',$enable_freeword_search) || in_array('tel2',$enable_freeword_search);
            $email_is_enable     = in_array('email',$enable_freeword_search);
            $multibyte_is_enable = in_array('general1',$enable_freeword_search) || in_array('general2',$enable_freeword_search);


            // Tel number
            if($tel_is_enable && preg_match('/^\d+$/',$type)){
                return $query->where('tel','like',"%{$type}%")->orWhere('tel2','like',"%{$type}%");
            }
            // Mail Address
            else if($email_is_enable && preg_match('/[\w\d]+@.*\.(com|org|net|int|edu|gov|mil)$/',$type)){
                return $query->where('email',"$type");
            }
            // Name or Kana or email
            else if($multibyte_is_enable){
                return $query->where('name','like', "%{$type}%")
                            ->orWhere('kana','like',"%{$type}%")
                            ->orWhere('email','like',"%{$type}%")
                            ->orWhere('general1','like',"%{$type}%")
                            ->orWhere('general2','like',"%{$type}%");
            }            
        }

        return $query;
    }
}
