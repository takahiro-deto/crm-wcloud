<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserDatabase;
use App\Models\Customer;
use App\Models\Entry;
use App\Models\Template;
use App\Models\History;
use App\Mail\AutoSend;

use Carbon\Carbon;
use Mail;
use Crypt;
use DB;
use Config;
use Exception;
use ErrorNotice;

class MailAutosendCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:autosend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mail Auto send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $databases = UserDatabase::all();
        foreach ($databases as $database)
        {
            // demo環境はスキップ（対象が増えるならフラグ化を検討）
            if ($database->web_host === 'demo.gcp.wcloud.jp' || $database->web_host === '192.168.33.20')
            {
                continue;
            }

            UserDatabase::reconnect($database);

            $this->Info('Autosend mail for ' . $database->web_host . ' .');

            

            // エントリー種別情報を取得
            $types = opt('mail.parser_entry_types_object');
            if (empty($types)) {
                continue;
            }

            // 資料請求情報を取得
            $request = $types['brochure'];
            if (empty($request) || empty($request['use']) || empty($request['name'])) {
                continue;
            }            

            // ステータス取得
            $status = opt('mail.parser_status_types_object');
            if (empty($status)) {
                continue;
            }

            $responsed = $status['reacted'] ?? '対応済み';            

            // メールテンプレートの取得
            // $template = MailTemplate::where('template_type', 0)
            //     ->where('use', true)
            //     ->where('auto_send', '>', 0)
            //     ->first();
            
            $template = Template::where('purpose', 0)
                ->where('auto_sending_flag', 1)
                ->where('auto_sending_interval', '>', 0)
                ->first();

            // 自動送信テンプレートがなければ終了
            if (empty($template)) {
                continue;
            }

            // 送信対象を抽出
            // $entryTypes = EntryType::where('entry_type', $request['name'])
            //     ->whereNull('auto_send_date')
            //     ->whereNotNull('response_date')
            //     ->whereDate('response_date', '<=', Carbon::today()->subDay($template->auto_send))
            //     ->whereHas('entries', function ($query) use ($responsed) {
            //         $query->where('status', $responsed);
            //     });

            $targets = Entry::where('entry_type','brochure')
                            ->where('react_status','reacted')
                            ->where('disable_auto_send',0)
                            ->whereNull('auto_send_date')
                            ->whereNotNull('react_date')
                            ->whereDate('react_date','<=',Carbon::today()->subDay($template->auto_sending_interval))
                            ->get();

            $mailConfig = opt('mail.sender_config');
            Mail::alwaysFrom($mailConfig['from'], $mailConfig['name'] ?? null);

            // これ何？？
            if (! empty($mailConfig['reply'])) {
                Mail::alwaysReply($mailConfig['reply']);
            }


            foreach($targets as $target)
            {
                if(empty($target->email))
                {
                    continue;
                }

                
                // DB処理開始
                DB::beginTransaction();

                try
                {
                    // データ
                    // $data = [];
                    // $data['name'] = $target->name;

                    // **ここ確認 多分いける**
                    // $mailBody = view(['template' => $mailBody], $data)->render();


                    // 履歴テーブルに登録
                    History::create([
                        'email'             => $target->email,
                        'history_date'      => Carbon::now(),
                        'history_type'      => 'auto_sending',
                        'history_title'     => $template->mail_title,
                        'history_content'   => $template->mail_content,
                        'subject'           => $template->mail_title,
                        'body'              => $template->mail_content
                    ]);


                    // Targetのauto_send_dateカラムに日付を入れる
                    Entry::where('id',$target->id)
                            ->update(['auto_send_date' => Carbon::now()]);


                    // Mail::raw($mailBody, function($mailer) use ($target, $template) {
                    //         $mailer->subject($template->mail_title)
                    //             ->to(trim($target->email));
                    // });


                    // 本文作成（$氏名$ を {{ $name }} に変換）
                    $name = $target->name;                    
                    $template->mail_content = str_replace('$氏名$', $name, opt('mail.replace_string') . $template->body);
                    $option = $template;                    
                    Mail::to($target)->send(new AutoSend($template));


                }catch(Exception $e){

                    DB::rollback();
                    ErrorNotice::send($e);
                    continue;

                }

                DB::commit();
            }
        }
    }
}
