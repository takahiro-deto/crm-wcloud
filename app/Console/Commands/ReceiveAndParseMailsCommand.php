<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\UserDatabase;
use App\Models\MailBox;
use App\Models\Configure; 
use App\Models\ReceivedMail;
use App\Models\Customer;
use App\Models\Entry;
use App\Models\History;

use PhpImap\Mailbox as ImapMailbox;

use App\Exceptions\ErrorNotice;

use Mail;
use File;
use DB;
use Carbon\Carbon;
use Crypt;

class ReceiveAndParseMailsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:receive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive and Parse Mails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $databases = UserDatabase::all();
        foreach ($databases as $database)
        {
            UserDatabase::reconnect($database);

            if($database->web_host === '192.168.33.20' || $database->web_host === 'demo.gcp.wcloud.jp')
            {
                $this->Info('Skip ' . $database->web_host);
                continue;
            }

            $this->Info('Receiving mail for ' . $database->web_host . ' .');

            $mail_boxes = MailBox::all();

            foreach ($mail_boxes as $mail_box)
            {
                // 添付ファイル用ディレクトリ作成
                $dir = storage_path('app/' . $database->web_host . '/mailbox/' . $mail_box->id);
                
                if (! File::exists($dir))
                {
                    File::makeDirectory($dir, 0777, true, true);
                }


                // php-imapを生成
                try
                {
                    $imap = new ImapMailbox($mail_box->mail_box, $mail_box->username, Crypt::decrypt($mail_box->password), $dir);
                } catch (Exception $e) {
                    $this->error($e->getMessage());
                    ErrorNotice::send($e);
                    continue;
                }

                // メールマスタ情報
                $mailConfig = opt('mail.sender_config');

                // メール受信
                // メールボックス内の全メールID取得
                $mailsIds = $imap->searchMailbox('ALL');

                foreach ($mailsIds as $mailId)
                {
                    // メールデータ取得
                    $mail = $imap->getMail($mailId);
                    $body_raw = $imap->getRawMail($mailId);                    

                    try
                    {
                        // $this->info('begin(' . $mailId . ')');

                        // message-idが無いものは日付＋メールアドレスで生成
                        if (empty($mail->messageId)) {
                            $mail->messageId = '<' . $mail->date . '_' . $mail->fromAddress . '>';
                        }

                        // 重複チェック
                        if (ReceivedMail::where('message_id', $mail->messageId)->count() > 0) {
                            // 既に登録済みで31日以上経過しているものは削除
                            $mailDate = new Carbon($mail->date);
                            if ($mailDate->diffInDays(Carbon::now()) >= 31) {
                                $imap->deleteMail($mailId);
                            } else {
                                $this->info('skip(' . $mailId . ')');
                            }
                            continue;
                        }

                        // 配列化されたreplyToを文字列化
                        $replyTo = isset($mail->replyTo) ? implode(',', array_keys($mail->replyTo)) : null;

                        // トランザクション開始
                        DB::beginTransaction();

                        // MailBoxに格納
                        $id = ReceivedMail::insertGetId([
                            'mail_box_id' => $mail_box->id,
                            'message_id' => $mail->messageId,
                            'received_date' => $mail->date,
                            'from' => $mail->fromAddress,
                            'from_name' => $mail->fromName,
                            'reply_to' => $replyTo,
                            'subject' => $mail->subject,
                            'body' => $mail->textPlain ?: null,
                            'body_html' => $mail->textHtml ?: null,
                            'header_raw' => $mail->headersRaw,
                            'body_raw' => $body_raw,
                        ]);                        

                        $userAddress = $mail->fromAddress;


                        // Options取得
                        $opt = [];
                        $opt['entry_type'] = opt('mail.parser_entry_types_object');
                        $opt['status_types'] = opt('mail.parser_status_types_object');
                        $opt['via_routes_types'] = opt('mail.parser_via_routes_types_object');
                        $opt['licence_type'] = "";



                        // メール解析
                        $parserList = opt('mail.parser_class');

                        // メール情報格納変数
                        $parserName = '';
                        $content = [];
                        $types = null;
                        $entry = null;
                        $history_type = null;

                        foreach($parserList as $klass)
                        {
                            $parserKlass = 'App\\Mail\\Parser\\' . $klass;
                            $parser = new $parserKlass($mail);
                            if(! $parser->parseMail() )
                            {
                                continue;
                            }

                            // パーサー名取得
                            $parserName = $parser->name;

                            // メール情報取得
                            $content = $parser->content;                            

                            // userAddressの書き換え
                            $userAddress = $content['email'] ? $content['email'] : '';

                            $opt['licence_type'] = opt('mail.licence_type_map_of_' . $parserName);

                            // entry_typeのみ特殊値となるため取り出し
                            $types = array_pull($content, 'entry_type');                            

                            // 免許種類をセット
                            $content['licence_type'] = @$opt['licence_type'][$content['licence_type']] ?: null;

                            // 経路をセット
                            $content['via_routes'] = $opt['via_routes_types'][$parserName];

                            break;

                        }

                        // customer Model
                        $customer = Customer::orderBy('id', 'desc')
                                    ->firstOrNew(['email' => $userAddress]);

                        // 既存ユーザーではなく、定形メールでもない場合は取り込まずに転送する
                        if(! $customer->exists && empty($parserName))
                        {
                            // 転送

                            // From情報をメール先頭に付加
                            if (! empty($mail->fromName)) {
                                $fromMessage = $mail->fromName . ' <' . $mail->fromAddress . '>';
                            } else {
                                $fromMessage = $mail->fromAddress;
                            }

                            $fromMessage .= " 様からのメールを転送します。\n\n";

                            Mail::raw($fromMessage . $mail->textPlain,
                                function ($message) use ($mail, $mailConfig, $replyTo)
                                {
                                    $message->from($mailConfig['from'], @$mailConfig['name'] ?: null);
                                    $message->to($mailConfig['to']);

                                    // メール返信先を replyTo または From に設定
                                    //  invalid_address@.syntax-error. の場合は、元メールがRFC822に違反しており、
                                    //  replayTo へ設定するとエラーとなるため無視する
                                    $forwardReplayTo = $replyTo ?: $mail->fromAddress;
                                    if ($forwardReplayTo !== 'invalid_address@.syntax-error.')
                                    {
                                        $message->replyTo($replyTo ?: $mail->fromAddress, $mail->fromName);
                                    }

                                    $message->subject('【CRM未取り込み】' . $mail->subject);

                                    if (! empty($mail->textHtml))
                                    {
                                        $message->setBody($mail->textHtml, 'text/html');
                                    }

                                    $attachments = $mail->getAttachments();
                                    if (! empty($attachments))
                                    {
                                        foreach ($attachments as $attachment)
                                        {
                                            $message->attach($attachment->filePath);
                                        }
                                    }
                                }
                            );
                        
                        }else{
                            // 対応中フラグ -- 直書き
                            $content['react_status'] = "ongoing";

                            // 新規の場合は一度saveしておく
                            if(! $customer->exists )
                            {
                                $customer->save();
                            }

                            // 現在の住所を取得
                            $oldAddress = $customer->address_pref . $customer->address_city . $customer->address_town;

                            if(empty($types))
                            {
                                // typeがない = 返信などの問い合わせ                                
                                $history_types = ['received_mail'];

                                $recent_entry = Entry::where('email',$mail->fromAddress)->first();

                                if($recent_entry)
                                {
                                    $content = $recent_entry->toArray();
                                }

                                $content['entry_type'] = 'inquiry';
                                $content['entry_date'] = $mail->date;
                                $content['via_routes'] = 'その他';

                            }else{
                                // typesあり
                                $history_types = $types;
                            }

                            // *** NEW ***
                            //
                            // CRM利用項目で許可されてるもののみを
                            // Entryレコードとして登録する
                            // $insertable_items = opt('db_config.crm_using_item');
                            $insertable_items = [
                                // Customer Item
                                'name', 'kana', 'email', 'zipcode', 'address_pref', 'address_city', 'address_town', 'tel',
                                'tel2', 'age', 'birthday', 'sex', 'occupation', 'school',
                                // General Item
                                'general1', 'general2', 'general3', 'general4', 'general5', 
                                'general6', 'general7', 'general8', 'general9', 'general10',
                                // Entry Item
                                'entry_date', 'licence_type', 'react_status',
                                // Entry type Item
                                'entry_date', 'entry_type', 'via_routes', 'react_status', 'device', 'user_agent'
                            ];


                            // $entry_instance = new Entry();
                            $entry_object = [];

                            foreach($content as $key => $val)
                            {
                                if(! empty($val))
                                {
                                    if(in_array($key,$insertable_items))
                                    {                                        
                                        $entry_object[$key] = $val;
                                    }    
                                }                                
                            }

                            $entry_object['entry_title']   = $mail->subject;
                            $entry_object['entry_content'] = $mail->textPlain ?: null;

                            if(isset($content['zipcode']))
                            {
                                $zipcode_level_address = DB::table('coredb.zipcodes')
                                                            ->select('kanji_address')
                                                            ->where('zipcode','=',$content['zipcode'])
                                                            ->first();                                                            

                                if($zipcode_level_address)
                                {
                                    $entry_object['zipcode_level_address'] = $zipcode_level_address->kanji_address;
                                }
                            }


                            if($types)  // typesがある場合はEntryが複数できる可能性があるのでForループ *１メールで複数エントリーの可能性があるため
                            {
                                foreach($types as $type)
                                {
                                    $entry_object['entry_type'] = $type; // 
                                    Entry::create($entry_object);    
                                }    
                            }else{
                                $entry_object['entry_type'] = 'inquiry';  // 返信の場合はお問い合わせで確定
                                Entry::create($entry_object);    
                            }
                            

                            // *** NEW ***
                            //
                            // Customerテーブルに、Addressを追加（Google API用）

                            if(isset($content['address_pref'])) { $customer->address_pref = $content['address_pref']; }
                            if(isset($content['address_city'])) { $customer->address_city = $content['address_city']; }
                            if(isset($content['address_town'])) { $customer->address_town = $content['address_town']; }

                            $customer->save();


                            // Google Map
                            //Google map geocoding APIで住所から経度緯度を取得
                            if(! empty($customer->address_town))
                            {
                                $newAddress = $customer->address_pref . $customer->address_city;

                                if($oldAddress !== ($newAddress . $customer->address_town))
                                {
                                    $http_query = http_build_query([
                                        'key' => opt('common.google_api_key')['geo'],
                                        'address' => $newAddress . mb_convert_kana($customer->address_town, 'RNASK'),
                                    ]);

                                    $locResult = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?' . $http_query);

                                    if(! empty($locResult))
                                    {
                                        $locArray = json_decode($locResult, true);

                                        if(! empty($locArray['results']))
                                        {
                                            $customer->lat = $locArray['results'][0]['geometry']['location']['lat'];
                                            $customer->lng = $locArray['results'][0]['geometry']['location']['lng'];
                                        }else{
                                            $customer->lat = null;
                                            $customer->lng = null;
                                        }
                                    }
                                }

                            }else{

                                $customer->lat = null;
                                $customer->lng = null;

                            }

                            $customer->save();
                            

                            $history_object = [
                                'history_title'     => $mail->subject ?: '（無題）',
                                'history_content'   => $mail->textPlain ?: null,
                                'history_date'      => $mail->date,
                                'subject'           => $mail->subject ?: '（無題）',
                                'body'              => $mail->textPlain,
                                'body_html'         => $mail->textHtml,
                                'reply_to'          => $replyTo,
                                'receive_mail_id'   => $id,
                            ];

                            if(! empty($entry_object['email']))
                            {
                                $history_object['email'] = $entry_object['email'];

                                if(array_search('inquiry', $history_types) && array_search('brochure', $history_types))
                                {
                                    $history_object['history_type'] = 'brochure';

                                    // History Insert
                                    History::create($history_object);

                                }else{

                                    foreach($history_types as $history_type)
                                    {
                                        $history_object['history_type'] = $history_type;
                                        
                                        // History Insert
                                        History::create($history_object);
                                    }    

                                }
                                
                            }else if(isset($history_types) && array_search('received_mail', $history_types)){

                                $history_object['email']            = $mail->fromAddress;
                                $history_object['history_type']     = 'received_mail';


                                // History Insert
                                History::create($history_object);
                            }
                        }

                        DB::commit();



                    }catch(Exception $e){
                        $this->error($e->getMessage());
                        ErrorNotice::send($e);
                        DB::rollback();
                        continue;
                    }
                }
            }
        }
    }
}
