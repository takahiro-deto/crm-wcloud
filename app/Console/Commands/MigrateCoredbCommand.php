<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateCoredbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:coredb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the database migrations for coredb';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate', array('--database' => 'coredb', '--path' => 'database/migrations/coredb'));
    }
}
