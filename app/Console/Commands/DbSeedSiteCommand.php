<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserDatabase;
use Crypt;
use Config;
use DB;

class DbSeedSiteCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed:site {host} {--class=DatabaseSeeder} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records with site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $host = $this->argument('host');

        $database = UserDatabase::change($host);

        $this->Info('Receiving mail for ' . $database->web_host . ' .');

        $class = $this->option('class');
        $force = $this->option('force');

        $options = [
            '--class' => 'Site_' . str_replace('.', '_', $host) . '\\' . $class,
            '--force' => $force,
        ];

        $this->call('db:seed', $options);
    }
}
