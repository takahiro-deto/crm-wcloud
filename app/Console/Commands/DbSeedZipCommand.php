<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DbSeedZipCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed:zip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records for zip';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running db:seed:zip for coredb.');
        $this->call('db:seed', array('--database' => 'coredb', '--class' => 'ZipsTableSeeder'));
    }
}
