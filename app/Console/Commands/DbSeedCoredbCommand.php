<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DbSeedCoredbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed:coredb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records for coredb';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running db:seed for coredb.');
        $this->call('db:seed', array('--database' => 'coredb', '--class' => 'UserDatabasesTableSeeder'));
    }
}
