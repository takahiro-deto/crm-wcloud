<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserDatabase;
use Config;
use Crypt;
use DB;

class MigrateUserdbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:userdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the database migrations for user_databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $databases = UserDatabase::all();
        foreach ($databases as $database) {
            UserDatabase::reconnect($database);

            $this->Info('Running migration for ' . $database->db_host . ' .');

            $this->call('migrate', array('--database' => 'mysql', '--path' => 'database/migrations'));
        }
    }
}
