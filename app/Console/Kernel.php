<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MigrateCoredbCommand::class,
        Commands\MigrateCoredbResetCommand::class,
        Commands\MigrateUserdbCommand::class,
        Commands\DbSeedCoredbCommand::class,
        Commands\DbSeedZipCommand::class,
        Commands\DbSeedSiteCommand::class,
        Commands\ReceiveAndParseMailsCommand::class,
        Commands\MailAutosendCommand::class,        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('mail:receive')
            ->everyFiveMinutes()
            ->withoutOverlapping();
        $schedule->command('mail:autosend')
            ->hourly()
            ->between('10:00', '18:00')
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
