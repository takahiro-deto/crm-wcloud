<?php

namespace App\Exceptions;

use Exception;
use Mail;
use Log;

class ErrorNotice {

    /**
     * 例外を通知
     *
     * @param Exception
     */
    public static function send(Exception $e)
    {
        $body = '[' . date('Y-m-d H:i:s') . '] ' . $e->getMessage();
        $body .= "\n";
        $body .= $e->getTraceAsString();

        try {
            //Mail::raw($body, function ($message) {
            //    $message->from('webmaster@wcors.wcloud.jp');
            //    $message->to('wcors-error@wkwk.co.jp');
            //    $message->subject('【WCORS】ErrorNotice');
            //});
            Log::error($body);
        } catch (Exception $e) {

        }
    }
}