<?php
	/**
	 * 自作ヘルパー
	 */
	use App\Models\Option;
	use App\Models\Zip;

	/**
	 * Optionsテーブルの値を取得
	 *
	 * @param string $key
	 * @return mixed
	 */
	function opt($key)
	{
	    return Option::val($key);
	}

	/**
	 * 住所の文字列を、都道府県/市区郡/それ以下に分割（pref,city,townキー）
	 *
	 * @param string $address
	 * @return array
	 */
	function split_address($address)
	{
	    return Zip::splitAddress($address);
	}
