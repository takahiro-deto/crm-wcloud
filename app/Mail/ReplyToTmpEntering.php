<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyToTmpEntering extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of Template
     */
    protected $template = [];


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($options)
    {
        $this->template['title'] = $options['title'];
        $this->template['content'] = $options['content'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_config = opt('mail.sender_config');
        $from = $mail_config['from'];
        $name = $mail_config['name'];

        return $this->from($from,$name)
                    ->subject($this->template['title'])
                    ->text('emails.reply.tmpentering')
                    ->with(['content' => $this->template['content']]);
    }
}
