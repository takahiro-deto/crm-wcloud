<?php

namespace App\Mail\Parser;

class DseMailParser extends MailParser
{
    /**
     * メールパターン名
     *
     * @var string
     */
    protected $name = 'dse';

    /**
     * メール From のパターンリスト
     *
     * @var array
     */
    protected $fromPattern = [
        '/.+/i',
    ];

    /**
     * メール Subject に適用するパターンリスト
     *
     * @var array
     */
    protected $subjectPattern = [
        '/^【D.O.L.】資料請求 - 【([^】]+)】(.+)$/ui' => [0 => ['entry_type' => ['brochure']], 1 => 'device'],
    ];

    /**
     * メール Body に適用するパターンリスト
     *
     * @var array
     */
    protected $bodyPattern = [
        '/^【受付時間】　(\d{1,4}\/\d{1,2}\/\d{1,2} \d{1,2}:\d{1,2})/uim' => [1 => 'entry_date'],
        '/^【名前】\n(.+)\n(.+)/uim' => [1 => 'name', 2 => 'kana'],
        '/^【性別】\n([男女])/uim' => [1 => 'sex'],
        '/^【年齢】\n(\d+)/uim' => [1 => 'age'],
        '/^【住所（資料送付先）】\n(.+)\n(.+)/uim' => [1 => 'zipcode', 2 => 'address'],
        '/^【電子メール】\n(.+)/uim' => [1 => 'email'],
        '/^【電話番号】\n(.+)/uim' => [1 => 'tel'],
        '/^第1希望 : (.+)/uim' => [1 => 'licence_type'],
        '/^第2希望 : (.+)/uim' => [1 => 'licence_type2'],
        '/^第3希望 : (.+)/uim' => [1 => 'licence_type3'],
        '/^【所持免許】\n([^【]+)/uim' => [1 => 'has_licence'],
        '/^【職業】\n(.+)/uim' => [1 => 'occupation'],
        '/^【備考】\n(.+)\n------------------------------------------------------------------\n　/uims' => [1 => 'body'],
    ];

    protected function customizeContent()
    {
        // entry_date は、年/月/日 時:分のため、秒を加える。秒は0とする。
        $this->content['entry_date'] .= ':00';

        // 質問内容がある時かつ、資料請求かつ問い合わせ・仮申込みがない場合に、問い合わせを加える
        $this->content['body'] = trim($this->content['body'] ?? '');
        if (! empty($this->content['body'])) {
            if (in_array('brochure', $this->content['entry_type']) && ! in_array('inquiry', $this->content['entry_type']) && ! in_array('tmp_entering', $this->content['entry_type'])) {
                // HistoryをはじめのEntryTypeに紐つけるため、unshiftでcontactを先頭におく
                array_unshift($this->content['entry_type'], 'inquiry');
            }
        }

        // 第2、第3希望免許
        $licence_type_etc = [];
        $opt_lic = opt('crm.lic_type_dse');
        for ($i = 2; $i <= 3; ++$i) {
            $licence_type = trim($this->content['licence_type' . $i] ?? '');
            if (isset($opt_lic[$licence_type])) {
                $licence_type_etc[] = $opt_lic[$licence_type];
            }
        }
        if (count($licence_type_etc) > 0) {
            $this->content['licence_type_etc'] = implode($licence_type_etc, ',');
        }
    }
}
