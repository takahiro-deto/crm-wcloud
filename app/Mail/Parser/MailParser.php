<?php

namespace App\Mail\Parser;

use PhpImap\IncomingMail;
use Carbon\Carbon;

abstract class MailParser
{
    /**
     * メールパターン名
     * @var string
     */
    protected $name;

    /**
     * メール From のパターンリスト
     * パターンのみの配列 [pattern, pattern, ...]
     *
     * @var array
     */
    protected $fromPattern;

    /**
     * メール Subject に適用するパターンリスト
     * パターンに対し、マッチした値を $this->content に格納するための情報を定義する
     * 0は全体マッチとなり値を絞り込めないためキーと値を定義、その他はキーのみを定義する
     * [pattern => [0 => [key => val], 1 => key, 2 => key, ...], pattern => ...]
     *
     * @var array
     */
    protected $subjectPattern;

    /**
     * メール Body に適用するパターンリスト
     * パターンに対し、マッチした値を $this->content に格納するための情報を定義する
     * 0は全体マッチとなり値を絞り込めないためキーと値を定義、その他はキーのみを定義する
     * [pattern => [0 => [key => val], 1 => key, 2 => key, ...], pattern => ...]
     *
     * @var array
     */
    protected $bodyPattern;

    /**
     * パースしたメール情報を格納する変数
     *
     * @var array
     */
    public $content = [];

    protected $mail         = null;
    protected $fromAddress  = '';
    protected $subject      = '';
    protected $body         = '';

    /**
     * コンストラクタ
     *
     * @return void
     */
    public function __construct(IncomingMail $mail)
    {
        //parent::__construct();
        $this->mail = $mail;
        $this->fromAddress = $mail->fromAddress;
        $this->subject = $mail->subject;
        $this->body = preg_replace('/(\r\n|\r|\n)/', "\n", $mail->textPlain);
    }

    /**
     * get accessor
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            $function = 'get' . studly_case($name) . 'Attribute';
            if (method_exists($this, $function)) {
                return $this->$function();
            }
        }
    }

    /**
     * name accessor
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->name;
    }

    /**
     * パターンリストから $this->content へ格納する
     *
     * @return boolean
     */
    protected function pattern2content($patternList, $subject)
    {
        foreach ($patternList as $pattern => $configList) {
            $matches = null;
            if (preg_match($pattern, $subject, $matches)) {
                foreach ($configList as $no => $config) {
                    if ($no == 0) {
                        $this->content = array_merge($this->content, $config);
                    } else {
                        $this->content = array_merge($this->content, [$config => trim($matches[$no] ?? null)]);
                    }
                }
            }
        }
    }

    /**
     * メール解析
     *
     * @return boolean
     */
    public function parseMail()
    {
        $fromCheck = false;

        // fromパターンのチェック
        foreach ($this->fromPattern as $pattern) {
            if (preg_match($pattern, $this->fromAddress)) {
                $fromCheck = true;
                break;
            }
        }

        // 該当がなければfalse
        if (! $fromCheck) {
            return false;
        }

        // 定義されたパターンによって情報を抽出
        $this->pattern2content($this->subjectPattern, $this->subject);
        $this->pattern2content($this->bodyPattern, $this->body);

        // entry_typeを抽出できない場合はfalse
        if (empty($this->content['entry_type'])) {
            return false;
        }

        // customizeContentメソッドが定義されている場合は実行
        if (method_exists($this, "customizeContent")) {
            $this->customizeContent();
        }

        // entry_date が取れない場合は、メール日付とする。
        if (empty($this->content['entry_date'])) {
            $this->content['entry_date'] = new Carbon($this->mail->date);
        }

        // 郵便番号、電話番号から〒やハイフンを削除
        if (! empty($this->content['zipcode'])) {
            $this->content['zipcode'] = mb_convert_kana($this->content['zipcode'], 'n');
            $this->content['zipcode'] = str_replace('〒', '', $this->content['zipcode']);
            $this->content['zipcode'] = str_replace('-', '', $this->content['zipcode']);
        }
        if (! empty($this->content['tel'])) {
            $this->content['tel'] = mb_convert_kana($this->content['tel'], 'n');
            $this->content['tel'] = str_replace('-', '', $this->content['tel']);
        }
        if (! empty($this->content['tel2'])) {
            $this->content['tel2'] = mb_convert_kana($this->content['tel2'], 'n');
            $this->content['tel2'] = str_replace('-', '', $this->content['tel2']);
        }

        // age_dateとbirthdayを日付型に変換
        // if (! empty($this->content['age_date'])) {
        //     $this->content['age_date'] = new Carbon($this->content['age_date']);
        // }
        if (! empty($this->content['birthday'])) {
            $this->content['birthday'] = new Carbon($this->content['birthday']);
        }

        // 住所を分割して格納
        if (! empty($this->content['address'])) {
            $address = split_address($this->content['address']);
            $this->content['address_pref'] = $address['pref'];
            $this->content['address_city'] = $address['city'];
            $this->content['address_town'] = $address['town'];
        }

        return true;
    }
}