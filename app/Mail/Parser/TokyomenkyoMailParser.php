<?php

namespace App\Mail\Parser;

use Carbon\Carbon;

class TokyomenkyoMailParser extends MailParser
{
    /**
     * メールパターン名
     *
     * @var string
     */
    protected $name = 'tokyomenkyo';

    /**
     * メール From のパターンリスト
     *
     * @var array
     */
    protected $fromPattern = [
        '/jkk@tokyo-menkyo\.or\.jp/i',
    ];

    /**
     * メール Subject に適用するパターンリスト
     *
     * @var array
     */
    protected $subjectPattern = [
        '/^【東京で免許】 資料請求受付申し込み通知$/ui' => [0 => ['entry_type' => ['brochure']]],
    ];

    /**
     * メール Body に適用するパターンリスト
     *
     * @var array
     */
    protected $bodyPattern = [
        '/^【資料請求受付日】\n　　(\d{1,4})年(\d{1,2})月(\d{1,2})日/uim' => [1 => 'entry_date_yy', 2 => 'entry_date_mm', 3 => 'entry_date_dd'],
        '/^【資料請求希望車種】\n　　(.+)/uim' => [1 => 'licence_type'],
        '/^【名前（漢字）】\n　　(.+)/uim' => [1 => 'name'],
        '/^【名前（フリガナ）】\n　　(.+)/uim' => [1 => 'kana'],
        '/^【電話番号】\n　　(.+)/uim' => [1 => 'tel'],
        '/^【メールアドレス】\n　　(.+)/uim' => [1 => 'email'],
        '/^【住所】\n　　(.+)\n　　(.+)/uim' => [1 => 'zipcode', 2 => 'address'],
        '/^【備考】\n(.+)\n※下記より『東京で免許 挨拶状』/uims' => [1 => 'body'],
    ];

    protected function customizeContent()
    {
        // entry_date は、yy年mm月dd日のため、時間を加える。
        $mailDate = new Carbon($this->mail->date);
        $this->content['entry_date'] = $this->content['entry_date_yy'] . '/'
                                  . $this->content['entry_date_mm'] . '/'
                                  . $this->content['entry_date_dd'] . ' '
                                  . $mailDate->toTimeString();

        // 質問内容がある時かつ、資料請求かつ問い合わせ・仮申込みがない場合に、問い合わせを加える
        $this->content['body'] = trim($this->content['body'] ?? null);
        if (! empty($this->content['body'])) {
            if (in_array('brochure', $this->content['entry_type']) && ! in_array('inquiry', $this->content['entry_type']) && ! in_array('tmp_entering', $this->content['entry_type'])) {
                // HistoryをはじめのEntryTypeに紐つけるため、unshiftでcontactを先頭におく
                array_unshift($this->content['entry_type'], 'inquiry');
            }
        }
    }
}
