<?php

namespace App\Mail\Parser;

class GomenkyoMailParser extends MailParser
{
    /**
     * メールパターン名
     *
     * @var string
     */
    protected $name = 'gomenkyo';

    /**
     * メール From のパターンリスト
     *
     * @var array
     */
    protected $fromPattern = [
        '/info@go-menkyo\.com/i',
    ];

    /**
     * メール Subject に適用するパターンリスト
     *
     * @var array
     */
    protected $subjectPattern = [
        '/^教習所へ行こう！\[クーポン\]\[自動送信\]$/ui' => [0 => ['entry_type' => ['tmp_entering']]],
    ];

    /**
     * メール Body に適用するパターンリスト
     *
     * @var array
     */
    protected $bodyPattern = [
        '/^■お名前（漢字）\n　(.+)/uim' => [1 => 'name'],
        '/^■お名前（フリガナ）\n　(.+)/uim' => [1 => 'kana'],
        '/^■生年月日\n　(.+)/uim' => [1 => 'birthday'],
        '/^■年齢\n　(\d+)/uim' => [1 => 'age'],
        '/^■性別\n　([男女])/uim' => [1 => 'sex'],
        '/^■住所\n　(.+)/uim' => [1 => 'address'],
        '/^■電話番号\n　(.+)/uim' => [1 => 'tel'],
        '/^■メールアドレス\n　(.+)/uim' => [1 => 'email'],
        '/^■取得希望車種\n　(.+)/uim' => [1 => 'licence_type'],
        '/^■入校予定日\n　(.+)/uim' => [1 => 'scheduled'],
        '/^■発券番号\n　(.+)/uim' => [1 => 'coupon'],
    ];
}
