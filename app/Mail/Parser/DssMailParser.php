<?php

namespace App\Mail\Parser;

use Carbon\Carbon;

class DssMailParser extends MailParser
{
    /**
     * メールパターン名
     *
     * @var string
     */
    protected $name = 'dss';

    /**
     * メール From のパターンリスト
     *
     * @var array
     */
    protected $fromPattern = [
        '/contact@wakuwakuport\.com/i',
    ];

    /**
     * メール Subject に適用するパターンリスト
     *
     * @var array
     */
    protected $subjectPattern = [
        '/^【教習所サーチからお問い合わせがありました】ご確認ください。$/ui' => [
        	0 => ['entry_type' => ['inquiry']]
        ],
        '/^【教習所サーチから資料請求がありました】ご確認ください。$/ui' => [
        	0 => ['entry_type' => ['brochure']]
        ],
        '/^【教習所サーチから仮申し込みがありました】ご確認ください。$/ui' => [
        	0 => ['entry_type' => ['tmp_entering']]
        ],
        '/^【教習所サーチから仮申し込み・資料請求がありました】ご確認ください。$/ui' => [
        	0 => ['entry_type' => ['brochure', 'tmp_entering']]
        ],
    ];

    /**
     * メール Body に適用するパターンリスト
     *
     * @var array
     */
    protected $bodyPattern = [
        '/^お問い合わせ日時：(\d{1,2}\/\d{1,2} \d{1,2}:\d{1,2})/uim' => [
        	1 => 'entry_date'
        ],
        '/^氏名：(.+)（(.+)／(\d+)歳）(?: (.+))?/uim' => [
        	1 => 'name',
        	2 => 'sex', 
        	3 => 'age', 
        	4 => 'occupation',
        ],
        '/^Tel：(.+)/uim' => [
        	1 => 'tel'
        ],
        '/^Mail：(.+)/uim' => [
        	1 => 'email'
        ],
        '/^ご住所：(.+)/uim' => [
        	1 => 'address'
        ],
        '/^希望免許：(.+)/uim' => [
        	1 => 'licence_type'
        ],
        '/^質問：(.+)\n\n============================\n/uims' => [
        	1 => 'body'
        ],
    ];

    protected function customizeContent()
    {
        // entry_date は、月/日 時:分のため、年と秒を加える。年はメール日付の年、秒は0とする。
        $mailDate = new Carbon($this->mail->date);
        if (! empty($this->content['entry_date'])) {
            $this->content['entry_date'] = $mailDate->year . '/' . $this->content['entry_date'] . ':00';
        }

        // 住所に郵便番号があれば分割する
        if (! empty($this->content['address'])) {
            if (str_contains($this->content['address'], '〒')) {
                if (preg_match('/^〒(\d{3}-?\d{4}) *(.+)/', $this->content['address'], $matches)) {
                    $this->content['zipcode'] = str_replace('-', '', $matches[1]);
                    $this->content['address'] = $matches[2];
                }
            }
        }

        // 質問内容がある時かつ、資料請求かつ問い合わせ・仮申込みがない場合に、問い合わせを加える
        if (! empty($this->content['body'])) {
            if (in_array('brochure', $this->content['entry_type']) && 
            	! in_array('inquiry', $this->content['entry_type']) && 
            	! in_array('tmp_entering', $this->content['entry_type'])) 
            {
                // HistoryをはじめのEntryTypeに紐つけるため、unshiftでcontactを先頭におく
                array_unshift($this->content['entry_type'], 'inquiry');
            }
        }
    }
}
