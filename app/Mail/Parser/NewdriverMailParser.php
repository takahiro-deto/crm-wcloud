<?php

namespace App\Mail\Parser;

class NewdriverMailParser extends MailParser
{
	/**
     * メールパターン名
     *
     * @var string
     */
    protected $name = 'hp';


	/**
     * メール From のパターンリスト
     *
     * @var array
     */
    protected $fromPattern = [
        '/info@newdriver\.co\.jp/i',
    ];    	


    /**
     * メール Subject に適用するパターンリスト
     *
     * @var array
     */
    protected $subjectPattern = [
        '/^【ニュードライバー教習所】資料請求・問い合わせがありました：/ui' => [0 => ['entry_type' => ['brochure']]],
        '/^【ニュードライバー教習所】ネット申し込みがありました：/ui' => [0 => ['entry_type' => ['tmp_entering']]],
    ];


	/**
     * メール Body に適用するパターンリスト
     *
     * @var array
     */
    protected $bodyPattern = [
        '/^■ お名前： (.+)（(.+)）$/uim' => [1 => 'name', 2 => 'kana'],
        '/^■ 年齢： (\d+)/uim' => [1 => 'age'],
        '/^■ 電話番号： (.+)/uim' => [1 => 'tel'],
        '/^■ メール： (.+)/uim' => [1 => 'email'],
        '/^■ 資料送付先：\n(.+)\n■ 取りたい免許/uims' => [1 => 'address'],
        '/^■ 取りたい免許： (.+)/uim' => [1 => 'licence_type'],
        '/^■ 確認したいこと： \n(.+)\n------------------------------------------------------------\n/uims' => [1 => 'body'],
        '/^■ お住いの地域： (.+)/uim' => [1 => 'general3'],
        '/^■ 管理番号：(.+)/uim' => [1 => 'generalContact1'],
        '/^■ デバイス：(.+)/uim' => [1 => 'device'],
    ];


    protected function customizeInfo()
    {
        // 住所に郵便番号があれば分割する
        if (! empty($this->content['address'])) {
            if (str_contains($this->content['address'], '〒')) {
                if (preg_match('/^〒(\d{3}-?\d{4})\n(.+)/uims', $this->content['address'], $matches)) {
                    $this->content['zipcode'] = str_replace('-', '', $matches[1]);
                    $this->content['address'] = trim(preg_replace('/[\r\n]+/', '', $matches[2]));
                } else {
                    $this->content['address'] = trim(preg_replace('/[\r\n]+/', '', $this->content['address']));
                }
            }
        }

        // 質問内容がある時かつ、資料請求かつ問い合わせ・仮申込みがない場合に、問い合わせを加える
        $this->content['body'] = trim($this->content['body'] ?? null);
        if (! empty($this->content['body'])) {
            if (in_array('brochure', $this->content['entry_type']) && ! in_array('inquiry', $this->content['entry_type']) && ! in_array('tmp_entering', $this->content['entry_type'])) {
                // HistoryをはじめのEntryTypeに紐つけるため、unshiftでcontactを先頭におく
                array_unshift($this->content['entry_type'], 'inquiry');
            }
        }
    }
}