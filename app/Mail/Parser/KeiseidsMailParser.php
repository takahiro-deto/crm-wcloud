<?php

namespace App\Mail\Parser;

class KeiseidsMailParser extends MailParser
{
    /**
     * メールパターン名
     *
     * @var string
     */
    protected $name = 'hp';

    /**
     * メール From のパターンリスト
     *
     * @var array
     */
    protected $fromPattern = [
        '/keisei-ds@wcloud\.jp/i',
    ];

    /**
     * メール Subject に適用するパターンリスト
     *
     * @var array
     */
    protected $subjectPattern = [
        '/^◆HPより【お問い合わせ】がありました（.+様）$/ui' => [0 => ['entry_type' => ['inquiry']]],
        '/^◆HPより【資料請求】がありました（.+様）$/ui' => [0 => ['entry_type' => ['brochure']]],
        '/^◆HPより【仮申し込み】がありました（.+様）$/ui' => [0 => ['entry_type' => ['tmp_entering']]],
    ];

    /**
     * メール Body に適用するパターンリスト
     *
     * @var array
     */
    protected $bodyPattern = [
        '/^【お名前】\n(.+)/uim' => [1 => 'name'],
        '/^【住所】\n((?:〒.+\n)?.+\n.+)/uim' => [1 => 'address'],
        '/^【メールアドレス】\n(.+)/uim' => [1 => 'email'],
        '/^【電話番号】\n(.+)/uim' => [1 => 'tel'],
        '/^【希望免許】\n(.+)/uim' => [1 => 'licence_type'],
        '/^【希望プラン】(.+)：/uim' => [1 => 'licence_type'],
        '/^【所持免許】\n(.+)/uim' => [1 => 'has_licence'],
        '/^【所持免許】(.+)/uim' => [1 => 'has_licence'],
        '/^【お問い合わせ内容】\n(.+)/uims' => [1 => 'body'],
    ];

    protected function customizeContent()
    {
        if (empty($this->content['licence_type'])) {
            $this->content['licence_type'] = '不明';
        }

        // 住所に郵便番号があれば分割する
        if (! empty($this->content['address'])) {
            if (str_contains($this->content['address'], '〒')) {
                if (preg_match('/^〒?(\d{3}-?\d{4})\n(.+)/uims', $this->content['address'], $matches)) {
                    $this->content['zipcode'] = str_replace('-', '', $matches[1]);
                    $this->content['address'] = preg_replace('/\n+/', '', $matches[2]);
                }
            }
        }

        // 質問内容がある時かつ、資料請求かつ問い合わせ・仮申込みがない場合に、問い合わせを加える
        $this->content['body'] = trim($this->content['body'] ?? null);
        if (! empty($this->content['body'])) {
            if (in_array('brochure', $this->content['entry_type']) && ! in_array('inquiry', $this->content['entry_type']) && ! in_array('tmp_entering', $this->content['entry_type'])) {
                // HistoryをはじめのEntryTypeに紐つけるため、unshiftでcontactを先頭におく
                array_unshift($this->content['entry_type'], 'inquiry');
            }
        }
    }
}
